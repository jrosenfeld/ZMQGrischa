FIND = find
RM = rm -f
ECHO = echo
XARGS = xargs
CHMOD = chmod
TAR = tar
TEST = test
CD = cd
UNZIP = unzip

LIB_DIR = ./lib/
LIB_SIMON = simon-1.0.0-jar-with-dependencies.jar
LIB_LOG4 = log4j-1.2.15.jar

JAVA_SRC_DIR=./src/
JAVA_SRC = $(shell find . -name *.java)
JAVA_OBJS = $(JAVA_SRC:.java=.class)

JAVAC = javac
JAVA_PATH=$(LIB_DIR)$(LIB_SIMON):$(LIB_DIR)$(LIB_LOG4):$(JAVA_SRC_DIR)
JAVAC_FLAGS = -cp $(JAVA_PATH) -encoding ISO-8859-1

JAR = jar
JAR_FLAGS = cmf
JAR_FILE=GriScha.jar
MANIFEST = ./mainClass
EXECUTABLE=grischa
TMP_DIR = tmp

.PHONY = all jar clean executable

all: $(JAVA_OBJS)
jar: $(JAR_FILE)

%.class: %.java
	$(JAVAC) $(JAVAC_FLAGS) $<

$(MANIFEST):
	@$(ECHO) "Generating manifest for jar archive"
	@$(ECHO) "Main-Class: de.htw.f4.grid.grischa.GriScha" > $(MANIFEST)
	@$(ECHO) "Class-Path: src/ \r" >> $(MANIFEST)

%.jar: $(MANIFEST) $(JAVA_OBJS)
	@$(ECHO) "Generating jar archive"
	@$(TEST) -d $(TMP_DIR) ||  mkdir tmp
	@$(UNZIP) -o $(LIB_DIR)$(LIB_SIMON) -d $(TMP_DIR)
	@$(UNZIP) -o $(LIB_DIR)$(LIB_LOG4) -d $(TMP_DIR)
	@$(CD) $(JAVA_SRC_DIR) && $(FIND) . \( -name '*.class' -o -name '*.properties' \) -print | $(XARGS) \
	$(JAR) $(JAR_FLAGS) ../$(MANIFEST) ../$(JAR_FILE) -C ../$(TMP_DIR) de -C ../$(TMP_DIR) org
	@$(RM) -r ./tmp mainClass



executable: jar
	@$(ECHO) "Generating executable"
	@$(ECHO) "#! /bin/sh" > $(EXECUTABLE)
	@$(ECHO) "java -jar ./GriScha.jar $$\c" >> $(EXECUTABLE)
	@$(ECHO) "*" >> $(EXECUTABLE)
	@$(ECHO) "exit 0;" >> $(EXECUTABLE)
	@$(CHMOD) 755 $(EXECUTABLE)

clean: 
	$(FIND) . -name '*.class' -o -name '*.log' | $(XARGS) $(RM)
	$(RM) $(MANIFEST)
	$(RM) $(EXECUTABLE)
	$(RM) $(JAR_FILE)
