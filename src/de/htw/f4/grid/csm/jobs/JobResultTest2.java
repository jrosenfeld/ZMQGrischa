package de.htw.f4.grid.csm.jobs;

import java.io.Serializable;

import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;

public class JobResultTest2 implements IJobResult<Double>, Serializable {

	private double result;

	public Double getResult() 
	{
		return this.result;
	}

	public void setResult(Double value) 
	{
		this.result = value;		
	}
	
}
