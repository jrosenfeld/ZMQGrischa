package de.htw.f4.grid.csm.jobs;

import java.io.Serializable;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;

public class JobResultChessImpl implements IJobResult<Integer>, Serializable
{
	private final static Logger	_log	= Logger.getLogger(JobResultChessImpl.class);

	private Integer result;
	
	public JobResultChessImpl()
	{
		this.result = 0;
	}
	
	public Integer getResult() 
	{
		return this.result;
	}

	public void setResult(Integer value) 
	{
		_log.info("Zwischenergebnis (" + value + ") vom Job gesetzt");
		this.result = value;
	}

}
