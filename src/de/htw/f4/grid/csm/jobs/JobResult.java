package de.htw.f4.grid.csm.jobs;

import java.io.Serializable;

import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;

public class JobResult<T> implements IJobResult<T>, Serializable  {

	private T result;
	
	public JobResult()
	{
		this.result = null;
	}
	
	public JobResult(T resultType)
	{
		this.result = resultType;
	}
	
	public T getResult() 
	{
		return this.result;
	}

	public void setResult(T value) 
	{
		this.result = value;
		
	}

}
