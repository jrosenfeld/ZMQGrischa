package de.htw.f4.grid.csm.jobs;

import java.io.Serializable;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.JobResultCallbackService;
import de.htw.f4.grid.csm.jobs.interfaces.IJob;
import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.htw.f4.grid.schachInterfaces.IChessGame;
import de.htw.f4.grid.schachlogik.IterativeAlphaBetaSearch;
import de.htw.f4.grid.schachlogik.Player;

public class JobChessImpl implements IJob, Serializable
{
	private static final long	serialVersionUID	= 1L;

	private final static Logger	log	= Logger.getLogger(JobChessImpl.class);
	
	private IChessGame			chessGame;
	private JobResultCallbackService jobResultCallbackObject;
	private IJobResult<Integer>	jobResult;
	private int workerNodeId;
	private boolean isDone;
	private Thread thread;
	private Player maximizingPlayer;

	public JobChessImpl(IChessGame chessGame, long executionTime, int workerNodeId, Player maximizingPlayer)
	{
		this.chessGame = chessGame;
		this.jobResult = new JobResult<Integer>();
		this.workerNodeId = workerNodeId;
		this.isDone = false;
		this.maximizingPlayer=maximizingPlayer;
	}

	public void execute()
	{
//		this.jobResult.setResult(-1000);
		
		//breche vorzeitig ab, falls keine gültige WorkerNode-ID übergeben wurde
		if (this.workerNodeId==-1)
		{
			this.log.error("execute(): ungültige WorkerNode id=" + this.workerNodeId + " übergeben. Job wird nicht ausgeführt");
			return;
		}
		
		IterativeAlphaBetaSearch iter = new IterativeAlphaBetaSearch(this.chessGame, maximizingPlayer);
		this.thread = new Thread(iter);
		thread.start(); //IterativeAlphaBetaSearch starten
		this.log.info("(wnId=" + workerNodeId + ") JobThread gestartet");
		
		try
		{
			while (this.isDone == false)
			{
				Thread.sleep(100);
				if(this.jobResult.getResult() == null || iter.getValue() != this.jobResult.getResult())
				{
					this.jobResult.setResult(iter.getValue());
					this.jobResultCallbackObject.sendJobResultToWnm(this.jobResult);					
					this.log.info("(wnId=" + workerNodeId + ") Zwischenergebnis wird gespeichert");
				}
			}
		}
		catch (Exception e)
		{
			if(this.isDone == false)
			{
				String message = "(wnId=" + workerNodeId + ") Setzen des Zwischenergebnis fehlgeschlagen";
				this.log.error(message);
				if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) this.log.debug(message, e);

			}
		}

		
		this.thread.stop();
	}

	public void abort()
	{
		if(this.thread.isAlive())
		{
			this.log.info("Jobausführung wird abgebrochen.");
			this.isDone = true;
			this.thread.stop();
		}
		this.isDone = true;
	}

	public IJobResult getResult()
	{
		return this.jobResult;
	}
	
	public boolean isDone()
	{
		return this.isDone;
	}

	public void setResultCallbackObject(JobResultCallbackService jobResultCallbackObject) {
		this.jobResultCallbackObject = jobResultCallbackObject;		
	}
	
	public IChessGame getChessGame() {
		return this.chessGame;
	}

}
