package de.htw.f4.grid.csm.jobs.interfaces;

import de.htw.f4.grid.csm.client.JobResultCallbackService;
import de.htw.f4.grid.schachInterfaces.IChessGame;


public interface IJob
{
	/**
	 * Führt Job aus.
	 */
	public void execute();
	
	/**
	 * gibt das aktuelle Ergebnis zurück.
	 * @return IJobResult - Aktuelles Ergebnis
	 */
	public IJobResult getResult();
	
	/**
	 * Bricht das Berechnen des Jobs ab.  
	 * @return IJobresult - Aktuelles Ergebnis
	 */
	public void abort();
	
	/**
	 * Gibt den Status des Job zurück. 
	 * @return boolean - True wenn Job fertig gerechnet wurde.
	 */
	public boolean isDone();
	
	public void setResultCallbackObject(JobResultCallbackService jobResultCallbackObject);
	
	public IChessGame getChessGame();
	
}
