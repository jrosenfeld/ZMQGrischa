package de.htw.f4.grid.csm.jobs.interfaces;

public interface IJobResult<T> 
{
	public void setResult(T value);
	
	public T getResult();
}
