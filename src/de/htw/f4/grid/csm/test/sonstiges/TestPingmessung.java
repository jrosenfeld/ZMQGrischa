package de.htw.f4.grid.csm.test.sonstiges;

import java.net.InetAddress;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.SystemInformationImpl;
import de.htw.f4.grid.csm.client.interfaces.ISystemInformation;
import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.ISite;
import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeListExtended;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 * Testet Latenz zwischen Master-WorkerNodes und WNM
 *
 */
public class TestPingmessung extends Thread
{
	private IWorkerNodeListExtended _workerNodeList;
	private final static Logger _log = Logger.getLogger(TestPingmessung.class);
	
	
	public TestPingmessung(IWorkerNodeListExtended workerNodeList)
	{
		_workerNodeList = workerNodeList;
	}
	
	
	@Override
	public void run()
	{
		starteTests();
		System.out.println("Thread gestartet: TestPingmessung");
	}
	
	
	private void starteTests()
	{
		
		
		//wiederhole test alle x-Sekunden
		while(true)
		{
			//warte auf mindestes einen WorkerNode
			while(_workerNodeList.getWorkerNodeHashMap().size() < 1) 
			{
				try
				{
					Thread.sleep(500);
				}
				catch (InterruptedException e)
				{
					String message ="Threadfehler";
					_log.error(message);
					if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
				}
			}
			
			
			try
			{
				Thread.sleep(10000);
				
				
				pingTest();
				
				
			}
			catch (InterruptedException e)
			{
				String message = "Threadfehler";
				_log.error(message);
				if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
			}
			
		}
		
		
	}
	
	
	private void pingTest()
	{
		IWorkerNodeListExtended workerNodeListManagement;
		IWorkerNode masterWorkerNode;
		SystemInformationCollecorPingWnToWnmThread mwnToWnmPingThread;	  //masterWorkerNode zu WNM
		SystemInformationCollecorPingMasterToWnThread wnToWnPingThread =null; //masterWorkerNode zu anderen WorkerNodes
		ArrayList<ISite> listOfSites;
		ArrayList<IWorkerNode> siteWorkerNodes;
		InetAddress[] currentAdresses;
		ArrayList<InetAddress[]> workerNodesAdresses;
		
		
		//hole alle Sites
		listOfSites = _workerNodeList.getAllSites();
		
		
		//gehe alle MasterWorkerNodes durch und ermittle Latenz zum Server
		for (ISite site : listOfSites)
		{
			workerNodesAdresses = new ArrayList<InetAddress[]>(); //Liste leeren, um neue WorkerNode-IPs abzulegen
			masterWorkerNode = site.getMasterWorkerNode();
			
			mwnToWnmPingThread = new SystemInformationCollecorPingWnToWnmThread(masterWorkerNode, _workerNodeList);
			mwnToWnmPingThread.start();
			
			//workernodes der aktuellen Site holen
			siteWorkerNodes = _workerNodeList.getAllWorkerNodesBySite(site);
			
			
			//Liste mit IP's der SiteWorkerNodes holen
			for (IWorkerNode currentWorkerNode : siteWorkerNodes)
			{
				
				//teste WorkerNode auf Korrektheit
				if (currentWorkerNode==null) continue ; //beende wenn workernode nicht existiert
				try
				{
					currentWorkerNode.getId();
				}
				catch (SimonRemoteException e)
				{
					//WN loeschen
					_workerNodeList.removeWorkerNode(currentWorkerNode);
					continue;
				}
				
				//IP-Adresse vom MasterWokerNode holen
				try
				{
					currentAdresses = currentWorkerNode.getSystemInformation().getIpAdresses();
					//merke IP-Adresse
					workerNodesAdresses.add(currentAdresses);
				}
				catch (SimonRemoteException e)
				{
					String message;
					message = "Abrufen der IPs vom WorkeNode fehlgeschlagen (Verbindung zum WorkerNode abgebrochen)";
					_log.error(message);
//					_log.debug(message, e);
				
					//WN loeschen
					_workerNodeList.removeWorkerNode(currentWorkerNode);
					
					listOfSites = _workerNodeList.getAllSites();
					//aktuellen Durchlauf vorzeitig beenden und zum nächsten übergehen
					break; //continue;
				}
				
			}
			
			//alle gesammelten IP's vom MasterWorkerNode durchpingen lassen
			//MasterWorkerNode pingt (sich selbst und) seine WorkerNodes an
			for (InetAddress[] workerNodeInetAddress : workerNodesAdresses)
			{
				wnToWnPingThread = new SystemInformationCollecorPingMasterToWnThread(masterWorkerNode,
						workerNodeInetAddress[0].getHostAddress().toString(), _workerNodeList);
				wnToWnPingThread.start();
			}
			
			
			
			
//			try
//			{
//				wnToWnPingThread = new SystemInformationCollecorPingMasterToWnThread(masterWorkerNode,
//						                                               currentAdresses[0].getHostAddress().toString(),
//						                                               currentWorkerNode.getId(),
//						                                               _workerNodeList);
//				wnToWnPingThread.start();
//			}
//			catch (SimonRemoteException e)
//			{
//				//Ueberpruenfen welcher WorkerNode nicht mehr erreichbar ist
//				//und diesen entsprechend Entfernen
//				
//				String message;
//			
//				int masterWnId = -1;
//				try
//				{
//					masterWnId = masterWorkerNode.getId();
//				}
//				catch (SimonRemoteException e1)
//				{
//					String message2;
//					message2="Abfrage der Id vom WorkerNode fehlgeschlagen  (Verbindung zum WorkerNode abgebrochen)";
//					_log.error(message2);
////					_log.debug(message2,e1);
//					
//					//defekten WorkerNode melden
//					_workerNodeList.removeWorkerNode(masterWorkerNode);
//				}
//				
//				
//				int currentWnId = -1;
//				try
//				{
//					currentWnId = currentWorkerNode.getId();
//				}
//				catch (SimonRemoteException e1)
//				{
//					String message2;
//					message2="Abfrage der Id vom WorkerNode fehlgeschlagen  (Verbindung zum WorkerNode abgebrochen)";
//					_log.error(message2);
////					_log.debug(message2,e1);
//					
//					//defekten WorkerNode melden
//					_workerNodeList.removeWorkerNode(currentWorkerNode);
//				}
//				
//				message = "Ping MasterWorkerNode (" + masterWnId +
//				          ")<->WorkerNode(" + currentWnId + ") fehlgeschlagen";
//					
//			}
			
			
		}
		
		
		//warte max. 30sek auf Ende des PingThreads
		//TODO folgende Lösung blockiert (zu lange) !?
//		try
//		{
//			wnToWnPingThread.join(30000);
//		}
//		catch (InterruptedException e)
//		{
//		}
		
	}

}
