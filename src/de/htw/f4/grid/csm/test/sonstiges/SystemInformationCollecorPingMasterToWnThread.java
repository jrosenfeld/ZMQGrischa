package de.htw.f4.grid.csm.test.sonstiges;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.ISystemInformation;
import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeListExtended;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 * 
 *	MasterWorkerNode pingt normalen WorkerNode an
 */
public class SystemInformationCollecorPingMasterToWnThread extends Thread
{
	private IWorkerNode _masterWorkerNode;
	private String _targetIpAddress;
	private final static Logger _log = Logger.getLogger(SystemInformationCollecorPingMasterToWnThread.class);
	private IWorkerNodeListExtended _workerNodeList;
	
	/**
	 * 
	 * @param workerNode
	 * @param wnmIp
	 * @param isMasterWorkerNode
	 * @param targetName Zielhost für Ping
	 */
	public SystemInformationCollecorPingMasterToWnThread(IWorkerNode masterWorkerNode, String targetIpAddress, IWorkerNodeListExtended workerNodeList)
	{
		_masterWorkerNode = masterWorkerNode;
		_targetIpAddress = targetIpAddress;
		_workerNodeList = workerNodeList;
//		
//		try
//		{
//			System.out.println(":: id=" + workerNode.getId() + "; wnmIp=" + wnmIp + "; isMasterWorkerNode="+isMasterWorkerNode + "; targetName=" +targetName );
//		}
//		catch (SimonRemoteException e)
//		{
//			System.err.println("FEHLER FEHLER FEHLER");
//		}
	}

	
	@Override
	public void run()
	{
		starteTests();
	}

	
	private void starteTests()
	{
		int rttAvg = 0;
		String message = "";
		
		
		//teste WorkerNode auf Korrektheit
		try
		{
			_masterWorkerNode.getId();
		}
		catch (SimonRemoteException e)
		{
			return;
		}
		
		
		try
		{
			ISystemInformation systemInformation = _masterWorkerNode.getSystemInformation();
			rttAvg = systemInformation.getRoundTripAvg(	_targetIpAddress);
			message = "rttMessung: masterWorkerNode (id=" + _masterWorkerNode.getId() + ", ip=" + _masterWorkerNode.getSystemInformation().getIpAdresses()[0].getHostAddress() + ") <---> workerNode (ip=" + _targetIpAddress +  ")  : " + rttAvg + "ms";
			_log.debug(message);
		}
		catch (SimonRemoteException e1)
		{
			String message2 = "Pingtest (MasterWorkerNodes<->WorkerNode) fehlgeschlagen";
			_log.error(message2);
			if (LoggingMessages.isUnknownErrorMessage(e1.getMessage())==true) _log.debug(message2, e1);
			
			_workerNodeList.removeWorkerNode(_masterWorkerNode);
		}

			
	}

}
