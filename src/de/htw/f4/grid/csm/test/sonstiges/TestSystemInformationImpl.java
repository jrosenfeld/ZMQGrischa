package de.htw.f4.grid.csm.test.sonstiges;

import java.net.InetAddress;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.SystemInformationImpl;
import de.htw.f4.grid.csm.client.interfaces.ISystemInformation;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.root1.simon.exceptions.SimonRemoteException;

public class TestSystemInformationImpl
{
	private final static Logger _log = Logger.getLogger(TestSystemInformationImpl.class);

	
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		ISystemInformation systemInformation = new SystemInformationImpl();
		
		String hostname = "";
		String site = "";
		int id = 0;
		int rtt01 = 0;
		int rtt02 = 0 ;
		InetAddress[] ipAdresses;

		
		try
		{
			site = systemInformation.getSite();
			_log.debug("site = " +site);
		}
		catch (SimonRemoteException e1)
		{
			String message = "getSite: fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e1.getMessage())==true) _log.debug(message, e1);
		}
		
		
		try
		{
			ipAdresses = systemInformation.getIpAdresses();
			_log.debug("WN: ipAdressen->hostname=" + ipAdresses[0].getHostName());
			_log.debug("WN: ipAdressen->ip="+ipAdresses[0].getHostAddress());
		}
		catch (SimonRemoteException e1)
		{
			String message = "Abfrage der IPAdressen fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e1.getMessage())==true) _log.debug(message, e1);
		}
		
		
		try
		{
			hostname = systemInformation.getHostname();
			_log.debug("hostname: " + hostname);
		}
		catch (SimonRemoteException e)
		{
			String message = "getHostname: fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
		}

		try
		{
			rtt01 = systemInformation.getRoundTripAvg("uranus.f4.htw-berlin.de");
			_log.debug("getRttDns: " + rtt01);
		}
		catch (SimonRemoteException e)
		{
			String message = "getRttDns: fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
		}
		
		
		try
		{
			rtt02 = systemInformation.getRoundTripAvg("141.45.146.102");
			_log.debug("getRttIp: " + rtt02);
		}
		catch (SimonRemoteException e)
		{
			String message = "getRttIp: fehlgeschlagen";
			_log.error(message);
			_log.debug(message, e);
		}
	}

}
