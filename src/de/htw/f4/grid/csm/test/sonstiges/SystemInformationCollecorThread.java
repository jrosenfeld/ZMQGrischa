package de.htw.f4.grid.csm.test.sonstiges;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.ISystemInformation;
import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.WorkerNodeListWithManagementImpl;
import de.htw.f4.grid.csm.server.interfaces.IServer;
import de.root1.simon.exceptions.SimonRemoteException;

/**
 * Sammelt ISystemInformation von einem einzigen Worker-Node (als Thread)
 * @author rybec
 *
 */
public class SystemInformationCollecorThread extends Thread
{
	private final static Logger _log = Logger.getLogger(SystemInformationCollecorThread.class);
	private IWorkerNode _workerNode;
	private IServer _server;

	public SystemInformationCollecorThread(IWorkerNode workerNode, IServer server)
	{
		_workerNode = workerNode;
		_server = server;
	}


	public void run()
	{
		outputRttAvg();
	}
	
	
	private void outputRttAvg()
	{
		int rttAvg = 0;
		String host = "uranus.f4.htw-berlin.de";
		
		try
		{
			ISystemInformation systemInformation = _workerNode.getSystemInformation();
			rttAvg = systemInformation.getRoundTripAvg(host);
			
			_log.debug("rttAvg  : " + rttAvg + "ms (" + host + ")");
		}
		catch (SimonRemoteException e)
		{
			String message = "Pingtest fehlgeschlagen (Verbindung zum WorkerNode abgebrochen)";
			_log.error(message);
//			_log.debug(message, e);
			
			
			//defekten WorkerNode melden
			_server.removeWorkerNode(_workerNode);
		}
	}

}
