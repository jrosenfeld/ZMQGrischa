package de.htw.f4.grid.csm.test.sonstiges;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IServer;
import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeList;


/**
 *
 * Testet die Worker-Node-Listen
 *
 */
public class TestWorkerNodeLists extends Thread
{
	private IServer _server;
	private final static Logger _log = Logger.getLogger(TestWorkerNodeLists.class);

	public TestWorkerNodeLists(IServer einServer)
	{
		_server = einServer;
	}

	@Override
	public void run()
	{
		_log.debug("Thread gestartet: TestWorkerNodeLists");
		try
		{
			while(true)
			{
				starteTests();
				Thread.sleep(5000);
			}
			
		}
		catch (InterruptedException e)
		{
			_log.error("Unbekannter Fehler");
		}
	}
	
	
	private void starteTests()
	{
		
		//warte auf mindestes einen WorkerNode
		while(_server.getWorkerNodeCount() < 1) 
		{
			try
			{
				Thread.sleep(500);
			}
			catch (InterruptedException e)
			{
				_log.error("Unbekannter Fehler");
			}
		}
//		System.out.println("Mindestens 1 WorkerNode hat sich angemeldet.");
		
		
		_server.parseWorkerNodeTree(true);
	
		
		//maximale Anzahl von WorkerNodes holen
		IWorkerNodeList workerNodeList = _server.getListOfWorkerNodes(_server.getWorkerNodeCount());
		
		
		//alle enthaltenen Worer-Nodes extrahieren
		ArrayList<IWorkerNode> listOfAllWorkerNodes = null;
		try
		{
			listOfAllWorkerNodes = workerNodeList.getAllWorkerNodes();
		}
		catch (Exception e1)
		{
			String message = "Abrufen von WorkerNodes fehlgeschlagen (Verbindung zum WorkerNode abgebrochen)";
			_log.error(message);
//			_log.debug(message, e1);
		}

		
		String hostname = "";
		int rttAvg = 0;
		SystemInformationCollecorThread systemInformationThread;
		ArrayList<Thread> listOfThreads = new ArrayList<Thread>(0); 
		
		
		for (IWorkerNode workerNode : listOfAllWorkerNodes)
		{
			systemInformationThread = new SystemInformationCollecorThread(workerNode, _server);
			listOfThreads.add(systemInformationThread);
			systemInformationThread.start();
			
//			try
//			{
//				ISystemInformation systemInformation = workerNode.getSystemInformation();
//				
//				//alle Hostnames ausgeben
//				hostname = 	systemInformation.getHostname();
//			}
//			catch (Exception e)
//			{
////				e.printStackTrace();
//				System.out.println("Hostname fehgeschlagen");
//			}
//			
//			try
//			{
//				ISystemInformation systemInformation = workerNode.getSystemInformation();
//				//ping-Test
//				rttAvg = systemInformation.getRoundTripAvg("uranus.f4.htw-berlin.de");
//				
//				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//				System.out.println("hostname: " + hostname);
//				System.out.println("rttAvg  : " + rttAvg + "ms (uranus.f4.htw-berlin.de)");
//				System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
//			}
//			catch (Exception e)
//			{
//				System.out.println("Hostname und Pingtest fehlgeschlagen");
//			}
		}
//			IWorkerNode workerNode = listOfAllWorkerNodes.get(1);
			
	
		
		
		
		//nur Worker-Nodes der Site "desy-hamburg" extrahieren
	}

}
