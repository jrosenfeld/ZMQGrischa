package de.htw.f4.grid.csm.test.sonstiges;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.ISystemInformation;
import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IServer;
import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeListExtended;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.root1.simon.exceptions.SessionException;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 *
 * WorkerNode pingt WorkerNodeManager an
 *
 */
public class SystemInformationCollecorPingWnToWnmThread extends Thread
{

	private IWorkerNode _workerNode;
	private String _target;
	private final static Logger _log = Logger.getLogger(SystemInformationCollecorPingWnToWnmThread.class);
	private String _targetName;
	private IWorkerNodeListExtended _workerNodeList;
	
	public SystemInformationCollecorPingWnToWnmThread(IWorkerNode masterWorkerNode, IWorkerNodeListExtended workerNodeList)
	{
		_workerNode = masterWorkerNode;
		_target = "uranus.f4.htw-berlin.de";
		_targetName = "WNM";
		_workerNodeList = workerNodeList;
	}
	

	@Override
	public void run()
	{
		starteTests();
	}

	
	
	private void starteTests()
	{
		int rttAvg = 0;
		String message = "";
		int idWn = -1;
		
		
		//teste WorkerNode auf Korrektheit
		if (_workerNode==null) return ; //beende wenn workernode nicht existiert
		try
		{
			_workerNode.getId();
		}
		catch (SimonRemoteException e)
		{
			return;
		}
		
		
		try
		{
			
			idWn = _workerNode.getId();
			ISystemInformation systemInformation = _workerNode.getSystemInformation();
			rttAvg = systemInformation.getRoundTripAvg(_target);
			message = "rttMessung: masterWorkerNode (id=" + _workerNode.getId() + ", ip=" + _workerNode.getSystemInformation().getIpAdresses()[0].getHostAddress() + ") <---> "+ _targetName + "  : " + rttAvg + "ms (" + _target + ")";
			
			//save rtt to MasterWorkerNode
//			_workerNode.getSystemInformation().setLastMeasuredRoundTripAvg(rttAvg);
			
			_log.debug(message);
		}
		catch (SimonRemoteException e)
		{
			String message2 = "Pingtest (MasterWorkerNode<->WNM) fehlgeschlagen";
			
			_log.error(message2);
			
			
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message2, e);
			
			
			//defekten WorkerNode melden
			_log.debug("WorkerNode id=" + idWn + " soll entfernt werden...");
			_workerNodeList.removeWorkerNode(_workerNode);
		}
		
	}



}
