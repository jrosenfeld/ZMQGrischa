package de.htw.f4.grid.csm.test.sonstiges;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;


/**
 * Testen von Listen/Baemen
 *
 */
public class ListTests
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		//Baumtest
		String s1 = "1-root";
		String s2 = "2-Katze";
		String s3 = "2-Maus";
		String s4 = "2-Elefant";
		String s5 = "3-Giraffe";
		String s6 = "3-Hund";
		DefaultMutableTreeNode top = new DefaultMutableTreeNode(s1);
		DefaultMutableTreeNode node01 = new DefaultMutableTreeNode(s2);
		DefaultMutableTreeNode node02 = new DefaultMutableTreeNode(s3);
		DefaultMutableTreeNode node03 = new DefaultMutableTreeNode(s4);
		DefaultMutableTreeNode node04 = new DefaultMutableTreeNode(s5);
		DefaultMutableTreeNode node05 = new DefaultMutableTreeNode(s6);
		
		JTree tree = new JTree(top);
		
		top.add(node01);
		top.add(node02);
		top.add(node03);
		
		node01.add(node04);
		node03.add(node05);
		
		
		DefaultMutableTreeNode wurzel = top;
		System.out.println("Anzahl der Kind-Knoten: " + top.getChildCount());
		
		parseTree(wurzel);
		
		DefaultMutableTreeNode user01 = (DefaultMutableTreeNode) top.getChildAt(2);
		System.out.println("3.1=" + user01.getUserObject());
		
		//sortieren auf 2. Ebene
		JTree treej = new JTree(top);
		
		
//		Enumeration e = top.children();
//		while (e.hasMoreElements())
//		{
//			DefaultMutableTreeNode obj = (DefaultMutableTreeNode) e.nextElement();
//			String str = (String) obj.getUserObject();
//			System.out.println(" +Knoten: " + str + " (children count: " + obj.getChildCount() + ")");
//		}
		
		//---------------------
		
		//erstellen
		ArrayList<Integer> intList = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++)
		{
			Random rand = new Random();
			
			intList.add(new Integer(rand.nextInt()));
		}

		//mit Zufallswerten fuellen
		System.out.println();
		for (int i = 0; i < intList.size(); i++)
		{
			System.out.println("element " + i + " = " + intList.get(i));
		}

		//sortieren
		Collections.sort(intList);
		
		//Ausgeben
		System.out.println();
		for (int i = 0; i < intList.size(); i++)
		{
			System.out.println("element " + i + " = " + intList.get(i));
		}
		
		//Ausgeben
		System.out.println();
		for (Integer integer : intList)
		{
			System.out.println("element = " + integer);
		}
	}
	
	
	private static void parseTree(DefaultMutableTreeNode node)
	{
		Enumeration e = node.children();
		
		TreeNode parent =  node.getParent();
		String parentStr = "";
		if (parent!=null) parentStr = parent.toString(); 
			
		System.out.println("parent=" + parentStr + " children=" + node.getUserObject());
		
		while (e.hasMoreElements())
		{
			parseTree((DefaultMutableTreeNode) e.nextElement());
		}
	}

}
