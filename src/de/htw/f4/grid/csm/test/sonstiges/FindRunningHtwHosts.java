package de.htw.f4.grid.csm.test.sonstiges;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import de.htw.f4.grid.csm.util.hostexplorer.Hostexplorer;

public class FindRunningHtwHosts
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		ArrayList<String> list = new ArrayList<String>();
		
		ArrayList<String> prefixes = new ArrayList<String>();
		String suffix = ".f4.htw-berlin.de";
		
		prefixes.add("pluto");
		prefixes.add("merkur");
		prefixes.add("mac");
		prefixes.add("saturn");
		prefixes.add("terra");
		prefixes.add("pan");
		
		
		//gehe alle prefixes durch
		for(String currentPrefix: prefixes)
		{
			//gehe alle Rechner-Nummern durch
			for (int i = 1; i <= 20; i++)
			{
				list.add(currentPrefix + i + suffix);
			}
		}
		
		//sonstige Rechner
//		list.add("141.45.154.72");
		

		
		Hostexplorer hostExplorer = new Hostexplorer();
		ArrayList<String> runningHosts = new ArrayList<String>();
		
		System.out.println("running hosts:");
		System.out.println("++++++++++++++\n");
		//gib alle hosts aus, die online sind
		runningHosts = hostExplorer.getRunningHosts(list);
		for(String currentHost : runningHosts)
		{
			System.out.println(currentHost);
		}
		
		
		System.out.println("\nssh hosts:");
		System.out.println("++++++++++++++\n");
		//gib alle hosts aus, die online sind und auf denen ssh läuft
		ArrayList<String[]> sshHosts = new ArrayList<String[]>();
		sshHosts = hostExplorer.getSshHosts(runningHosts);
		for(String[] currentHost : sshHosts)
		{
			System.out.println(currentHost[0] + " -p " + currentHost[1]);
		}
	}

}
