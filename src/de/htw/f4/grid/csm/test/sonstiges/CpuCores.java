package de.htw.f4.grid.csm.test.sonstiges;

/**
 * Gibt die Anzahl der dem System zur Verfügung stehenden CPU-Cores aus
 *
 */
public class CpuCores
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		displayAvailableProcessors();
	}

	
	private static void displayAvailableProcessors()
	{
        
        Runtime runtime = Runtime.getRuntime();
        int nrOfProcessors = runtime.availableProcessors();
        System.out.println(nrOfProcessors);
        
    }
    
}
