package de.htw.f4.grid.csm.test.serverUndClients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.server.ServerImpl;
import de.htw.f4.grid.csm.server.interfaces.IServer;

public class runServer
{
	private final static Logger _log = Logger.getLogger(runServer.class);
	private static IServer _server;

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		System.out.println("runServer.jar [port] [graphOutputfilename]\n");
		
		_server = new ServerImpl();
		
		//port setzen
		if (args.length>=1) _server.setListeningPort(Integer.valueOf(args[0]));
		
		//graph-filename setzen
		if (args.length>=2) _server.setGraphOutputFilename(String.valueOf(args[1]));
		
		_server.start();
		
		//interaktive Konsole starten
		startInteractiveCommandLine();
		
		System.out.println("Server gestoppt");
		_log.info("Server gestoppt");
		
		System.exit(0);
	}
	

	
	static private void startInteractiveCommandLine()
	{
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		String command = "";
		Boolean stillRunning = true;
		try
		{
			while(stillRunning==true)
			{
				showServerCommands();
				System.out.println("Kommando an Server: ");
				command=console.readLine();
				
				//server stoppen
				if (command.equals("stop"))
				{
					System.out.println("Server wird gestoppt...");
					_log.info("Server wird gestoppt...");
					_server.stop();
					
					_server = null;
					stillRunning = false;
				}
				
				
				//alle WorkerNodes abmelden/beenden
				if(command.equals("removeAllWorkerNodes"))
				{
					deregisterAllWorkerNodes();
				}
				
				//alle WorkerNodes abmelden/beenden
				if(command.equals("stat"))
				{
					int countWn = _server.getCountWorkerNodes();
					System.out.println("WorkerNode-Anzahl (ohne Netzwerk-Abfrage): " + countWn); 
				}
				
				//reparieren anstoßen
				if(command.equals("reapir"))
				{
					_server.repairWorkerNodes();
				}
				
				
				//alle registrierten/validen WorkerNodes ausgeben
				if(command.equals("showRegisteredWorkerNodes"))
				{
					StringBuffer workerNodeMap = new StringBuffer();
					long start = System.currentTimeMillis();
					workerNodeMap = _server.parseWorkerNodeTree(false);
					_log.debug("Zeitmessung: " + String.valueOf(System.currentTimeMillis()-start)  + "ms für _server.parseWorkerNodeTree()");
					
//					System.out.println(workerNodeMap);
				}
			}
		}
		catch (IOException e)
		{
			_log.error("Fehler beim Einlesen auf der Konsole");
		}

	}

	
	/**
	 * Alle WorkerNodes abmelden/beenden
	 */
	static private void deregisterAllWorkerNodes()
	{
		boolean shutDownWorkerNodes = true;
		_log.info("WorkerNodes sollen beendet werden...");
		_server.releaseAllWorkerNodes(shutDownWorkerNodes);
	}
	
	
	
	static private void showServerCommands()
	{
		System.out.println("\n\n--------------------------------------------------------");
		System.out.println("showRegisteredWorkerNodes	- alle registrierten WorkerNodes anzeigen");
		System.out.println("removeAllWorkerNodes		- alle WorkerNodes abmelden");
		System.out.println("stat				- alle registrierten WorkerNodes anzeigen (ohne Netzwerkabfrage)");
		System.out.println("reapir				- Reparieren der WN's anstoßen");
		System.out.println("--------------------------------------------------------\n");
	}

}
