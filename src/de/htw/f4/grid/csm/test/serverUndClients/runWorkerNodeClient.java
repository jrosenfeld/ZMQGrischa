package de.htw.f4.grid.csm.test.serverUndClients;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.WorkerNodeClientImpl;
import de.htw.f4.grid.csm.client.interfaces.IClient;
import de.htw.f4.grid.csm.server.interfaces.IWnm;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.htw.f4.grid.csm.util.argumentParser.ArgumentParser;

public class runWorkerNodeClient 
{
	private final static Logger _log = Logger.getLogger(runWorkerNodeClient.class);	
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		

		IClient<IWnm> client = new WorkerNodeClientImpl();

		setParameter(args, client);

		client.start();
		
		boolean isStopped = false;
		while(!isStopped)
		{
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e)
			{
				String message = "Fehler im Thread";
				_log.error(message);
				if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
			}
			
			//initialisiere das Beenden des Client, wenn seine Lebenszeit abgelaufen ist
			if (client.getRemainingLifetime()<=0)
			{
				isStopped = client.stop();	
			}				
		}
		
		_log.info("Client wird beendet");
		System.exit(0);
	}
	
	
	

	private static void setParameter(String[] args, IClient client) 
	{
		ArgumentParser cap = new ArgumentParser(args);
		
		/* check options */

		// set host
		if (cap.hasOption("host"))
		{
			client.setHost(cap.getOption("host"));
		}

		
		// set port
		if (cap.hasOption("port"))
		{
			try
			{
				int port = Integer.parseInt(cap.getOption("port"));
				client.setPort(port);
			} 
			catch (NumberFormatException nfex)
			{
				String message = "Port must be an integer: " + nfex;
				_log.error(message);
				if (LoggingMessages.isUnknownErrorMessage(nfex.getMessage())==true) _log.debug(message, nfex);
				
				System.exit(-1);
			}
		}
		
		// set serviceName
		if (cap.hasOption("serviceName"))
		{
			client.setServiceName(cap.getOption("serviceName"));
		}	
		
		// set lifetime
		if (cap.hasOption("lifetime"))
		{
			try
			{
				int lifetime = Integer.parseInt(cap.getOption("lifetime"));
				client.setLifetime(lifetime);
			} 
			catch (NumberFormatException nfex)
			{
				String message = "Lifetime must be an integer: " + nfex;
				_log.error(message);
				if (LoggingMessages.isUnknownErrorMessage(nfex.getMessage())==true) _log.debug(message, nfex);
				System.exit(-1);
			}
		}
		
		// set jobid
//		if (cap.hasOption("jobid"))
//		{
//			try
//			{
//				int jobId = Integer.parseInt(cap.getOption("jobid"));
//				client.setjobId(jobId);
//			} 
//			catch (NumberFormatException nfex)
//			{
//				_log.error("JobId must be an integer: " + nfex);
//				System.exit(-1);
//			}
//			hasParams = true;
//		}
		
	}

}
