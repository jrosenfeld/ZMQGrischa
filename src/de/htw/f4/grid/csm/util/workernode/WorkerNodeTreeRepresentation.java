package de.htw.f4.grid.csm.util.workernode;

import java.util.concurrent.Callable;


import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.ISite;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 *
 * Sammelt Information über ein WorkerNodes und arbeitet diese so auf,
 * dass daraus die Repräsentation für parseTree() erzeugt wird. 
 *
 */
public class WorkerNodeTreeRepresentation implements Callable<String>
{
	private final static Logger _log = Logger.getLogger(WorkerNodeTreeRepresentation.class);
	private IWorkerNode _workerNode;
	private ISite _siteOfWorkerNode;
	
	
	public WorkerNodeTreeRepresentation(IWorkerNode workerNode, ISite siteOfWorkerNode)
	{
		_workerNode = workerNode;
		_siteOfWorkerNode = siteOfWorkerNode;
	}


	public String call() throws Exception
	{
		StringBuilder outputMessage = new StringBuilder();
		int id;
		boolean isMasterWorkerNode = false;
		String hostname = "";
		
		
		//get hostname of WorkerNode
		try
		{
			//teste ob WorkerNode der MasterWorkerNode von der aktuellen Site ist
			String masterWorkerNodeOutput = "";
			id = _workerNode.getId();

			if (_siteOfWorkerNode.getMasterWorkerNode() == _workerNode) //pruefe auf Gleichheit
			{
				isMasterWorkerNode = true;
			}
			else
			{
				isMasterWorkerNode = false;
			}
			
			hostname = _workerNode.getSystemInformation().getHostname();
			outputMessage.append("\n		hostname=" + hostname + ", masterWN=" + isMasterWorkerNode + ", id=" + id);
		}
		catch (SimonRemoteException e)
		{
			String message = "Abrufen von Informationen zum WorkerNode fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
		}
		
		return outputMessage.toString();
	}

}
