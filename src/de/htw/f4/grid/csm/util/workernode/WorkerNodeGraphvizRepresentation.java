package de.htw.f4.grid.csm.util.workernode;

import java.util.concurrent.Callable;


import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.WorkerNodeToSiteMapping;
import de.htw.f4.grid.csm.server.interfaces.ISite;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 *
 * Sammelt Information über einen WorkerNode und arbeitet diese so auf,
 * dass daraus die Repräsentation für Graphviz erzeugt wird. 
 *
 */
public class WorkerNodeGraphvizRepresentation implements Callable<String>
{
	private final static Logger _log = Logger.getLogger(WorkerNodeGraphvizRepresentation.class);
	private ISite _site;
	private IWorkerNode _workerNode;
	
	public WorkerNodeGraphvizRepresentation(WorkerNodeToSiteMapping mapping)
	{
		_site = mapping.getSite();
		_workerNode = mapping.getWorkerNode();
	}


	
	public String call() throws Exception
	{
		StringBuilder graph = new StringBuilder();
		String siteName = _site.getName();
		String workerNodeHostName = "";
		String masterWorkerNodeRttToWnm = ""; //"todo ";
		boolean isMasterWorkerNode = false;
		
		//spezielle Behandlung, falls es sich um den MasterWorkerNode handelt
		if (_site.getMasterWorkerNode() == _workerNode)
		{
			isMasterWorkerNode = true;
			workerNodeHostName = _site.getMasterWorkerNode().getSystemInformation().getHostname();
			graph.append("\"" + siteName + "\" -> \"" + 
						 workerNodeHostName + "\"" +
					    "[ label = \"" + masterWorkerNodeRttToWnm  + "ms\" ]" +
					    ";");
			
			graph.append("\"" + workerNodeHostName + "\" [style=\"filled\",color=\"blue\"];");
			
			return graph.toString();
		}
		
		
		//"normalen" WorkerNode hinzufügen
		workerNodeHostName = _workerNode.getSystemInformation().getHostname();
		graph.append("\"" + siteName + "\" -> \"" + workerNodeHostName + "\";");
		
		
		return graph.toString();
	}
	
	
}
