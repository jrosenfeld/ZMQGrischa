package de.htw.f4.grid.csm.util.workernode;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.WnmImpl;
import de.root1.simon.exceptions.SimonRemoteException;



/**
 * Testet, ob eine WorkerNode defekt ist.
 *
 */
public class HealtCheck implements Callable<Boolean>
{
	private final static Logger _log = Logger.getLogger(HealtCheck.class);
	private IWorkerNode _workerNode;

	public HealtCheck(IWorkerNode workerNodeToTest)
	{
		_workerNode = workerNodeToTest;
	}


	/**
	 * Versucht die ID des WorkerNodes abzufragen. Bei Erfolg == TRUE
	 */
	public Boolean call() throws Exception
	{
		
		int workerNodeId;
		
		if (_workerNode==null) return true;
		
		try
		{
			long start = System.currentTimeMillis();
//			_log.debug("Zeitmessung: call() (start)");
			workerNodeId = _workerNode.getId();
//			_log.debug("Zeitmessung: call() took " + String.valueOf((System.currentTimeMillis()-start)) + "ms  (wnId=" + workerNodeId + ")");
		}
		catch (SimonRemoteException e)
		{
			return true;
		}
		
		return false;
		
	}

}
