package de.htw.f4.grid.csm.util.workernode;

import java.util.concurrent.Callable;


import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.ISite;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 *
 * Sammelt Information über eine Site und arbeitet diese so auf,
 * dass daraus die Repräsentation für Graphviz erzeugt wird. 
 *
 */
public class SiteGraphvizRepresentation implements Callable<String>
{
	private final static Logger _log = Logger.getLogger(SiteGraphvizRepresentation.class);
	private ISite _site;
	private String _graphRootName;
	
	public SiteGraphvizRepresentation(ISite site, String graphRootName)
	{
		_site = site;
		_graphRootName = graphRootName;
	}


	
	public String call() throws Exception
	{
		StringBuilder graph = new StringBuilder();
		String siteName = _site.getName();
		String graphRootName = _graphRootName;
//		int masterWorkerNodeId = getMasterWorkerNodeId();
		
		graph.append("\"" + graphRootName + "\" -> \"" + siteName + "\";"); 		//link: root<->site
		graph.append("\"" + siteName + "\" [style=\"filled\",color=\"#3f5f00\",fontsize=\"200.0\", fixedsize=\"false\", fontcolor=\"#E4FFAF\"];");	//style der Site
		
		
//		graph.append("\"" + siteName + "\" -> \"" + masterWorkerNodeId + "\";");
		
		return graph.toString();
	}
	
	
	private int getMasterWorkerNodeId()
	{
		int id = -1;
		
		try
		{
			//Fehlerfälle abfangen
			if (_site==null) return -1;
			if (_site.getMasterWorkerNode() == null)
			{
				_log.error("getMasterWorkerNodeId(): Auslesen der MasterWorkerNodes der Site '" + _site.getName() + "' fehlgeschlagen");
				return -1;
			}

			id = _site.getMasterWorkerNode().getId();
		}
		catch (SimonRemoteException e)
		{
			_log.error("Ermittlung der WorkerNode-ID fehlgeschlagen");
		}
		
		return id;
	}

}
