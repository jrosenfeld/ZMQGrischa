package de.htw.f4.grid.csm.util.workernode;

import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.server.interfaces.IVirtualWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IWnmConnector;
import de.htw.f4.grid.schachInterfaces.IChessGame;
import de.htw.f4.grid.schachlogik.GridSearch;
import de.htw.f4.grid.schachlogik.Player;
import de.htw.f4.grid.schachlogik.TestMain;


/**
 * Startet einen Job am WorkerNode
 *
 */
public class StartJob implements Callable<Boolean>
{
	private final static Logger _log = Logger.getLogger(StartJob.class);
	private IWnmConnector 		_wnmConnector; 
	private IVirtualWorkerNode  _node; 
	private IChessGame 			_game; 
	private long 				_wait; 
	private long				_BUFFER_TIME;
	private Player _maximizingPlayer;
	private TreeMap<String, Integer> _resultSet;
	private TreeSet<String> _sended;
	
	
	public StartJob(IWnmConnector wnmConnector, 
					IVirtualWorkerNode node, 
					IChessGame game, 
					long wait, 
					long BUFFER_TIME,
					Player maximizingPlayer,
					TreeMap<String, Integer> resultSet,
					TreeSet<String> sended)
	{
		_wnmConnector = wnmConnector;
		_node = node;
		_game = game;
		_wait = wait;
		
		_BUFFER_TIME = BUFFER_TIME;
		_maximizingPlayer = maximizingPlayer;
		_resultSet = resultSet;
		_sended = sended;
	}

	
	private void sendJob()
	{
		//*** Wartezeit anpassen *********************************************************
		long modifiedWaitTime = _wait-_BUFFER_TIME;						
		
		//*** Suche anlegen **************************************************************
		GridSearch currentGridSearch;	
//		_log.info("sendJob(): Erstelle neues GridSearch()");
		currentGridSearch = new GridSearch(_wnmConnector, _node, _game, _resultSet,
									       _maximizingPlayer, modifiedWaitTime);
		
//		//*** Suche starten, Thread dabei in Gruppen paken *******************************
//		Thread t=new Thread(_threadGroupForJobs, currentGridSearch);	
//		t.start();
		
		
		//da DistributedSearch durch einen ThreadPoolExecutor gejagt wird, ist keine
		//weitere Untergliederung in noch einen zusätzlichen Thread nötig, daher muss
		//GridSearch nicht als Thread gestartet werden
		currentGridSearch.run();
		
		
		//*** Das gesendete Brett merken *************************************************
//		_log.info("sendJob(): Das gesendete Brett merken");
		_sended.add(_game.getStringRepresentation());							
	}




	public Boolean call() throws Exception
	{
		try
		{
			sendJob();
		}
		catch (Exception e)
		{
			return false;
		}
		
		return true;
	}

}
