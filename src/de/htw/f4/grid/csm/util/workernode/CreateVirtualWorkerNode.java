package de.htw.f4.grid.csm.util.workernode;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.VirtualWorkerNodeImpl;
import de.htw.f4.grid.csm.server.interfaces.IVirtualWorkerNode;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.root1.simon.exceptions.SimonRemoteException;

public class CreateVirtualWorkerNode implements Callable<IVirtualWorkerNode>
{
	private final static Logger _log = Logger.getLogger(CreateVirtualWorkerNode.class);
	private IWorkerNode _workerNode; 
	private Integer _listId;
	
	public CreateVirtualWorkerNode(IWorkerNode workerNode, int listId)
	{
		_workerNode = workerNode;
		_listId = listId;
	}
	

	public IVirtualWorkerNode call() throws Exception
	{
		IVirtualWorkerNode virtualWorkerNode = null;
		
		try
		{
			int currentId = new Integer(_workerNode.getId());
			
			virtualWorkerNode = new VirtualWorkerNodeImpl(_listId,currentId);
		}
		catch (SimonRemoteException e)
		{
			String message = "Anlegen eines neuen Wertepaares <virtuelerWorkerNode,workerNode> fehlgeschlagen (WorkerNode existiert nicht mehr)";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
		}	
		
		return virtualWorkerNode;
	}

}
