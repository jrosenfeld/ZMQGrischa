package de.htw.f4.grid.csm.util.workernode;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;


/**
 * Beendet den Job eines WorkerNodes
 */
public class StopJob implements Callable<Boolean>
{
	private IWorkerNode _workerNode;
	private final static Logger _log = Logger.getLogger(StopJob.class);
	
	public StopJob(IWorkerNode workerNode)
	{
		_workerNode = workerNode;
	}


	public Boolean call() throws Exception
	{
		Boolean stopped = false;
		
		try
		{
			_workerNode.stopJob();
			stopped = true;
		}
		catch (Exception e)
		{
			_log.error("Hartes Abbrechen eine Jobs fehlgeschlagen");
		}
		
		return stopped;
	}

}
