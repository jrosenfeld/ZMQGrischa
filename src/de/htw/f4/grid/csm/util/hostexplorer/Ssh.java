package de.htw.f4.grid.csm.util.hostexplorer;

import java.net.Socket;
import java.util.concurrent.Callable;

import de.htw.f4.grid.csm.client.SystemInformationImpl;
import de.root1.simon.exceptions.SimonRemoteException;

public class Ssh implements Callable<String[]>
{
	private String _host;

	/**
	 * 
	 * @param host - IPAddress
	 */
	public Ssh(String host)
	{
		_host = host;
	}
	
	public String[] call() throws Exception
	{
		Boolean sshUp = false;
		int sshPort = 22;
		int plutoPort = 23222;
		
		//Sonderfälle für Pluto-Rechner
		if (_host.equals("pluto2.f4.htw-berlin.de")) sshPort = plutoPort; //23232;
		if (_host.equals("pluto3.f4.htw-berlin.de")) sshPort = plutoPort; //23233;
		if (_host.equals("pluto4.f4.htw-berlin.de")) sshPort = plutoPort; //23234;
		if (_host.equals("pluto5.f4.htw-berlin.de")) sshPort = plutoPort; //23235;
		if (_host.equals("pluto6.f4.htw-berlin.de")) sshPort = plutoPort; //23236;
		if (_host.equals("pluto7.f4.htw-berlin.de")) sshPort = plutoPort; //23237;
		if (_host.equals("pluto8.f4.htw-berlin.de")) sshPort = plutoPort; //23238;
		if (_host.equals("pluto9.f4.htw-berlin.de")) sshPort = plutoPort; //23239;
		if (_host.equals("pluto10.f4.htw-berlin.de")) sshPort = plutoPort; //23240;
		if (_host.equals("pluto11.f4.htw-berlin.de")) sshPort = plutoPort; //23241;
		if (_host.equals("pluto12.f4.htw-berlin.de")) sshPort = plutoPort; //23242;
		if (_host.equals("pluto13.f4.htw-berlin.de")) sshPort = plutoPort; //23243;
		if (_host.equals("pluto14.f4.htw-berlin.de")) sshPort = plutoPort; //23244;
		if (_host.equals("pluto15.f4.htw-berlin.de")) sshPort = plutoPort; //23245;
		if (_host.equals("pluto16.f4.htw-berlin.de")) sshPort = plutoPort; //23246;
		if (_host.equals("pluto17.f4.htw-berlin.de")) sshPort = plutoPort; //23247;
		if (_host.equals("pluto18.f4.htw-berlin.de")) sshPort = plutoPort; //23248;
		if (_host.equals("pluto19.f4.htw-berlin.de")) sshPort = plutoPort; //23249;
		if (_host.equals("pluto20.f4.htw-berlin.de")) sshPort = plutoPort; //23250;
		if (_host.equals("pluto21.f4.htw-berlin.de")) sshPort = plutoPort; //23251;
		
		if (_host.equals("141.45.154.72"))
		{
			sshPort = 23222;
		}
		
		//auf ssh-dienst testen
		try
		{
			Socket sock=new Socket(_host,sshPort);
			sshUp = true;
			sock.close();
		}
		catch (Exception e)
		{
			sshUp = false;
		}

		String[] result = {_host, sshUp.toString(), String.valueOf(sshPort)};
		
		return result;
	}

}
