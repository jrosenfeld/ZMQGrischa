package de.htw.f4.grid.csm.util.hostexplorer;

import java.util.concurrent.Callable;

import de.htw.f4.grid.csm.client.SystemInformationImpl;
import de.root1.simon.exceptions.SimonRemoteException;

public class Ping implements Callable<String[]>
{
	private String _host;

	/**
	 * 
	 * @param host - IPAddress
	 */
	public Ping(String host)
	{
		_host = host;
	}
	
	public String[] call() throws Exception
	{
		Boolean hostUp = false;
		int rtt = -1;
		
		//ping ausführen
		SystemInformationImpl util = new SystemInformationImpl();
		try
		{
			rtt = util.getRoundTripAvg(_host);
		}
		catch (SimonRemoteException e)
		{
		}

		if (rtt>=0) hostUp = true;

		String[] result = {_host, hostUp.toString()};
		
		return result;
	}

}
