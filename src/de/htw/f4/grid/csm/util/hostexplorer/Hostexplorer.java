package de.htw.f4.grid.csm.util.hostexplorer;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;



public class Hostexplorer
{
	/**
	 * 
	 * @param listOfIpAdresses - Liste mit IP's die geprüft werden soll
	 */
	public Hostexplorer()
	{
	}
	
	public ArrayList<String> getRunningHosts(ArrayList<String> listOfIpAdresses)
	{
		PingResult callback = new PingResult();
		Thread t = null;
		LinkedList<Callable<String[]>> serviceList = new LinkedList<Callable<String[]>>();
		ArrayList<String> resultList = new ArrayList<String>();
		
		//Arbeitsaufträge zusammenstellen
		for (String currentHost : listOfIpAdresses)
		{
			serviceList.add(new Ping(currentHost));
		}
		
		ThreadPoolExecutor pool = new ThreadPoolExecutor(serviceList.size(),
														 serviceList.size(),
														 0L,
														 TimeUnit.SECONDS,
														 new LinkedBlockingQueue<Runnable>());
		try
		{
			List<Future<String[]>> futureList;
			futureList = pool.invokeAll(serviceList);
			
			for (Future<String[]> future : futureList)
			{
				String[] futureStr = future.get();
				String host = futureStr[0];
				String up = futureStr[1];
				
				if (up.equals("true"))
				{
					resultList.add(host);
				}
			}
			
			pool.shutdown();
			
		}
		catch (InterruptedException e)
		{
		}
		catch (ExecutionException e)
		{
		}
		
		return resultList;
		
	}
	
	
	
	public ArrayList<String[]> getSshHosts(ArrayList<String> listOfIpAdresses)
	{
		PingResult callback = new PingResult();
		Thread t = null;
		LinkedList<Callable<String[]>> serviceList = new LinkedList<Callable<String[]>>();
		ArrayList<String[]> resultList = new ArrayList<String[]>();
		
		//Arbeitsaufträge zusammenstellen
		for (String currentHost : listOfIpAdresses)
		{
			serviceList.add(new Ssh(currentHost));
		}
		
		ThreadPoolExecutor pool = new ThreadPoolExecutor(serviceList.size(),
														 serviceList.size(),
														 0L,
														 TimeUnit.SECONDS,
														 new LinkedBlockingQueue<Runnable>());
		try
		{
			List<Future<String[]>> futureList;
			futureList = pool.invokeAll(serviceList);
			
			for (Future<String[]> future : futureList)
			{
				String[] futureStr = future.get();
				String host = futureStr[0];
				String up = futureStr[1];
				String port = futureStr[2];

				if (up.equals("true"))
				{
					String[] result = {host, port};
					resultList.add(result);
				}
			}
			
			pool.shutdown();
			
		}
		catch (InterruptedException e)
		{
		}
		catch (ExecutionException e)
		{
		}
		
		return resultList;
		
	}
}