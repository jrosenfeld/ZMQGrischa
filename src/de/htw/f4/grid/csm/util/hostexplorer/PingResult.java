package de.htw.f4.grid.csm.util.hostexplorer;

import java.util.HashMap;

public class PingResult
{
	private HashMap<String, Boolean> _resultHashMap; // <host, result>
	
	public PingResult()
	{
		_resultHashMap = new HashMap<String, Boolean>();
	}

	
	public void saveResult(String host, Boolean result)
	{
		_resultHashMap.put(host, result);
	}
}
