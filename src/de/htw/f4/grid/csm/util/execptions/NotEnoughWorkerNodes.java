package de.htw.f4.grid.csm.util.execptions;

public class NotEnoughWorkerNodes extends Exception
{

	public NotEnoughWorkerNodes()
	{
	}


	public NotEnoughWorkerNodes(String message)
	{
		super(message);
	}


	public NotEnoughWorkerNodes(Throwable cause)
	{
		super(cause);
	}


	public NotEnoughWorkerNodes(String message, Throwable cause)
	{
		super(message, cause);
	}

}
