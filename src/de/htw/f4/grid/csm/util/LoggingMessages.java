package de.htw.f4.grid.csm.util;

public class LoggingMessages
{
	public static final String SESSION_CLOSED = "session was closed";
	public static final String INDEX_OUT_OF_RANGE = "String index out of range";
	public static final String SHELL_NOT_FOUND = "/bin/sh: not found";
	public static final String GET_ID_FAILED = "Cannot handle method call \"public abstract int de.htw.f4.grid.csm.client.interfaces.IWorkerNode.getId() throws de.root1.simon.exceptions.SimonRemoteException\" on already closed session";

	
	public static  boolean isUnknownErrorMessage(String errorMessage)
	{
		if (errorMessage == null) return true;
		
		if (errorMessage.contains(SESSION_CLOSED)) return false;
		
		if (errorMessage.contains(INDEX_OUT_OF_RANGE)) return false;
		
		if (errorMessage.contains(SHELL_NOT_FOUND)) return false;
		
		if (errorMessage.contains(GET_ID_FAILED)) return false;
		
		return true;
	}
}
