package de.htw.f4.grid.csm.util.argumentParser;

public class ArgumentParserTest
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		ArgumentParser p = new ArgumentParser();
		
		p.set_args(args);
		p.checkArguments();
		
		if (p.hasOption("host"))
		{
			String host = p.getOption("host");
			System.out.println("host: " + host);
		}
		
		if (p.hasOption("port"))
		{
			String port = p.getOption("port");
			System.out.println("Port: " + port);
		}
		
		if(p.hasParam("bla")) {
			System.out.println("bla");
		}
		if(p.hasParam("bla2")) {
			System.out.println("bla2");
		}
	}
}