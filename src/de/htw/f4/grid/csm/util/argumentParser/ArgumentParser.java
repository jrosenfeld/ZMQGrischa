package de.htw.f4.grid.csm.util.argumentParser;

import java.util.Hashtable;
import java.util.Vector;

/**
 * ArgumentParser
 * 
 * @author Christoph Neumann
 * 
 */
public class ArgumentParser {

	private String[] args = {};
	private Vector<String> params = new Vector<String>(); // params
	private Hashtable<String, String> options = new Hashtable<String, String>(); // params+options
	private int paramIndex = 0;

	public ArgumentParser() {

	}

	public ArgumentParser(String[] args) {
		this.args = args;
		checkArguments();
	}

	/**
	 * @param args
	 *            the _args to set
	 */
	public void set_args(String[] args) {

		this.args = args;
	}
	

	public void checkArguments() {
		for (int i = 0; i < args.length; i++) {
			if (args[i].startsWith("-")) {
				if (args[i].contains("=")) {
					int k = args[i].indexOf("=");
					String key = (k > 0) ? args[i].substring(1, k) : args[i]
							.substring(1);
					String value = (k > 0) ? args[i].substring(k + 1) : "";
					options.put(key.toLowerCase(), value);
				} else {
					params.addElement(args[i].substring(1));
				}
			}
			params.addElement(args[i]);
		}
	}
	

	public void deleteEntries() {
		args = null;
		params.clear();
		options.clear();
		paramIndex = 0;
	}
	

	/**
	 * check agruments contain option
	 * 
	 * @return boolean
	 */
	public Boolean hasOption(String opt) {
		return options.containsKey(opt.toLowerCase());
	}
	

	/**
	 * return value for an option
	 * 
	 * @return value from option
	 */
	public String getOption(String opt) {
		return (String) options.get(opt.toLowerCase());
	}
	

	/**
	 * check arguments contains param
	 * 
	 * @return value from option
	 */
	public boolean hasParam(String opt) {
		return params.contains(opt.toLowerCase());
	}
	

	/**
	 * return next param from paramlist
	 * 
	 * @return param
	 */
	public String getNextParam() {
		if (paramIndex < params.size()) {
			return (String) params.elementAt(paramIndex++);
		}
		return null;
	}
	

}
