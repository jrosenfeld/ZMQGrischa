package de.htw.f4.grid.csm.client;

import java.io.IOException;
import java.security.Timestamp;
import java.util.Date;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.server.ServerImpl;
import de.htw.f4.grid.csm.server.interfaces.IWnm;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.htw.f4.grid.csm.util.argumentParser.ArgumentParser;
import de.htw.f4.grid.csm.client.interfaces.IClient;
import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.root1.simon.Simon;
import de.root1.simon.exceptions.EstablishConnectionFailed;
import de.root1.simon.exceptions.LookupFailedException;
import de.root1.simon.exceptions.SimonRemoteException;

public class WorkerNodeClientImpl implements IClient<IWnm>
{
	// constants
	private final static Logger log = Logger.getLogger(WorkerNodeClientImpl.class);
	private final static int STDLIFETIME = 46800000; //13h
	private final static String STDHOST = "localhost";
	private final static int STDPORT = 22222;
	private final static String STDSERVICENAME = "wnm";
	
	private IWorkerNode _workerNode;
	private IWnm _wnm;	// Wnm reference to server
	
	private String _host;
	private int _port;
	private String _serviceName;
	private long _lifetime;			// client lifetime
	private Date _lifetimeEnd;
	

	
	public WorkerNodeClientImpl() 
	{
		this._workerNode = null;
		this._wnm = null;
		this._host = STDHOST;
		this._port = STDPORT;
		this._serviceName = STDSERVICENAME;
		setLifetime(STDLIFETIME);
	}
	
	public void start() 
	{
		if(this._workerNode == null)
		{
			this._workerNode = createWorkerNode();
		}
		
		if(this._wnm == null)
		{
			this._wnm = getRemoteObject();
			
			//warte falls WNM nicht vom Server geholt werden konnte
			//und probiere es solange bis es funktioniert
			while (_wnm==null)
			{
				this._wnm = getRemoteObject();
				try
				{
					Thread.sleep(60000); //warte eine Minute
				}
				catch (InterruptedException e)
				{
					String message = "Fehler im Thread";
					this.log.error(message);
					if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) this.log.debug(message,e);
				}
			}
			
		}
		
		registerWorkerNodeToWnm();
		
		
	}


	private IWorkerNode createWorkerNode() 
	{
		IWorkerNode workerNode = new WorkerNodeImpl();
		this.log.info("WorkerNode created sucessfully");
		return workerNode;
	}
	
//	private IWnm getWnmFromServer()
//	{
//		IWnm wnm = null;
//		
//		try 
//		{
//			wnm = (IWnm) Simon.lookup(this._host, this._port, this._serviceName);
//			this.log.info("Wnm '" + this._serviceName + "' successfully retrieved from server");
//		} 
//		catch (EstablishConnectionFailed e) 
//		{
//			String message = "Holen des WNM vom Server fehlgeschlagen --> EstablishConnectionFailed";
//			log.error(message);
//			log.debug(message,e);
//		} 
//		catch (SimonRemoteException e) 
//		{
//			String message = "Holen des WNM vom Server fehlgeschlagen --> SimonRemoteException";
//			log.error(message);
//			log.debug(message, e);
//		} 
//		catch (IOException e) 
//		{
//			String message = "Holen des WNM vom Server fehlgeschlagen --> IOException";
//			log.error(message);
//			log.debug(message, e);
//		} 
//		catch (LookupFailedException e) 
//		{
//			String message = "Holen des WNM vom Server fehlgeschlagen --> LookupFailedException";
//			log.error(message);
//			log.debug(message, e);
//		}
//
//		
//		return wnm;
//	}
	
	private void registerWorkerNodeToWnm()
	{
		IWorkerNode workerNode = null;
		workerNode = this._workerNode;
		
		IWnm wnm = null;
		wnm = this._wnm;
		
		try
		{
			wnm.registerWorkerNode(workerNode);
			this.log.info("WorkerNode registered successfully");
			_wnm.sendMessageToWnm("WorkerNode an Wnm registriert");
			workerNode.setWnm(wnm);
		}
		catch (SimonRemoteException e)
		{
			String message = "WorkerNode Registrierung fehlgeschlagen --> SimonRemoteException";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		}
		
	}
	
	public boolean stop() 
	{
		this.log.info("Try to stop client.");
		
		IWnm wnm = this._wnm;
		
		try
		{
			wnm.releaseWorkerNode(this._workerNode);
			this.log.info("WorkerNode released successfully.");
		} 
		catch (SimonRemoteException e)
		{
			String message = "WorkerNode can't release. (" + e.getMessage() + ")"; 
			this.log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) this.log.debug(message, e);

			return false;
		}
		
		return true;
		
	}

	public void setHost(String host) {
		this._host = host;
		
	}

	public void setPort(int port) {
		this._port = port;
		
	}

	public void setServiceName(String serviceName) {
		this._serviceName = serviceName;
		
	}
	
	public void setLifetime(long lifetime)
	{
		this._lifetime = lifetime;
		
		Date actualDate = new Date();
		long lifetimeToSwitchOff = -1;
		lifetimeToSwitchOff = actualDate.getTime() + this._lifetime;
		
		this._lifetimeEnd = new Date(lifetimeToSwitchOff);
		
	}
	

	public long getRemainingLifetime()
	{
		Date actualDate = new Date();
		long remainLifetimeToSwitchOff;

		remainLifetimeToSwitchOff = (this._lifetimeEnd.getTime() - actualDate.getTime());
		
		return (int) remainLifetimeToSwitchOff;
	}

	
	public IWnm getRemoteObject()
	{
		IWnm wnm = null;
		
		try 
		{
			wnm = (IWnm) Simon.lookup(this._host, this._port, this._serviceName);
			this.log.info("Wnm '" + this._serviceName + "' successfully retrieved from server");
		} 
		catch (EstablishConnectionFailed e) 
		{
			String message = "Holen des WNM vom Server fehlgeschlagen --> EstablishConnectionFailed";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message,e);
		} 
		catch (SimonRemoteException e) 
		{
			String message = "Holen des WNM vom Server fehlgeschlagen --> SimonRemoteException";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		} 
		catch (IOException e) 
		{
			String message = "Holen des WNM vom Server fehlgeschlagen --> IOException";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		} 
		catch (LookupFailedException e) 
		{
			String message = "Holen des WNM vom Server fehlgeschlagen --> LookupFailedException";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		}

		
		return wnm;
	}

	public void setParameter(String[] args, IClient client) 
	{
		ArgumentParser cap = new ArgumentParser(args);
		
		/* check options */

		// set host
		if (cap.hasOption("host"))
		{
			client.setHost(cap.getOption("host"));
		}

		
		// set port
		if (cap.hasOption("port"))
		{
			try
			{
				int port = Integer.parseInt(cap.getOption("port"));
				client.setPort(port);
			} 
			catch (NumberFormatException nfex)
			{
				String message = "Port must be an integer: " + nfex;
				log.error(message);
				if (LoggingMessages.isUnknownErrorMessage(nfex.getMessage())==true) log.debug(message, nfex);
				
				System.exit(-1);
			}
		}
		
		// set serviceName
		if (cap.hasOption("serviceName"))
		{
			client.setServiceName(cap.getOption("serviceName"));
		}	
		
		// set lifetime
		if (cap.hasOption("lifetime"))
		{
			try
			{
				int lifetime = Integer.parseInt(cap.getOption("lifetime"));
				client.setLifetime(lifetime);
			} 
			catch (NumberFormatException nfex)
			{
				String message = "Lifetime must be an integer: " + nfex;
				log.error(message);
				if (LoggingMessages.isUnknownErrorMessage(nfex.getMessage())==true) log.debug(message, nfex);
				System.exit(-1);
			}
		}
	}

	
}
