package de.htw.f4.grid.csm.client.shellscript.interfaces;

import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;


public interface IShellScriptExecuter extends SimonRemote
{
	/**
	 * Fuehr eine ShellScript aus und liefert die Ausgabe zurueck.
	 * @param script
	 * @return
	 */
	public String execute(IShellScript script) throws SimonRemoteException;
}
