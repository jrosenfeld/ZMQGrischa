package de.htw.f4.grid.csm.client.shellscript;

import java.util.ArrayList;
import java.util.Map;

import de.htw.f4.grid.csm.client.shellscript.interfaces.IShellScript;
import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 * 
 * Ermittelt die Latenz (in ms) zu einem Host.
 *
 */
public class ScriptPingImpl implements IShellScript
{

	private String[] _script;
	private Map<String,String> _parameter;
	
	public ScriptPingImpl()
	{
		
	}
	
	public String[] getScript() throws SimonRemoteException
	{
		create();
		return _script;
	}

	
	private void create() throws SimonRemoteException
	{
		String hostname = getHostname();
		String[] script = {"/bin/sh", "-l", "-c", "ping -c 4 " + hostname};
		_script = script;
	}

	
	
	private String getHostname()
	{
		String hostname = "";
		
		if (_parameter.containsKey("hostname")) hostname = _parameter.get("hostname");
		
		return hostname;
	}
	

	
	public void setParameter(Map<String,String> parameter)
	{
		_parameter = parameter;
		
	}
}
