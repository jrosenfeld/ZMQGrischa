package de.htw.f4.grid.csm.client.shellscript.interfaces;

import java.util.Map;

import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;


public interface IShellScript extends SimonRemote
{
	
	/**
	 * Liefert das Skript als String
	 * @return
	 */
	public String[] getScript() throws SimonRemoteException;
	
	public void setParameter(Map<String,String> parameter) throws SimonRemoteException;
}
