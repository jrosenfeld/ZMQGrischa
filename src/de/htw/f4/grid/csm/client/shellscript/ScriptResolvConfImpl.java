package de.htw.f4.grid.csm.client.shellscript;

import java.util.ArrayList;
import java.util.Map;

import de.htw.f4.grid.csm.client.shellscript.interfaces.IShellScript;
import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 * 
 * Ermittelt die Latenz (in ms) zu einem Host.
 *
 */
public class ScriptResolvConfImpl implements IShellScript
{

	private String[] _script;
	private Map<String,String> _parameter;
	
	public ScriptResolvConfImpl()
	{
		
	}
	
	public String[] getScript() throws SimonRemoteException
	{
		create();
		return _script;
	}

	
	private void create() throws SimonRemoteException
	{
		String script;
		
		//don't work
		//redirection operator like | are shell feature, but the
		//command passed to exec() isn't running in a command shell
		//script = "cat /etc/resolv.conf | grep 'search '";
		
//		script = "/bin/sh -c 'cat /etc/resolv.conf | grep 'name''";
		String[] command = {"/bin/sh", "-l", "-c", "/bin/cat /etc/resolv.conf | grep 'search'"};
		
		_script = command;
	}

	
	public void setParameter(Map<String,String> parameter)
	{
		_parameter = parameter;
		
	}
}
