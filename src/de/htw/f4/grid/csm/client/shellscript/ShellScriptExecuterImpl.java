package de.htw.f4.grid.csm.client.shellscript;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.swing.tree.VariableHeightLayoutCache;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.WorkerNodeClientImpl;
import de.htw.f4.grid.csm.client.shellscript.interfaces.IShellScript;
import de.htw.f4.grid.csm.client.shellscript.interfaces.IShellScriptExecuter;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.root1.simon.exceptions.SimonRemoteException;


public class ShellScriptExecuterImpl implements IShellScriptExecuter
{

	private final static Logger _log = Logger.getLogger(ShellScriptExecuterImpl.class);
	
	public ShellScriptExecuterImpl()
	{
	}


	public String execute(IShellScript script) throws SimonRemoteException
	{
		IShellScript shellScript = (IShellScript) new ScriptPingImpl();
		String[] scriptCommand = script.getScript();
		String result = "";
		
		// http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html?page=4
		try
		{
			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(scriptCommand);
			
			InputStream stderr = proc.getErrorStream();
			InputStreamReader isrErr = new InputStreamReader(stderr);
			BufferedReader brErr = new BufferedReader(isrErr);
			String lineErr = null;
			while ((lineErr = brErr.readLine()) != null)
			{
				System.out.println(lineErr);
			}
			
			InputStream stdin = proc.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdin);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
//			System.out.println("<OUTPUT>");
			while ((line = br.readLine()) != null)
			{
//				System.out.println(line);
				result+= line;
			}
//			System.out.println("</OUTPUT>");
			int exitVal = proc.waitFor();
//			System.out.println("Process exitValue: " + exitVal);
		} 
		catch (Throwable t)
		{
			String message = "Ausfuehrung eines Scripts fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(t.getMessage())==true) _log.debug(message, t);
		}
		
		return result;
		
	}

}
