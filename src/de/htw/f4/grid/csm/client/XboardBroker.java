package de.htw.f4.grid.csm.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.util.LoggingMessages;


/**
 * Dieses Programm wird von xboard aufgerufen.
 * Es verbindet sich beim Start automatisch mit dem
 * Schachspielt und nutzt dessen stdin/out (diese sind
 * über eine Socket-Verbindung abgebunden) 
 *
 *
 *
 * Aufruf
 * ======
 * 		zuerst: TestMain.java starten
 * 		danach: dieses Broker starten und über die Kommandozeile den
 * 				ersten Zug eingeben, z.B.: a7a6
 */
public class XboardBroker
{
	private final static Logger	_log	= Logger.getLogger(XboardBroker.class);

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		connectToChessServer();
	}
	
	
	static private void connectToChessServer()
	{
		Socket echoSocket;
		PrintWriter out;
		BufferedReader in;
		String tmpOut;
		try
		{
			InetAddress removeServerIp = InetAddress.getLocalHost();
			echoSocket = new Socket(removeServerIp, 4444);
			out = new PrintWriter(echoSocket.getOutputStream(), true);
			in = new  BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
		
			
			BufferedReader stdIn = new BufferedReader( new InputStreamReader(System.in));
			BufferedWriter stdOut = new BufferedWriter(new OutputStreamWriter(System.out));
			
			String userInput;
			while ((userInput = stdIn.readLine()) != null)
			{
				out.println(userInput);
				tmpOut = in.readLine();
				tmpOut = tmpOut.replace("%%", "\n");
				System.out.println("Server antwortet: \n" + tmpOut);
			}
			
			out.close();
			in.close();
			stdIn.close();
			echoSocket.close();
		}
		catch (UnknownHostException e)
		{
			_log.error("Verbindung zum CLI-Socket des ChessServer fehlgeschlagen");
		}
		catch (IOException e)
		{
			String message = "Unbekannter Fehler im Broker";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
		}
	}

}
