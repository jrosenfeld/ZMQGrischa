package de.htw.f4.grid.csm.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.IClient;
import de.htw.f4.grid.csm.jobs.JobResult;
import de.htw.f4.grid.csm.jobs.interfaces.IJob;
import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;
import de.htw.f4.grid.csm.server.interfaces.IVirtualWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IWnm;
import de.htw.f4.grid.csm.server.interfaces.IWnmConnector;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.htw.f4.grid.csm.util.argumentParser.ArgumentParser;
import de.root1.simon.Simon;
import de.root1.simon.exceptions.EstablishConnectionFailed;
import de.root1.simon.exceptions.LookupFailedException;
import de.root1.simon.exceptions.SimonRemoteException;

public class ChessClientImpl implements IClient<IWnmConnector> {
	
	// constants
	private final static Logger log = Logger.getLogger(ChessClientImpl.class);
	private final static int STDLIFETIME = 86400000; // 1000 * 60 * 60 * 24
	private final static String STDHOST = "localhost";
	private final static int STDPORT = 13735; //22222
	private final static String STDSERVICENAME = "wnmConnector";
	
	private IWnm wnm;	// Wnm reference to server
	private IWnmConnector wnmConnector;
	
	private String host;
	private int port;
	private String serviceName;
	private long lifetime;			// client lifetime
	private long endTime;
	

	
	

	public ChessClientImpl()
	{
//		super();
//		this.wnm = wnm;
//		this.wnmConnector = wnmConnector;
		this.host = STDHOST;
		this.port = STDPORT;
		this.serviceName = STDSERVICENAME;
		setLifetime(STDLIFETIME);
	}

	public long getRemainingLifetime()
	{
		long actualTime = System.currentTimeMillis();
		long remainLifetimeToSwitchOff = -1;
		remainLifetimeToSwitchOff = (this.endTime - actualTime);

		return remainLifetimeToSwitchOff;
	}

	public void setHost(String host)
	{
		this.host = host;
	}

	public void setLifetime(long lifetime)
	{
		this.lifetime = lifetime;
		
		long actualTime = System.currentTimeMillis();
		this.endTime = TimeUnit.MILLISECONDS.convert(this.lifetime,
				TimeUnit.MILLISECONDS) + actualTime;		
	}

	public void setPort(int port)
	{
		this.port = port;
	}

	public void setServiceName(String serviceName)
	{
		this.serviceName = serviceName;
	}

	public void start()
	{

		// if(this.wnm == null)
		// {
		// this.wnm = getWnmFromServer();
		// }

		if (this.wnmConnector == null)
		{
			this.wnmConnector = getWnmConnectorFromServer();
		}

		//testing();
	}

	private IWnmConnector getWnmConnectorFromServer()
	{
		IWnmConnector wnmConnector = null;

		try
		{
			wnmConnector = (IWnmConnector) Simon.lookup(this.host, this.port,
					this.serviceName);
			this.log.info("WnmConnector '" + this.serviceName
					+ "' successfully retrieved from server");
		} catch (EstablishConnectionFailed e)
		{
			String message = "Holen des WnmConnector vom Server fehlgeschlagen --> EstablishConnectionFailed";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		} catch (SimonRemoteException e)
		{
			String message = "Holen des WnmConnector vom Server fehlgeschlagen --> SimonRemoteException";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		} catch (IOException e)
		{
			String message = "Holen des WnmConnector vom Server fehlgeschlagen --> IOException";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		} catch (LookupFailedException e)
		{
			String message = "Holen des WnmConnector vom Server fehlgeschlagen --> LookupFailedException";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true)log.debug(message, e);
		}

		return wnmConnector;
	}

	public boolean stop()
	{
		return false;
	}
	
	public IWnmConnector getWnmConnector(){
		return this.wnmConnector;
	}

	public IWnmConnector getRemoteObject()
	{
		return this.wnmConnector;
	}

	public void setParameter(String[] args, IClient client) 
	{
		ArgumentParser cap = new ArgumentParser(args);
		
		/* check options */

		// set host
		if (cap.hasOption("host"))
		{
			client.setHost(cap.getOption("host"));
		}

		
		// set port
		if (cap.hasOption("port"))
		{
			try
			{
				int port = Integer.parseInt(cap.getOption("port"));
				client.setPort(port);
			} 
			catch (NumberFormatException nfex)
			{
				String message = "Port must be an integer: " + nfex;
				log.error(message);
				if (LoggingMessages.isUnknownErrorMessage(nfex.getMessage())==true) log.debug(message, nfex);
				
				System.exit(-1);
			}
		}
		
		// set serviceName
		if (cap.hasOption("serviceName"))
		{
			client.setServiceName(cap.getOption("serviceName"));
		}	
		
		// set lifetime
		if (cap.hasOption("lifetime"))
		{
			try
			{
				int lifetime = Integer.parseInt(cap.getOption("lifetime"));
				client.setLifetime(lifetime);
			} 
			catch (NumberFormatException nfex)
			{
				String message = "Lifetime must be an integer: " + nfex;
				log.error(message);
				if (LoggingMessages.isUnknownErrorMessage(nfex.getMessage())==true) log.debug(message, nfex);
				System.exit(-1);
			}
		}
	}	
}
