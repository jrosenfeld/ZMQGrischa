package de.htw.f4.grid.csm.client;

import java.io.Serializable;
import java.util.concurrent.Callable;

import de.htw.f4.grid.csm.jobs.interfaces.IJob;
import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;

public class JobService implements Runnable, Serializable
{
	
	private IJob job;
	
	public JobService(IJob job)
	{
		this.job = job;
	}

	public void run()
	{
		this.job.execute();
	}

}
