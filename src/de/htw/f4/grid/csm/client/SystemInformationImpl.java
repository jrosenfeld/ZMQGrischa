package de.htw.f4.grid.csm.client;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.ISystemInformation;
import de.htw.f4.grid.csm.client.shellscript.ScriptPingImpl;
import de.htw.f4.grid.csm.client.shellscript.ScriptResolvConfImpl;
import de.htw.f4.grid.csm.client.shellscript.ShellScriptExecuterImpl;
import de.htw.f4.grid.csm.client.shellscript.interfaces.IShellScript;
import de.htw.f4.grid.csm.client.shellscript.interfaces.IShellScriptExecuter;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.root1.simon.exceptions.SimonRemoteException;


public class SystemInformationImpl implements ISystemInformation
{
	private final String ENV_HOSTNAME = "HOSTNAME";
	private final static Logger log = Logger.getLogger(SystemInformationImpl.class);
	private IShellScriptExecuter _shellScriptExecuter;
	private int _lastRtt;
	
	public SystemInformationImpl()
	{
		_shellScriptExecuter = new ShellScriptExecuterImpl();
		_lastRtt = -1;
	}

	
	public String getHostname() throws SimonRemoteException
	{
		String hostname = "";
		
		//FIXME Java kann nicht auf $HOSTNAME zugreifen???
		hostname = System.getenv(ENV_HOSTNAME);
		
		//workaround
		
		try
		{
			hostname = java.net.InetAddress.getLocalHost().getHostName();
		}
		catch (UnknownHostException e)
		{
			log.warn("Hostname des System konnte nicht ermittelt werden", e);
		}

		
		return hostname;
	}

	
	public String getSite() throws SimonRemoteException
	{
		IShellScript shellScriptResolvConf = new ScriptResolvConfImpl();
		String result = "";
		String domain = "";
		String domainRaw = ""; //ungefilterer Eintrag aus resolv.conf

		//Shellscript ausfuehren
		domainRaw = _shellScriptExecuter.execute(shellScriptResolvConf);		//"search    d-grid.scai.fraunhofer.de scai.fraunhofer.de"
//		domainRaw = "search 	d-grid.scai.fraunhofer.de scai.fraunhofer.de";
		//debug
//		domainRaw = "search d-grid.scai.fraunhofer.de scai.fraunhofer.de";
		
		log.debug("domainRaw=" + domainRaw);
		
		String searchToken = "search";
		if (domainRaw.contains(searchToken)!=true)
		{
			String message = "Keinen passenden Eintrag in resolv.conf gefunden. Inhalt der resolv.conf='" + domainRaw + "'";
			log.error(message);
			
			return ""; //fraunhofer.de
		}

		
		try
		{
			domain = domainRaw;
			
			//search entfernen
			domain = domain.replace(searchToken, "");							//"    d-grid.scai.fraunhofer.de scai.fraunhofer.de"
			domain  = domain.trim(); //tabs etc. entfernen						//"d-grid.scai.fraunhofer.de scai.fraunhofer.de"
			
			int firstDomainEndsIndex = 0;
			firstDomainEndsIndex = domain.indexOf(" ");
			
			//erste domain ermitteln falls mindestens zwei vorhanden sind
			if (firstDomainEndsIndex>0)
			{
				result = domain.substring(0, firstDomainEndsIndex);				//"d-grid.scai.fraunhofer.de"
			}
			else
			{
				result = domain;												//"d-grid.scai.fraunhofer.de"
			}
			
			
			//domain kuerzen
			int topLevelIndex = 0;
			int secondLevelIndex = 0;
			String tmp;
			topLevelIndex = result.lastIndexOf(".");
			tmp = result.substring(0, topLevelIndex);							//"d-grid.scai.fraunhofer"
			secondLevelIndex = tmp.lastIndexOf(".") +1;
			result = result.substring(secondLevelIndex);						//"fraunhofer.de"
		}
		catch (Exception e)
		{
			String message = "Ausleses der Site-Informationen fehlgeschlagen. Inhalt der resolf.conf='" + domainRaw + "'";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
			return "";
		}
		
		return result;
	}


	public int getRoundTripAvg(String hostName) throws SimonRemoteException
	{
		int rttAvg = 0; //roundTripTimeAvg
		IShellScript shellScriptPing = new ScriptPingImpl();
		String result;
		
		//Parameter fuer Script setzen
		Map<String,String> parameter = new Hashtable<String, String>();
		parameter.put("hostname", hostName);
		shellScriptPing.setParameter(parameter);
		
		try
		{
			result = _shellScriptExecuter.execute(shellScriptPing);
		}
		catch (Exception e1)
		{
			String message;
			message = "Ausführung des ShellScript fehlgeschlagen";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e1.getMessage())==true) log.debug(message, e1);
			return -1;
		}
		
		//debug
//		log.debug("(client) " + result);
		
		
		//FIXME bei 100% PaketLoss kommt eine Fehlermeldung
		String rttMinStr = "";
		String rttAvgStr = "";
		String rttMaxStr = "";
		String rttStdDevStr = "";
		
		String rttAll = "";
		
		int avgStarts = 0;
		int rttAvgStarts = 0;
		int rttAvgEnds = 0;
		
//		avgStarts = result.lastIndexOf("stddev = ");
//		rttAll = result.replaceAll("stddev = ", "");
//		
//		rttAll = rttAll.substring(avgStarts);
//		rttAll = rttAll.replace("ms", "");
//		rttAll = rttAll.trim();
//		
//		//debug
//		System.out.println("(client) " + rttAll);
//				
//		
//		rttAvgStarts = rttAll.indexOf("/");
//		rttAvgEnds = rttAll.indexOf("/", rttAvgStarts+1);
//		
//		
//		rttAvgStr = rttAll.substring(rttAvgStarts+1, rttAvgEnds);
		
		
		try
		{
			avgStarts = result.lastIndexOf(" = ");	// "(...) = 6.516/6.516/6.516/0.000 ms"
			rttAll = result.replace(" = ", "");		// "(...)6.516/6.516/6.516/0.000 ms"
			rttAll = rttAll.substring(avgStarts);	// "6.516/6.516/6.516/0.000 ms"
			rttAll = rttAll.replace(" ms", "");		// "6.516/6.516/6.516/0.000"
			
			rttAvgStarts = rttAll.indexOf("/");
			rttAvgEnds = rttAll.indexOf("/", rttAvgStarts+1);
			rttAvgStr = rttAll.substring(rttAvgStarts+1, rttAvgEnds);	//6.516
		}
		catch (Exception e)
		{
			String message;
			message = "Ermittlung der rtt zu '" + hostName + "' fehlgeschlagen"; 
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message,e);
			
			return -1;
		}
		
		
		
		try
		{
			//debug
			log.debug(rttAvgStr);
			
			
			rttAvg = (int) Math.round(Float.valueOf(rttAvgStr));
			
			//debug
//			log.debug("(client) " + hostName +"->"+rttAvg+"ms");
		}
		catch (NumberFormatException e)
		{
			String message = "Umwandlung der rtt zum String fehlgeschlagen";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
			
			return -1;
		}
		
		
		return rttAvg;
	}


	
	/**
	 * Hole IPAdressen
	 */
	public InetAddress[] getIpAdresses() throws SimonRemoteException
	{
		String hostname = "";
		InetAddress ipAdressen[] = null;
		
		try 
		{
			hostname = InetAddress.getLocalHost().getHostName();
		} 
		catch (UnknownHostException e)
		{
			String message = "Client: Kann lokalen hostnamen nicht ermitteln";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		}
		
		
		try
		{
			ipAdressen =  InetAddress.getAllByName(hostname);
		} 
		catch (UnknownHostException e)
		{
			String message = "Aufloesung des hostnamens in Ip-Adressen fehlgeschlagen";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message,e);
		}
		
		return ipAdressen;
	}


	public int getLastMeasuredRoundTripAvg()  throws SimonRemoteException
	{
		return _lastRtt;
	}


	public void setLastMeasuredRoundTripAvg(int rtt)  throws SimonRemoteException
	{
		_lastRtt = rtt;
	}

}
