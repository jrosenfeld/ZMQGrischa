package de.htw.f4.grid.csm.client;

import java.io.Serializable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.ISystemInformation;
import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.jobs.interfaces.IJob;
import de.htw.f4.grid.csm.server.interfaces.IWnm;
import de.root1.simon.exceptions.SimonRemoteException;

public class WorkerNodeImpl implements IWorkerNode, Serializable
{

	private final static Logger			log	= Logger.getLogger(WorkerNodeImpl.class);

	private IJob						job;
	private ISystemInformation			systemInformation;
	private int							id;

	private ExecutorService				executorService;
	private JobService					jobService;
	private JobResultCallbackService	jobResultCallbackObject;
	private IWnm						wnm;

	
	
	public WorkerNodeImpl()
	{
		this.job = null;
		this.systemInformation = new SystemInformationImpl();
		this.executorService = null;
		this.jobService = null;
		
	}

	
	public void startJob(IJob job)
	{
		sendMessageToWnm("Job entgegengenommen");
		initJob(job);

		executorService.submit(this.jobService);

		sendMessageToWnm("Job (wnId=" + id + ") gestartet");
		this.log.info("(wnId=" + id + ") Starting job.");

	}


	
	/**
	 * Job initialisieren.
	 * Alle noch laufenden (alten) Jobs werden hart abgebrochen
	 * @param job
	 */
	private void initJob(IJob job)
	{
		//ungültiger Job?
		if (job == null)
		{
			this.log.error("(wnid=" + id + ") Es wurde versucht einen ungültigen (NULL) job auszuführen");
			
			// TODO: Wird Exception weitergereicht?
			throw new NullPointerException();
		}
		
		this.job = job;
		this.jobResultCallbackObject = new JobResultCallbackService(this.wnm, this.job.getChessGame().getHash());;
		this.job.setResultCallbackObject(this.jobResultCallbackObject);		
		this.jobService = new JobService(job);
		this.executorService = Executors.newSingleThreadExecutor();
	}
	
	public void stopJob() throws SimonRemoteException
	{
		
		//vorzeitiges Beenden, falls noch gar kein Job gerechnet wurde
		if (executorService==null) return; 
		
		
		//nur dann hart Abbrechen, wenn überhaupt noch einer aktiv ist
		if (this.executorService.isTerminated()==false)
		{
			this.job.abort();

			this.executorService.shutdownNow();
			this.log.info("WorkerNode (id=" + id + "): Aktuellen Job hart beendet");
		}

	}

	
//	public void destroyWorkerNode()
//	{
//		// FIXME not implemented yet
//		this._executorService.shutdown();
//	}

	
	/**
	 * Stellt Systeminformationen zur Verfuegung.
	 */
	public ISystemInformation getSystemInformation()
			throws SimonRemoteException
	{
		return this.systemInformation;
	}

	
	public int getId() throws SimonRemoteException
	{
		return this.id;
	}

	
	public void setId(int id) throws SimonRemoteException
	{
		this.id = id;
		
		this.log.info("Id des Worker-Nodes wurde veraendert: id=" + this.id);
	}
	
	
	
	private void sendMessageToWnm(String message)
	{
		//falls Wnm nicht vorhanden
		if (this.wnm==null)
		{
			return;
		}
		
//		try
//		{
//			long start = System.currentTimeMillis();
//			this.wnm.sendMessageToWnm("WorkerNode (id=" + this.id + ") -- " + message);
//			this.log.debug("Zeitmessung: sendMessageToWnm: " + String.valueOf(System.currentTimeMillis()-start) + " ms");
//		}
//		catch (SimonRemoteException e)
//		{
//			this.log.debug("(id=" + this.id + ") sendMessageToWnm() fehlgeschlagen");
//		}
	}

	
	
	public void setWnm(IWnm wnm) throws SimonRemoteException
	{
		this.wnm = wnm;
		sendMessageToWnm("Wnm in WorkerNode gesetzt");
	}

	
	/**
	 * WorkerNode ist vom Server entfernt worden.
	 * Es sind alle noch laufenden Jobs zu stoppen.
	 * 
	 * @param shutdown - bei TRUE wird der Prozess beenden,
	 *                   bei FALSE bleibt er für die restliche Lebenszeit aktiv
	 */
	public void release(boolean shutDown) throws SimonRemoteException
	{
		
//		_log.info("WorkerNode (id=" + _id + ") meldet sich vom WNM ab");
//		try
//		{
//			_wnm.releaseWorkerNode(this);
//		}
//		catch (Exception e)
//		{
//		}
//		_log.info("WorkerNode (id=" + _id + ") von dem WNM abgemeldet");
		
		this.log.info("WorkerNode (id=" + id + ") meldet sich vom WNM ab");
		
		if (shutDown==true)
		{
			this.log.info("WorkerNode-Prozess (id=" + id + ") beendet");
			System.exit(0);
		}
	}

}
