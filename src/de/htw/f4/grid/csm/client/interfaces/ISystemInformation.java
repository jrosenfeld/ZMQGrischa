package de.htw.f4.grid.csm.client.interfaces;

import java.net.InetAddress;

import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;

/**
 *
 * Interface stellt Informationen zum Hostsystem zur Verfuegung.
 *
 */
public interface ISystemInformation extends SimonRemote
{
	/**
	 * Hostname des WorkerNodes
	 * @return
	 */
	public String getHostname() throws SimonRemoteException;
	
	
	/**
	 * Site des Worker-Nodes (wie z.B. Hamburg oder Berlin)
	 * @return
	 */
	public String getSite() throws SimonRemoteException;
	

	
	/**
	 * Liefert Latenz zu einem angegebenen Host
	 * 
	 * @param hostName
	 * @return
	 */
	public int getRoundTripAvg(String hostName) throws SimonRemoteException;
	
	
//	/**
//	 * rtt direkt im WorkerNode speichern bzw von diesem abrufen (rtt zu Wnm) 
//	 */
//	public int getLastMeasuredRoundTripAvg() throws SimonRemoteException;
//	public void setLastMeasuredRoundTripAvg(int rtt) throws SimonRemoteException;
	
	
	/**
	 * Ermittelt die IPAdressen aller verfügbaren lokalen NetzwerkInterfaces
	 * @return
	 * @throws SimonRemoteException
	 */
	public InetAddress[] getIpAdresses() throws SimonRemoteException;
}
