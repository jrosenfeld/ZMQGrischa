package de.htw.f4.grid.csm.client.interfaces;

import java.util.concurrent.TimeUnit;

import de.htw.f4.grid.csm.jobs.interfaces.IJob;
import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;
import de.htw.f4.grid.csm.server.interfaces.IWnm;
import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;

public interface IWorkerNode extends SimonRemote {


	/**
	 * Startet einen Job auf dem WorkerNode
	 * @param job
	 * @throws SimonRemoteException
	 */
	public void startJob(IJob job) throws SimonRemoteException;
	
	/**
	 * Stoppt den aktuellen Job hart auf dem WorkerNode.
	 * @return IJobResult - Aktuelles Ergebnis von Job
	 * @throws SimonRemoteException
	 */
	public void stopJob() throws SimonRemoteException;
	
	/**
	 * Gibt aktuelles Ergebnis von WorkerNode zurück
	 * @return IJobResult - Aktuelles Ergebnis von Job
	 * @throws SimonRemoteException
	 */
//	public IJobResult getResult() throws SimonRemoteException;
	
	
	/**
	 * WorkerNode vom WNM abmelden
	 * @param shutDown - true, dann beendet sich autom. der WorkerNode-Prozess
	 * @throws SimonRemoteException
	 */
	public void release(boolean shutDown) throws SimonRemoteException;
	

	/**
	 * Stellt Systeminformationen zur Verfuegung.
	 * @return
	 * @throws SimonRemoteException
	 */
	public ISystemInformation getSystemInformation() throws SimonRemoteException;
	
	
	/**
	 * Setzt eine (fortlaufende, eindeutige) ID die intern von dem WNM verwendet wird um den 
	 * WorkerNode zuidentifizieren
	 * @param id
	 * @throws SimonRemoteException
	 */
	public void setId(int id) throws SimonRemoteException;

	
	/**
	 * Ruft die ID des WNM ab, die dem WorkerNode zugeordnet ist.
	 * @return
	 * @throws SimonRemoteException
	 */
	public int getId() throws SimonRemoteException;
	
	//DEBUG
	public void setWnm(IWnm wnm) throws SimonRemoteException;
	
}
