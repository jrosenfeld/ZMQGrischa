package de.htw.f4.grid.csm.client.interfaces;

public interface IClient<T> {

	/* 
	 * start client 
	 */
	public void start();
	
	/* 
	 * stop client 
	 */
	public boolean stop();
	
	public void setHost(String host);
	
	public void setPort(int port);
	
	public void setServiceName(String serviceName);
	
	public void setLifetime(long lifetime);
	
	public void setParameter(String[] args, IClient client);

	/**
	 * 
	 * @return remainingLifetime in ms
	 */
	public long getRemainingLifetime();
	
	public T getRemoteObject();
}
