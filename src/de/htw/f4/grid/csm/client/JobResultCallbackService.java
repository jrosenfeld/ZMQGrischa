package de.htw.f4.grid.csm.client;

import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;
import de.htw.f4.grid.csm.server.interfaces.IWnm;
import de.root1.simon.exceptions.SimonRemoteException;

public class JobResultCallbackService {

	private IWnm workerNodeManager;
	private String mMoveHash;
	
	public JobResultCallbackService(IWnm wnm, String moveHash)
	{
		this.workerNodeManager = wnm;
		this.mMoveHash = moveHash;
	}
	
	public void sendJobResultToWnm(IJobResult jobResult) throws SimonRemoteException
	{
		this.workerNodeManager.saveJobResult(jobResult, this.mMoveHash);
	}
}
