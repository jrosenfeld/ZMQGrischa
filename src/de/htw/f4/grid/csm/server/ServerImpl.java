	package de.htw.f4.grid.csm.server;

import java.io.FileWriter;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IServer;
import de.htw.f4.grid.csm.server.interfaces.IWnm;
import de.htw.f4.grid.csm.server.interfaces.IWnmConnector;
import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeList;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.htw.f4.grid.csm.util.execptions.NotEnoughWorkerNodes;
import de.root1.simon.Registry;
import de.root1.simon.Simon;
import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.NameBindingException;
import de.root1.simon.exceptions.SimonRemoteException;


public class ServerImpl implements IServer 
{
	
	// constants
	private final static Logger log = Logger.getLogger(ServerImpl.class);
	private final static int STANDARDPORT = 22222;
	
	private String _graphOutputFilename;
	
	private IWnm wnm;
	private IWnmConnector wnmConnector;
	private int serverPort;
	private Registry registry;
	private String serviceNameWnm = "wnm";
	private String serviceNameWnmConnector = "wnmConnector";
	
//	private int arraySize = 1;
//	private int pollingInterval = 4000;
//	private int numberOfPasses = 10;
//	private int minNumberOfWorkernodes = 1;
//	private int upperArrayBound = 100000;
	
	public ServerImpl() 
	{
		this.wnm = null;
		this.serverPort = STANDARDPORT;
		_graphOutputFilename = "graph.dot";
	}
	
	public void start() 
	{
		if(this.wnm == null) 
		{
			this.wnm = createWnm();
		}
		
		if(this.wnmConnector == null)
		{
			this.wnmConnector = createWnmConnector();
		}
		
		if(this.registry == null) 
		{
			this.registry = createRegistry();
		}
		
		bindWnmToRegistry();
		
		bindWnmConnectorToRegistry();
		
		this.log.info("Server started successfully on port " + serverPort);
	}

	private IWnm createWnm() 
	{
		IWnm wnm = null;
		
		try 
		{
			wnm = new WnmImpl();
			wnm.setGraphOutputFilename(_graphOutputFilename);
		} 
		catch (SimonRemoteException e) 
		{
			String message = "Error: Can't create Wnm";
			this.log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) this.log.debug(message, e);
		}
		
		return wnm;
		
	}
	
	private IWnmConnector createWnmConnector() 
	{
		IWnmConnector wnmConnector = null;
		
		try 
		{
			wnmConnector = new WnmConnectorImpl(this.wnm);
		} 
		catch (SimonRemoteException e) 
		{
			String message = "Error: Can't create WnmConnector";
			this.log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) this.log.debug(message, e);
		}
		
		return wnmConnector;
		
	}

	private Registry createRegistry() {
		Registry registry = null;

			try 
			{
				registry = Simon.createRegistry(this.serverPort);
				this.log.info("Create SIMON-Registry successfully");
			} catch (UnknownHostException e) 
			{
				String message = "Error: Create SIMON-Registry failed (UnknownHostException)";
				this.log.error(message);
				if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) this.log.debug(message, e);
			}
			catch (IOException e) 
			{
				String message = "Error: Create SIMON-Registry failed (IOException)";
				this.log.error(message);
				if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) this.log.debug(message,e);
			}

		return registry;
	}
	
	
	private void bindWnmToRegistry()
	{
		IWnm wnm = this.wnm;
		String serviceName = this.serviceNameWnm;

		this.log.info("Try to add Object '" + this.serviceNameWnm + "' to SIMON-Registry");
		bindToRegistry(serviceName, wnm);
	}
	
	
	private void bindWnmConnectorToRegistry()
	{
		IWnmConnector wnmConnector = this.wnmConnector;
		String serviceName = this.serviceNameWnmConnector;

		this.log.info("Try to add Object '" + this.serviceNameWnmConnector + "' to SIMON-Registry");
		bindToRegistry(serviceName, wnmConnector);
	}
	
	
	private void bindToRegistry(String serviceName, SimonRemote object) {
		
		Registry registry = this.registry;
		
		try 
		{
			registry.bind(serviceName, object);
			this.log.info("Add WnmObject '" + serviceName + "' to SIMON-Registry successfully");
		} 
		catch (NameBindingException e) 
		{
			String message = "Can not add object '" + serviceName + "' to registry";
			this.log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) this.log.debug(message,e);
		}
	}


	
	public void stop() 
	{
		boolean shutDownWorkerNode = true;
		
		releaseAllWorkerNodes(shutDownWorkerNode);
		unbindWnmFromRegistry();
		unbindWnmConnectorFromRegistry();
		deleteRegistry();
		this.log.info("Server stoped successfully.");
	}
	
	
	@SuppressWarnings("deprecation")
	private void deleteRegistry() 
	{
		this.registry.stop();
		this.registry = null;
		this.log.info("Registry deleted successfully");
	}
	
	
	private void unbindWnmFromRegistry() 
	{
		String stubName = this.serviceNameWnm;
		
		this.registry.unbind(stubName);
		
		
		this.log.info("Unbind wnm from registry successfully");
		
		this.wnm = null;
	}
	
	
	private void unbindWnmConnectorFromRegistry() 
	{
		String stubName = this.serviceNameWnmConnector;
		
		this.registry.unbind(stubName);
		
		this.log.info("Unbind WnmConnector from registry successfully");
		
		this.wnmConnector = null;
	}

	
	public IWorkerNodeList getListOfWorkerNodes(int number) 
	{
		IWorkerNodeList workerNodeList = null;
		try
		{
			workerNodeList = wnm.getWorkerNodeList(number);
		}
		catch (SimonRemoteException e)
		{
			String message = "Aufruf von getListOfWorkerNodes() fehlgeschlagen";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		}
		
		
		return workerNodeList;
	}

	public void getStatus() 
	{
		// FIXME not implemented yet
		
	}
	
	public int getWorkerNodeCount()
	{
		int wnCount = -1 ;
		try
		{
			wnCount = this.wnm.getNumberOfRegisteredWorkerNodes();
		}
		catch (SimonRemoteException e)
		{
			String message = "Aufrufen von getNumberOfRegisteredWorkerNodes() an dem Wnm gescheitert";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		}
		this.log.info("Gesamtanzahl der angemeldeten WorkerNodes: " + wnCount);
		return wnCount;
	}

	public void setListeningPort(int port)
	{
		this.serverPort = port;
	}

	
	/**
	 * DEBUG ONLY!!!
	 * print out workerNode-tree
	 */
	public StringBuffer parseWorkerNodeTree(boolean printOut)
	{
		StringBuffer result = new StringBuffer();
		try
		{
			result = wnm.parseWorkerNodeTree(printOut);
		}
		catch (SimonRemoteException e)
		{
			String message = "Aufruf von parseWorkerNodeTreeAndPrintOut() fehlgeschlagen";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		}
		
		return result;
	}


	/**
	 * DEBUG ONLY !!!
	 */
	public void startTestPingmessung()
	{
		try
		{
			wnm.startTestPingmessung();
		}
		catch (SimonRemoteException e)
		{
			String message = "Aufruf von startTestPingmessung() fehlgeschlagen";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message,e);
		}
	}
	
	

	synchronized public void removeWorkerNode(IWorkerNode workerNode)
	{
		try
		{
			wnm.removeWorkerNode(workerNode);
		}
		catch (SimonRemoteException e)
		{
			String message = "Aufruf von removeWorkerNode() fehlgeschlagen";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message,e);
		}
	}


	/**
	 * DEBUG ONLY !!!
	 */
	public void saveTreeRepresentationAsDotFile(FileWriter outputFile)
	{
		try
		{
			wnm.saveTreeRepresentationAsDotFile(outputFile);
		}
		catch (SimonRemoteException e)
		{
			String message = "Aufruf von saveTreeRepresentationAsDotFile() fehlgeschlagen";
			log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) log.debug(message, e);
		}
	}

	
	
	
	public int getFreeWorkerNodeCount() throws SimonRemoteException
	{
		return this.wnm.getFreeWorkerNodeCount();
	}

	public ArrayList<IWorkerNode> getWorkerNodeList(int count)
			throws SimonRemoteException, NotEnoughWorkerNodes
	{
		return this.wnm.getFreeWorkerNodeList(count, 0);
	}

	public void releaseWorkerNodeList(ArrayList<IWorkerNode> workerNodeList)
			throws SimonRemoteException
	{
		// FIXME not implemented yet (workerNodes freigeben)
	}

	
	
	/**
	 * Alle am Server angemeldeten WorkerNodes abmelden
	 */
	public void releaseAllWorkerNodes(boolean shutDown)
	{
		try
		{
			this.wnm.releaseAllWorkerNodes(shutDown);
		}
		catch (SimonRemoteException e)
		{
			this.log.error("releaseAllWorkerNodes() fehlgeschlagen");
		}
	}

	
	public void setGraphOutputFilename(String filename)
	{
		_graphOutputFilename = filename;
	}

	public int getCountWorkerNodes()
	{
		int wnCount = -1;
		try
		{
			wnCount = wnm.getNumberOfRegisteredWorkerNodes();
		}
		catch (SimonRemoteException e)
		{
			this.log.error("getCountWorkerNodes(): 'wnm.getNumberOfRegisteredWorkerNodes()' fehlgeschlagen");
		}
		
		return wnCount;
	}

	public void repairWorkerNodes()
	{
		this.wnm.repairWorkerNodes();
	}

	
}
