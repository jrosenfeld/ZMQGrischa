package de.htw.f4.grid.csm.server;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.ISystemInformation;
import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.ISite;
import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeList;
import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeListExtended;
import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeMetaInformation;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.htw.f4.grid.csm.util.graphviz.GraphViz;
import de.htw.f4.grid.csm.util.hostexplorer.Ping;
import de.htw.f4.grid.csm.util.workernode.HealtCheck;
import de.htw.f4.grid.csm.util.workernode.SiteGraphvizRepresentation;
import de.htw.f4.grid.csm.util.workernode.WorkerNodeGraphvizRepresentation;
import de.htw.f4.grid.csm.util.workernode.WorkerNodeTreeRepresentation;
import de.root1.simon.exceptions.SimonRemoteException;

/**
 * 
 * Zentrale Liste, die alle Worker-Nodes enthaelt.
 * Wird ausschliesslich von dem WNM verwendet.
 * 
 * (Das Schach-Programm darf nur eine Instanz der 
 *  IWorkerNodeList angeboten bekommen!)
 *
 */
public class WorkerNodeListWithManagementImpl extends WorkerNodeListAbstract implements IWorkerNodeList,
		IWorkerNodeListExtended, Serializable
{
	private final static Logger _log = Logger.getLogger(WorkerNodeListWithManagementImpl.class);
	
	private String _graphOutputFilename;
	
	//In Abfolge der Registrierung bei dem WNM werden die WorkerNodes der Reihe
	//nach in workerNodeHashMap abgelegt.
	private HashMap<Integer, IWorkerNode> _workerNodeHashMap;

	private GraphViz _graphVizRepresentationOfTree; //enthält Baum als GraphViz-Beschreibung
	
	// fortlaufende Nummer, die bei jeder Anmeldung eines Worker-Nodes
	// um 1 erhoeht wird
	private int _totalRegistrations;
	
	private DefaultMutableTreeNode _workerNodeTreeRoot; //Baumstruktur die WorkerNodes und Sites enthält

	
	//Liste mit den gleichen ID's der WorkerNodes. Diese Liste enthaelt MetaInformationen
	//zu den einzelen WorkerNodes
	private HashMap<Integer, IWorkerNodeMetaInformation> _workerNodeMetaInformation;
	
	
	//verknuepft die selbst erzeugten WorkerNode-IDs mit den eindeutigen RemoteObjectHash der WorkerNodes
	//Wird fuer das spaetere Loeschen eines WNs benoetigt, wenn gezielt ein WorkerNode-Objekt und nicht ueber die
	//id geloescht werden soll.
	private HashMap<String, Integer> _wnIdToRemoteObjectHashMapping; //<RemoteObjectHash, ID>
	
	private ThreadPoolExecutor _threadPool; //Threads für healtcheck & co werden darin ausgeführt 
	
	private final static String GRAPHVIZ_ROOT_NAME = "GriScha";


	public WorkerNodeListWithManagementImpl()
	{
		super();
		_graphOutputFilename = "graph.dot";
		
		_workerNodeHashMap = new HashMap<Integer, IWorkerNode>();
		_workerNodeTreeRoot = createAndPrepareWorkerNodeTreeRoot();
		_workerNodeMetaInformation = new HashMap<Integer, IWorkerNodeMetaInformation>(0);
		_wnIdToRemoteObjectHashMapping = new HashMap<String, Integer>(0);
		
		_graphVizRepresentationOfTree = new GraphViz();
		_graphVizRepresentationOfTree.addln(_graphVizRepresentationOfTree.start_graph());
		
		_threadPool =  new ThreadPoolExecutor(128,
										 	  1024,
											  0L,
											  TimeUnit.SECONDS,
											  new LinkedBlockingQueue<Runnable>());
	}
	
	/**
	 * Erstellt das Wurzelelement für den Baum und fügt zusätzlich
	 * eine default-Site hinzu.
	 * In dieser default-Site sollen alle WorkerNodes abeglegt werden, wo
	 * keine Site eindeutig ermittelt werden konnte.
	 * @return
	 */
	private DefaultMutableTreeNode createAndPrepareWorkerNodeTreeRoot()
	{
		DefaultMutableTreeNode root;
		root = new DefaultMutableTreeNode();
		
		return root;
	}

	
	public IWorkerNode get(int workerNodeId)
	{
		// TODO: WNs autom. entfernen berücksichtigen
		return this.getWorkerNodeHashMap().get(workerNodeId);
	}


	public ArrayList<IWorkerNode> getAllWorkerNodes()
	{
		ArrayList<IWorkerNode> resultList = new ArrayList<IWorkerNode>();

		
		//defekte WN's aus Liste entfernen
		//  __Marco - aufrufen, nachdem alle JobResults vorliegen, um keine Rechenzeit zu blokieren.
		//                Im schlimmsten Fall können dadurch keine Jobs an defekte WorkerNodes geschickt werden,
		//                was aber nicht weiter schlimm ist, denn die meisten WorkerNodes sollten korrekt sein.
//		repairWorkerNodeHashMap();
		
		for(IWorkerNode currentWorkerNode:_workerNodeHashMap.values())
		{
			resultList.add(currentWorkerNode);
		}

		
		return resultList;
	}

	private int getNewWorkerNodeId()
	{
		_totalRegistrations++;
		return _totalRegistrations;
	}

	
	public void add(IWorkerNode workerNode) 
	{
		//ungültige Einträge aus HashMap entfernen
		
		repairWorkerNodeHashMap();

		HashMap<Integer, IWorkerNode> wnHashMap = this.getWorkerNodeHashMap();
		
		//erzeuge neuen eindeutigen Key fuer den WorkerNode
		int newKey = getNewWorkerNodeId();
		
		
		//fuege WorkerNode der lokalen Liste hinzu
		wnHashMap.put(newKey, workerNode);
		
		//verknuepfe ID mit dem RemoteObjectHash des Worker-Nodes
		String wnHash = getWorkerNodeRemoteObjectHash(workerNode);
		_wnIdToRemoteObjectHashMapping.put(wnHash, newKey);
		
		//fuege MetaInformation zum WorkerNode hinzu
		_workerNodeMetaInformation.put(newKey, new WorkerNodeMetaInformationImpl());
		
		
		//WorkerNode wird die ID bekannt gegeben, unter der er
		//in dem WNM verwaltet wird
		try
		{
			workerNode.setId(newKey);
			
			addWorkerNodeToTree(workerNode);
			
			//MetaInformation sammeln
			collectMetaInformation(newKey, workerNode);
		}
		catch (SimonRemoteException e)
		{
			String message = "Aufruf der Methode setID beim WorkerNode fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message,e);
			
			//defekten WorkerNode melden
			removeWorkerNode(workerNode);
		}
		
		
		//Graphviz aktualisieren
//		updateGraphvizRepresentationOfWorkerNodes();
		
		//debug: Baum ausgeben
//		parseTree(true);
		
	}
	
	
	/**
	 * generates/updates MetaInformation of a given WorkerNode
	 * @param workerNode
	 */
	private void collectMetaInformation(int workerNodeId, IWorkerNode workerNode)
	{
		IWorkerNodeMetaInformation metaInformation;
		ISystemInformation wnSystemInformation = null;
		InetAddress[] inetAdresses = null;
		
		metaInformation = _workerNodeMetaInformation.get(workerNodeId);
		
		try
		{
			wnSystemInformation = workerNode.getSystemInformation();
		}
		catch (SimonRemoteException e)
		{
			String message = "SystemInformation vom WorkerNode ("+workerNodeId+") nicht abrufbar";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
			
			//defekten WorkerNode melden
			removeWorkerNode(workerNode);
			
			return; //vorzeitiges Beenden
		}
		
		
		//IP-Adressen setzen
		try
		{
			inetAdresses = wnSystemInformation.getIpAdresses();
			metaInformation.setIpAdresses(inetAdresses);
		}
		catch (SimonRemoteException e)
		{
			String message = "Auslesen der IP-Adressen vom WorkerNode fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
			
			//defekten WorkerNode melden
			removeWorkerNode(workerNode);
		}
	}
	
	
	/**
	 * Durchläuft den WorkerNode-Baum und gibt alle Elemente aus
	 */
	public StringBuffer parseTree(boolean printOut)
	{
		StringBuffer result = new StringBuffer();
		StringBuilder outputMessage = new StringBuilder();
		int countWorkerNodes = 0;
		ArrayList<WorkerNodeToSiteMapping> workerNodeToSiteMapping;
		ArrayList<String> workerNodeTreeMapRepresentation = new ArrayList<String>();
		String currentSiteString;
		
		//WorkerNode-Listen reparieren
		long start = System.currentTimeMillis();
		repairWorkerNodeHashMap();
		_log.debug("Zeitmessung: parseTree.repairWorkerNodeHashMap() took " + String.valueOf(System.currentTimeMillis()-start) + " ms");
		
		start = System.currentTimeMillis();
		repairWorkerNodeTree();
		_log.debug("Zeitmessung: parseTree.repairWorkerNodeTree() took " + String.valueOf(System.currentTimeMillis()-start) + " ms");
		
		outputMessage.append("\n");
		outputMessage.append("\n");
		outputMessage.append("parsetree");
		
		
		//sammle alle Sites mit dazugehörigen WorkerNodes
		start = System.currentTimeMillis();
		workerNodeToSiteMapping = getWorkerNodeToSiteMapping();
		_log.debug("Zeitmessung: parseTree.getWorkerNodeToSiteMapping() took " + String.valueOf(System.currentTimeMillis()-start) + " ms");
		start = System.currentTimeMillis();
		countWorkerNodes = workerNodeToSiteMapping.size();
		_log.debug("Zeitmessung: parseTree.workerNodeToSiteMapping.size() took " + String.valueOf(System.currentTimeMillis()-start) + " ms");

		
		//erzeuge Ausgabe
		start = System.currentTimeMillis();
		workerNodeTreeMapRepresentation = getWorkerNodeTreeMapRepresentation(workerNodeToSiteMapping);
		_log.debug("Zeitmessung: parseTree.getWorkerNodeTreeMapRepresentation(workerNodeToSiteMapping) took " + String.valueOf(System.currentTimeMillis()-start) + " ms");

		//der Ausgabeliste hinzufügen
//		for (String string : workerNodeTreeMapRepresentation)
//		{
//			outputMessage.append(string);
//		}
		String lastSite = "";
		String currentSite = "";
		
		outputMessage.append("\n");
		
		start = System.currentTimeMillis();
		for (int i = 0; i < workerNodeTreeMapRepresentation.size(); i++)
		{
			currentSite = workerNodeToSiteMapping.get(i).getSite().getName();
			if (currentSite.equals(lastSite) == false)
			{
				outputMessage.append("\n\t" + currentSite);
				lastSite = currentSite;
			}
			
			currentSiteString = workerNodeTreeMapRepresentation.get(i);
			outputMessage.append(currentSiteString);
		}
		_log.debug("Zeitmessung: parseTree.for (int i = 0; i < workerNodeTreeMapRepresentation.size(); i++) took " + String.valueOf(System.currentTimeMillis()-start) + " ms");
		
		
		outputMessage.append("\n");
		outputMessage.append("(derzeit angemeldete WorkerNodes: " + countWorkerNodes + ")\n");
		outputMessage.append("\n");
		
		
		//Ausgeben falls so gewollt
		if (printOut=true)
		{
			_log.info(outputMessage.toString());
		}
		
		//Baum speichern
		result = new StringBuffer(outputMessage.toString());
		
		//Graphviz aktualisieren
//		updateGraphvizRepresentationOfWorkerNodes();
		
		return result;
	}
	
	
	
	private ArrayList<String> getWorkerNodeTreeMapRepresentation(ArrayList<WorkerNodeToSiteMapping> workerNodeToSiteList)
	{
		ArrayList<String> result = new ArrayList<String>();
		long timeout = 10000; //10sek
		
		ThreadPoolExecutor pool = _threadPool;
		
		//Arbeitsaufträge erstellen
		LinkedList<Callable<String>> serviceList = new LinkedList<Callable<String>>();
		IWorkerNode currentWorkerNode;
		ISite currentSite;
		for (WorkerNodeToSiteMapping mapping : workerNodeToSiteList)
		{
			serviceList.add(new WorkerNodeTreeRepresentation(mapping.getWorkerNode(), mapping.getSite()));
		}

		//vorzeitiges Beenden, falls keine Elemente in der ServiceList enthalten sind
		if (serviceList.size()<=0) return result;
		
		try
		{
			//Arbeitsaufträge starten und auf Ergebnis warten
			List<Future<String>> futureList;
			futureList = pool.invokeAll(serviceList, timeout, TimeUnit.MILLISECONDS); //warte max. 10sek auf die Ergebnisse
	
			//Ergebnisse einsammeln
			for (int i = 0; i < futureList.size(); i++)
			{
				Future<String> currentSiteTestResult = futureList.get(i);
				
				result.add(currentSiteTestResult.get());
			}
		}
		catch (InterruptedException e)
		{
			_log.error("Generierung der Graphviz-Representation für WorkerNodes einer Site fehlgeschlagen (InterruptedException)");
		}
		catch (ExecutionException e)
		{
			_log.error("Generierung der Graphviz-Representation für WorkerNodes einer Site fehlgeschlagen (ExecutionException)");
		}
		
		return result;
	}
	
	
	
	
	/**
	 * Aktualisiert das Erzeugungsskript für Graphviz, in welchem alle momentan
	 * angemeldeten Workernodes enthalten sind.  
	 */
	public void updateGraphvizRepresentationOfWorkerNodes()
	{
		//update Graphviz-Graph
		_graphVizRepresentationOfTree = getGraphvizRepresentation();
		
		FileWriter outputFile;
		int workerNodeCount = getAllWorkerNodes().size();
		
		//write graph to file
		try
		{
//			outputFile = new FileWriter("gridgraph_" + workerNodeCount + "_" + System.currentTimeMillis() + ".dot");
			outputFile = new FileWriter(_graphOutputFilename);
			saveTreeRepresentationAsDotFile(outputFile);
		}
		catch (IOException e)
		{
			String message = "Erzeugung der GraphViz-Ausgabe fehlgeschlagen"; 
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
			
		}
	}
	

	private GraphViz getGraphvizRepresentation()
	{
		long timeout = 5000; //5sec
		GraphViz graph = new GraphViz();
		ArrayList<String> sites = new ArrayList<String>();
		ArrayList<String> workerNodes = new ArrayList<String>();
		ArrayList<ISite> allSitesList = new ArrayList<ISite>();
		ArrayList<IWorkerNode> allWorkerNodeList = new ArrayList<IWorkerNode>();
		String currentSiteName = "";
		
		//neue GraphViz-Beschreibung erstellen
		graph.addln(graph.start_graph());

		//layout
		graph.addln("graph[bgcolor=\"black\"];");
		graph.addln("edge[style=\"bold\",color=\"#77b900\"];");
		graph.addln("node [style=\"filled\",color=\"#77b900\", fontsize=\"1\", fixedsize=\"false\"];");
		
		graph.addln("ratio=auto;");
		graph.addln("ranksep=3;");
		
		
		//Graphviz: RootElement hinzufügen
		String graphCenterName = GRAPHVIZ_ROOT_NAME;
		graph.addln("\"" + graphCenterName + "\" [style=\"filled\",color=\"#77b900\", fontsize=\"300.0\", fixedsize=\"false\", fontcolor=\"#E4FFAF\"];");
		
		
		//Liste mit allen Sites erstellen
		allSitesList = getAllSites();

		
		//Site-Ausgabe erzeugen lassen
		sites = getSiteGraphvizRepresentation(allSitesList, GRAPHVIZ_ROOT_NAME, timeout);
		
		//Sites hinzufügen
		for(String currentSiteRepresentation : sites)
		{
			//Zeilenumbruch bei jedem Auftauchen eines Semikolons hinzufügen
			graph.addln(currentSiteRepresentation.replace(";", ";\n"));
		}

		
		//RootElement auf alle Sites zeigen lassen
//		for(ISite currentSite : allSitesList)
//		{
//			currentSiteName = currentSite.getName();
//			graph.addln("\"" + GRAPHVIZ_ROOT_NAME + "\" -> \"" + currentSiteName + "\";");
//		}
		
	
		//WorkerNode-Ausgabe erzeugen lassen
		ArrayList<WorkerNodeToSiteMapping> workerNodeToSiteList = getWorkerNodeToSiteMapping();	//Site mit WorkerNode verbinden
		workerNodes = getWorkerNodeGraphvizRepresentation(workerNodeToSiteList);
		
		//WorkerNodes hinzufügen
		for(String currentWorkerNodeRepresentation : workerNodes)
		{
			//Zeilenumbruch bei jedem Auftauchen eines Semikolons hinzufügen
			graph.addln(currentWorkerNodeRepresentation.replace(";", ";\n"));
		}
		
		//Graph abschließen
		graph.addln(graph.end_graph());
		
		return graph;
	}
	
	
	/**
	 * Verknpüft WorkerNode und Site in einer Hashmap
	 * @return
	 */
	private ArrayList<WorkerNodeToSiteMapping> getWorkerNodeToSiteMapping()
	{
		ArrayList<WorkerNodeToSiteMapping> workerNodeToSiteList = new ArrayList<WorkerNodeToSiteMapping>();
		
		
		Enumeration<ISite> allSites = _workerNodeTreeRoot.children();
		ISite site = null;
		DefaultMutableTreeNode siteNode;
		ArrayList<ISite> listOfSites = new ArrayList<ISite>(0);
		IWorkerNode currentWorkerNode;
		DefaultMutableTreeNode currentWorkerNodeNode;
		
		//durchlaufe alle Sites und ordner der Map die entsprechenden WorkeNodes zu
		while (allSites.hasMoreElements())
		{
			siteNode = (DefaultMutableTreeNode) allSites.nextElement();
			Object siteObject = siteNode.getUserObject();
			
			if (ISite.class.isInstance(siteObject)==true)
			{
				site = (ISite) siteObject;
				listOfSites.add(site);
			}
			else
			{
				_log.error("Fehlerhafte Typen (ISite) in Baumstruktur");
			}
			
			
			//get all WorkerNodes of current Site
			Enumeration<IWorkerNode> allWorkerNodesOfASite = siteNode.children();

			
			//iteriere durch alle WorkerNodes
			while (allWorkerNodesOfASite.hasMoreElements())
			{
				//WorkerNodes in einer Liste speichern
				currentWorkerNodeNode = (DefaultMutableTreeNode) allWorkerNodesOfASite.nextElement();
				currentWorkerNode = (IWorkerNode) currentWorkerNodeNode.getUserObject();
				
				//füge der Liste hinzu
				if (site!= null && currentWorkerNode != null)
				{
					workerNodeToSiteList.add(new WorkerNodeToSiteMapping(currentWorkerNode, site));
				}
				else
				{
					_log.error("getWorkerNodeSiteHashMap(): Aktuelle Site ist NULL und kann deshalb nicht zur HashMap hinzugefügt werden");
				}
			}
		}
		
		return workerNodeToSiteList;
	}
	
	
	private ArrayList<String> getSiteGraphvizRepresentation(ArrayList<ISite> allSitesList, String graphRootName, long timeout)
	{
		ArrayList<String> result = new ArrayList<String>();
		String currentString = "";
		ThreadPoolExecutor pool = _threadPool;
		
		//Arbeitsaufträge erstellen
		LinkedList<Callable<String>> serviceList = new LinkedList<Callable<String>>();
		for (ISite site : allSitesList)
		{
			serviceList.add(new SiteGraphvizRepresentation(site, graphRootName));
		}

		//vorzeitiges Beenden, falls keine Elemente in der ServiceList enthalten sind
		if (serviceList.size()<=0) return result;
		
		try
		{
			//Arbeitsaufträge starten und auf Ergebnis warten
			List<Future<String>> futureList;
			futureList = pool.invokeAll(serviceList, timeout, TimeUnit.MILLISECONDS);
	
			//Ergebnisse einsammeln
			for (int i = 0; i < futureList.size(); i++)
			{
				Future<String> currentSiteTestResult = futureList.get(i);
				if (currentSiteTestResult==null)
				{
					_log.error("getSiteGraphvizRepresentation(): Generierung der TreeMap-Representation für eine Site fehlgeschlagen");
				}
				else
				{
					try
					{
						currentString = currentSiteTestResult.get();
						result.add(currentString);
					}
					catch (ExecutionException e)
					{
						_log.error("getSiteGraphvizRepresentation(): Generierung der TreeMap-Representation für Site fehlgeschlagen (ExecutionException) (currentString=" + currentString + ")", e);
					}
				}
			}
		}
		catch (InterruptedException e)
		{
			_log.error("getSiteGraphvizRepresentation(): Generierung der TreeMap-Representation für Site fehlgeschlagen (InterruptedException)", e);
		}
		
		
		return result;
	}
	
	
	
	private ArrayList<String> getWorkerNodeGraphvizRepresentation(ArrayList<WorkerNodeToSiteMapping> workerNodeToSiteList)
	{
		ArrayList<String> result = new ArrayList<String>();
		
		ThreadPoolExecutor pool = _threadPool;
		
		//Arbeitsaufträge erstellen
		LinkedList<Callable<String>> serviceList = new LinkedList<Callable<String>>();
		IWorkerNode currentWorkerNode;
		ISite currentSite;
		for (WorkerNodeToSiteMapping mapping : workerNodeToSiteList)
		{
			serviceList.add(new WorkerNodeGraphvizRepresentation(mapping));
		}

		//vorzeitiges Beenden, falls keine Elemente in der ServiceList enthalten sind
		if (serviceList.size()<=0) return result;
		
		try
		{
			//Arbeitsaufträge starten und auf Ergebnis warten
			List<Future<String>> futureList;
			futureList = pool.invokeAll(serviceList);
	
			//Ergebnisse einsammeln
			for (int i = 0; i < futureList.size(); i++)
			{
				Future<String> currentSiteTestResult = futureList.get(i);
				
				result.add(currentSiteTestResult.get());
			}
		}
		catch (InterruptedException e)
		{
			_log.error("Generierung der Graphviz-Representation für WorkerNodes einer Site fehlgeschlagen (InterruptedException)");
		}
		catch (ExecutionException e)
		{
			_log.error("Generierung der Graphviz-Representation für WorkerNodes einer Site fehlgeschlagen (ExecutionException)");
		}
		
		return result;
	}
	
	
	
	/**
	 * Sammelt Information über ein WorkerNodes und arbeitet diese so auf,
     * dass daraus die Repräsentation für parseTree() erzeugt wird.
     * 
	 * @param workerNodes
	 * @return
	 */
	private ArrayList<String> getWorkerNodeTreeRepresentation(ArrayList<IWorkerNode> workerNodes, ISite siteOfWorkerNodes)
	{
		ArrayList<String> result = new ArrayList<String>();
		ThreadPoolExecutor pool = _threadPool;
		
		//Arbeitsaufträge erstellen
		LinkedList<Callable<String>> serviceList = new LinkedList<Callable<String>>();
		for (IWorkerNode workerNode : workerNodes)
		{
			serviceList.add(new WorkerNodeTreeRepresentation(workerNode, siteOfWorkerNodes));
		}

		//vorzeitiges Beenden, falls keine Elemente in der ServiceList enthalten sind
		if (serviceList.size()<=0) return result;
		
		//Arbeitsaufträge vorbereiten (sofern überhaupt vorhanden) --> jeder WorkerNode wird in einem extra Thread getestet
//		pool = new ThreadPoolExecutor(serviceList.size(),
// 								 	  serviceList.size(),
//									  0L,
//									  TimeUnit.SECONDS,
//									  new LinkedBlockingQueue<Runnable>());

		try
		{
			//Arbeitsaufträge starten und auf Ergebnis warten
			List<Future<String>> futureList;
			futureList = pool.invokeAll(serviceList);
	
			//Ergebnisse einsammeln
			for (int i = 0; i < futureList.size(); i++)
			{
				Future<String> currentWorkerNodeTestResult = futureList.get(i);
				
				result.add(currentWorkerNodeTestResult.get());
			}
		}
		catch (InterruptedException e)
		{
			_log.error("Generierung der TreeMap-Representation für WorkerNodes einer Site fehlgeschlagen (InterruptedException)");
		}
		catch (ExecutionException e)
		{
			_log.error("Generierung der TreeMap-Representation für WorkerNodes einer Site fehlgeschlagen (ExecutionException)");
		}
		
		
		return result;
	}
	
	
	//entfernt einen WorkerNode von einer gegebenen Site
	private void removeWorkerNodeFromSite(DefaultMutableTreeNode siteNode,
										  DefaultMutableTreeNode workerNodeNode,
										  int workerNodeId)
	{
		try
		{
			siteNode.remove(workerNodeNode);
			_log.info("Defekten WorkerNode (id=" + workerNodeId + ") aus Baum entfernt");
		}
		catch (Exception e)
		{
			String message1 = "WorkerNode ist keine Element der Site und kann deswegen nicht gelöscht werden";
			_log.error(message1);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message1, e);
		}

	}

	
	
	/**
	 * Fuegt den WorkerNode in den Baum ein
	 * @param workerNode
	 */
	private void addWorkerNodeToTree(IWorkerNode workerNode)
	{
		//erzeuge Knoten mit Worker-Node
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(workerNode);
		
		String siteName = "";
		
		//bestimme Site des Worker-Nodes
		try
		{
			siteName = workerNode.getSystemInformation().getSite();
			
			DefaultMutableTreeNode siteForWorkerNode;
			siteForWorkerNode = getSiteNodeForWorkerNode(siteName);
		
			//Füge Knoten der passenden Site zu
			addWorkerNodeToSite(siteForWorkerNode, node);
		}
		catch (SimonRemoteException e)
		{
			String message = "Ermittlung des WorkerNode Site Namens fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
			
			//defekten WorkerNode melden
			removeWorkerNode(workerNode);
		}
	}
	
	
	/**
	 * Anhand des WorkerNodes-Sitenamens wird ermittelt, ob eine entsprechende Site
	 * bereits im Baum exitiert. In diesem Fall wird der WorkerNode daran angehangen.
	 * Im andern Fall wird ein neuer SiteKnoten erstellt und der WorkerNode
	 * daran angehangen.
	 * @param siteName Sitename, den der WorkerNode ermittelt hat
	 * @return
	 */
	private DefaultMutableTreeNode getSiteNodeForWorkerNode(String siteName)
	{
		DefaultMutableTreeNode siteNode;
		DefaultMutableTreeNode result = null;
		Enumeration<ISite> allSiteNodes = _workerNodeTreeRoot.children();
		ISite currentSite;

		//wenn kein SiteName angegeben ist wird er zur DefaultSite hinzugefügt
		if (siteName.equals("".toString()))
		{
//			return (DefaultMutableTreeNode) _workerNodeTreeRoot.getChildAt(0); //default Site
			return getDefaultSite();
		}
		
		
		//iteriere über alle Sites
		while (allSiteNodes.hasMoreElements())
		{
			siteNode = (DefaultMutableTreeNode) allSiteNodes.nextElement();
			currentSite = (ISite) siteNode.getUserObject();
			
			//aktuelle Site ist die gesuchte Site?
//			if (currentSite.getName()==siteName)
			if (siteName.equals(currentSite.getName()))
			{
				result = siteNode;
				return result;
			}
		}
		
		//kein Node gefunden --> erstell neuen Node mit dem angegeben SiteNamen
		ISite newSite = new SiteNormalImpl(siteName);
		result = new DefaultMutableTreeNode(newSite);
		
		//füge neue Site der Baumliste hinzu
		_workerNodeTreeRoot.add(result);
		
		return result;
	}
	

	
	
	/**
	 * Default Site zurückgegebn und ggf erzeugen falls diese noch nicht existiert
	 * @return
	 */
	private DefaultMutableTreeNode getDefaultSite()
	{
		DefaultMutableTreeNode currentSiteNode;
		Enumeration allSiteNodes = _workerNodeTreeRoot.children();
		DefaultMutableTreeNode defaultSiteNode = null;
		
		//prüfe, ob DefaultSite bereits existiert
		while (allSiteNodes.hasMoreElements())
		{
			currentSiteNode = (DefaultMutableTreeNode) allSiteNodes.nextElement();
			

			//wenn die aktuelle Site eine Instanz von SiteUnknownImpl ist dann gebe diese als Treffer zurück
			Object siteObject = currentSiteNode.getUserObject();
			if ( (siteObject.getClass() == SiteUnknownImpl.class) == true)
			{
				return currentSiteNode;
			}
		}
		
		//erzeuge DefaultSite, da diese scheinbar noch nicht existiert
		defaultSiteNode = createDefaultSite();
		_workerNodeTreeRoot.add(defaultSiteNode);
		
		return defaultSiteNode;
		
	}
	
	
	//DefaultSite (Unknown) anlegen
	private DefaultMutableTreeNode createDefaultSite()
	{
		ISite defaultSite = new SiteUnknownImpl();
		DefaultMutableTreeNode defaultSiteNode;
		
		defaultSiteNode = new DefaultMutableTreeNode(defaultSite);
		
		return defaultSiteNode;
	}
	
	
	/**
	 * Fuege ein WorkerNode einer Site hinzu.
	 * @param siteForWorkerNode enthaelt Site
	 * @param node enthaelt WorkerNode
	 */
	private void addWorkerNodeToSite(DefaultMutableTreeNode siteForWorkerNode, DefaultMutableTreeNode node)
	{
		//hole Site-Objekt aus Knoten
		Object siteObject = siteForWorkerNode.getUserObject();
		if (ISite.class.isInstance(siteObject)==true)
		{
			ISite site = (ISite) siteObject;
			
			//hole WorkerNode aus Knoten
			Object workerNodeObject = node.getUserObject();
			if (IWorkerNode.class.isInstance(workerNodeObject))
			{
				IWorkerNode workerNode = (IWorkerNode) workerNodeObject;
				
				//Wenn die Zeit noch keinen WorkerNodes hat, dann definiere
				//den Node (WorkerNode) automatisch als MasterWorkerNode
				if (site.hasMasterWorkerNode()==false)
				{
					site.setMasterWorkerNode(workerNode);
					try
					{
						//TODO _Marco : WorkerNodeID lokal aus HashMap bestimmen!
						_log.info("WorkerNode (id=" + workerNode.getId() + ") zum Master ernannt (site=" + site.getName() + ")");
					}
					catch (SimonRemoteException e)
					{
						String message = "Ausleser der WorkerNode-ID fehlgeschlagen";
						_log.error(message);
						if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
						
						//defekten WorkerNode melden
						removeWorkerNode(workerNode);
					}
				}
				
				//fuege WorkerNode der Site hinzu
				siteForWorkerNode.add(node);
			}
			else
			{
				_log.error("Fehlerhafte Typen (IWorkerNode) in Baumstruktur");
			}
			
		}
		else
		{
			_log.error("Fehlerhafte Typen (ISite) in Baumstruktur");
		}
	}
	
	
	
	
	public void remove(int id) 
	{
		HashMap<Integer, IWorkerNode> wnHashMap = this.getWorkerNodeHashMap();
		
		// TODO: WNs autom. entfernen berücksichtigen
		if(wnHashMap.containsKey(id))
		{
			wnHashMap.remove(id);
		}
	}


	public int size()
	{
		HashMap<Integer, IWorkerNode> wnHashMap = this.getWorkerNodeHashMap();
		return wnHashMap.size();
	}

// wird in AbtrakteKlasse bereits zur Verfügung gestellt
//	public HashMap<Integer, IWorkerNode> getWorkerNodeHashMap()
//	{
//		HashMap<Integer, IWorkerNode> wnHashMap = getWorkerNodeHashMap();
//		return wnHashMap;
//	}
	

//	// wird in AbtrakteKlasse bereits zur Verfügung gestellt
//	public void removeWorkerNode(int id)
//	{
//		HashMap<Integer, IWorkerNode> wnHashMap = this.getWorkerNodeHashMap();
//		
//		// TODO: WNs autom. entfernen berücksichtigen
//		if(wnHashMap.containsKey(id))
//		{
//			wnHashMap.remove(id);
//			this._log.info("WorkerNode ("+id+") deleted.");
//		}		
//	}

	
	
	synchronized public void removeWorkerNode(IWorkerNode workerNode)
	{

		Date start = new Date();
		
		//vorzeitiges Beenden, falls der übergebene WorkerNode NULL ist
		if (workerNode==null)
		{
			return;
		}
		
		int debugId = -1;
		try
		{
			String wnHash = getWorkerNodeRemoteObjectHash(workerNode);
			Integer wnId = _wnIdToRemoteObjectHashMapping.get(wnHash);
			
			//workerNode exitsiert nicht mehr in HashMap
			if (_wnIdToRemoteObjectHashMapping.containsKey(wnHash) == false ||
				wnId == null)
			{
				_log.error("Löschen des WorkerNode (hash=" + wnHash +") feghlgeschlagen, da dieser bereits nicht mehr existiert");
				return;
			}
			
			_log.info("WorkerNode (id=" + wnId + ") soll entfernt werden...");
			
			debugId = wnId;
			removeWorkerNodeFromAllMaps(wnHash, wnId);
			Date stop = new Date();
			System.err.println("Zeitmessung: removeWorkerNode(): " + String.valueOf(stop.getTime()-start.getTime()) + "ms");
			
			//neue map ausgeben
//			parseTree(true);
		}
		catch (Exception e)
		{
			String message = "Entfernen des WorkerNodes (id=" + debugId + ") fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
		}
		
		//Graphviz aktualisieren
//		updateGraphvizRepresentationOfWorkerNodes();
	}
	

	/**
	 * Loescht saemtliche Verweise eines WorkerNodes in den vorhandenen Datenstrukturen
	 * @param wnHash
	 * @param wnId
	 */
	synchronized private void removeWorkerNodeFromAllMaps(String wnHash, int wnId)
	{
		removeWorkerNodeMetaInformation(wnId); //aus MetaListe entfernen
		
		removeWorkerNodeFromTree(wnHash, wnId); // aus Baum entfernen
		
		removeWorkerNodeFromHashMap(wnId); //aus Hash-Map entfernen
		
		removeWorkerNodeFromSite(wnHash, wnId);

		//TODO _assignedWorkerNodeMap berücksichtigen
		//TODO _assignedWorkerNodeList berücksichtigen
		
		
		//<wnHash,wnId> Paar aus Map loeschen
		_log.info("Loesche Mapping <" + wnHash + ";" + wnId + "> aus HashMap");
		_wnIdToRemoteObjectHashMapping.remove(wnHash);
	}
	
	
	
	/**
	 * WorkerNode aus Baum entfernen
	 * @param wnHash
	 * @param wnId
	 */
	private void removeWorkerNodeFromTree(String wnHash, int wnId)
	{
		ArrayList<IWorkerNode> workerNodes = new ArrayList<IWorkerNode>();
		Enumeration<ISite> allSiteNodes = _workerNodeTreeRoot.children();
		ISite currentSite;
		DefaultMutableTreeNode siteNode;
		IWorkerNode currentWorkerNode;
		String currentWnHash;

		//iteriere über alle Sites
		while (allSiteNodes.hasMoreElements())
		{
			siteNode = (DefaultMutableTreeNode) allSiteNodes.nextElement();
			currentSite = (ISite) siteNode.getUserObject();
			
			//get all WorkerNodes of current Site
			Enumeration<IWorkerNode> allWorkerNodesOfASite = siteNode.children();
			
			//iteriere über alle WorkerNodes der aktuellen Site
			while (allWorkerNodesOfASite.hasMoreElements())
			{
				//WorkerNode-Objekt  holen
				DefaultMutableTreeNode workerNodeNode = (DefaultMutableTreeNode) allWorkerNodesOfASite.nextElement();
				currentWorkerNode = (IWorkerNode) workerNodeNode.getUserObject();
				
				//hash vom Worker-Node Objekt erstellen
				currentWnHash = getWorkerNodeRemoteObjectHash(currentWorkerNode);
				
				//bei Gleichheit den aktuellen Knoten der Site loeschen und methode verlassen
				if(currentWnHash.equals(wnHash))
				{
					siteNode.remove(workerNodeNode); //gefundenen Knoten aus Baumstruktur entfernen
					_log.info("WorkerNode (id=" + wnId + ") aus Baumstruktur entfernt");
					_log.debug("WorkerNode (id=" + wnId + ", hash=" + wnHash + ") aus Baumstruktur entfernt");
					
					//falls MasterWorkerNode auch gleich das Flag bei der Site ändern
					IWorkerNode siteMasterWorkerNode = currentSite.getMasterWorkerNode();
					String siteMasterWorkerNodeHash = getWorkerNodeRemoteObjectHash(siteMasterWorkerNode);
					if (wnHash.equals(siteMasterWorkerNodeHash)==true)
					{
						currentSite.setMasterWorkerNode(null);
					}
					
					repairWorkerNodeTree(); //Konsistenz im Baum herstellen
					return;
				}
			}
		}
		
		
		//Graphviz aktualisieren
//		updateGraphvizRepresentationOfWorkerNodes();
	}
	
	
	
	/**
	 * WorkerNode aus MetaInformation-Liste entfernen
	 * @param wnId
	 */
	private void removeWorkerNodeMetaInformation(int wnId)
	{
		try
		{
			_workerNodeMetaInformation.remove(wnId);
		}
		catch (Exception e)
		{
			_log.error("Entfernen des WorkerNodes (id=" + wnId + " aus der MetaInformationen-Liste gescheitert");
		}
	}
	
	
	
	/**
	 * Legt einen neuen MasterWorkerNode fuer eine Site fest,
	 * falls der alte nicht mehr existiert.
	 * @param wnHash
	 */
	private void repairSiteMasterWorkerNode(String wnHash)
	{
		boolean wnIsMasterWorkerNode = false;
		ISite site = null;
		IWorkerNode newMasterWorkerNode = null;
		
		wnIsMasterWorkerNode = isMasterWorkerNode(wnHash);
		
		//Site muss neuen Master zugewiesen werden?
		if (wnIsMasterWorkerNode==true)
		{
			
//			_log.info("Site <" + siteName + "> hat MasterWorkerNode verloren");
			
			site = getSiteOfMasterWorkerNodeHash(wnHash);
			

			//Site einen neuen MasterWorkerNode zuweisen
			setNewMasterNodeForSite(site);
		}
		
	}
	
	
	/**
	 * Eine Site einen neuen MasterWorkeNode zuweisen
	 * @param site
	 * @param newMasterWorkerNode
	 */
	private void setNewMasterNodeForSite(ISite site)
	{
		//weise der Site den neuen MasterWorkerNode zu
		try
		{
			String siteName;
			IWorkerNode newMasterWorkerNode;
			
			siteName = site.getName();
			newMasterWorkerNode = getNextWorkerNodeCandidate(site);
			
			//teste, ob ein MasterWorkerNode bereitsteht
			if (newMasterWorkerNode==null)
			{
				_log.info("Site <" + siteName +"> fehlt derzeit ein MasterWorkerNode");
				site.setMasterWorkerNode(null);
				return; //vorzeitiges Beenden
			}
			
			site.setMasterWorkerNode(newMasterWorkerNode);
			_log.info("Site <" + siteName + "> wurde ein neuer MasterWorkerNode zugewiesen");
			String newWnHash;
			int newWnId;
			newWnId = newMasterWorkerNode.getId();
			newWnHash = getWorkerNodeRemoteObjectHash(newMasterWorkerNode);
			_log.debug("Site <" + siteName + "> wurde ein neuer MasterWorkerNode (id=" + newWnId +
					  ", hash=" + newWnHash + ") zugewiesen");
		}
		catch (SimonRemoteException e)
		{
			String message = "Zuweisung eines neuen MasterWorkerNodes fuer die Site <> fehlgeschlagen";
			_log.error(message);
			
			site.setMasterWorkerNode(null);
			_log.info("Site <> verfuegt im Moment ueber keinen MasterWorkerNode");
		}
	}
	
	
	/**
	 * Entfernen einen bestimmen WorkerNode aus seinem Site-Baum
	 * @param wnHash
	 */
	private void removeWorkerNodeFromSite(String wnHash, int wnId)
	{
		DefaultMutableTreeNode currentSiteNode;
		Enumeration<ISite> allSiteNodes = _workerNodeTreeRoot.children();
		ISite currentSite;
		Object currentSiteObj;
		String currentWnHash = "";
		IWorkerNode currentWorkerNode;
		
		DefaultMutableTreeNode siteNode;
		//iteriere über alle Sites
		while (allSiteNodes.hasMoreElements())
		{
			siteNode = (DefaultMutableTreeNode) allSiteNodes.nextElement();
			currentSite = (ISite) siteNode.getUserObject();
			
			//get all WorkerNodes of current Site
			Enumeration<IWorkerNode> allWorkerNodesOfASite = siteNode.children();
			
			//iteriere über alle WorkerNodes der aktuellen Site
			while (allWorkerNodesOfASite.hasMoreElements())
			{
				//WorkerNode-Objekt holen
				DefaultMutableTreeNode workerNodeNode = (DefaultMutableTreeNode) allWorkerNodesOfASite.nextElement();
				currentWorkerNode = (IWorkerNode) workerNodeNode.getUserObject();
				
				//hash generieren
				currentWnHash = getWorkerNodeRemoteObjectHash(currentWorkerNode);
				
				//wurde der passende WorkerNode gefunden?
				if (currentWnHash.equals(wnHash))
				{
					siteNode.remove(workerNodeNode);
					_log.info("WorkerNode (id=" + wnId + ") aus Baum entfernt");
					_log.debug("WorkerNode (id=" + wnId + ", hash=" + wnHash + ") aus Baum entfernt");
					
					
					repairSiteMasterWorkerNode(wnHash); //ggf. neuen MasterNode ernennen, falls der zu
                    									//Loeschende WN der Master der Site war
					
					return;
				}
			}
		}
			
	}
	
	
	/**
	 * Bestimmt den naechstbesten, freien noch nicht zugewiesenen
	 * WorkerNode einer bestimmten Site, der zum MasterWorkerNode
	 * ernannen werden kann
	 * @param site
	 * @return null falls kein passender WorkerNode gefunden werden konnte
	 */
	private IWorkerNode getNextWorkerNodeCandidate(ISite site)
	{
		IWorkerNode newMasterWorkerNode = null;
		ArrayList<IWorkerNode> allWorkerNodes;
		
		//hole alle WorkerNodes der entsprechenden Site
		allWorkerNodes = getAllWorkerNodesBySite(site);
		
		//TODO __Marco : Parallelisieren
		//iteriere durch alle WorkerNodes
		for (IWorkerNode currentWorkerNode : allWorkerNodes)
		{
			//WorkerNode defekt?
			if (isCurrupt(currentWorkerNode)==false) //nein, voll funktionsfaehig
			{
				//merke validen WorkerNode
				newMasterWorkerNode = currentWorkerNode;
				//beende vorzeitig die Schleife
				break;
			}
		}	
		
		return newMasterWorkerNode;
	}

	
	/**
	 * Prueft ein WorkerNode auf Korrektheit.
	 * @param workerNodeToTest
	 * @return true wenn der WorkerNode defekt ist
	 */
	private boolean isCurrupt(IWorkerNode workerNodeToTest)
	{
		int workerNodeId;
		
		if (workerNodeToTest==null) return true;
		
		try
		{
			workerNodeId = workerNodeToTest.getId();
		}
		catch (SimonRemoteException e)
		{
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Loescht alle korrupten Worker-Nodes aus dem Baum _workerNodeTreeRoot
	 * @return false wenn mindestens ein Fehler gefunden wurde  
	 */
	private boolean repairWorkerNodeTree()
	{
		boolean errorFound = false;
		
		DefaultMutableTreeNode siteNode;
		ISite site;
		IWorkerNode currentWorkerNode;
		int siteMasterWorkerNodeHash;
		ArrayList<IWorkerNode> workerNodesToTest = new ArrayList<IWorkerNode>(); //diese WorkerNodes sollen getestet werden
		ArrayList<Integer> curruptWorkerNodesIndexes = new ArrayList<Integer>(); //enthält Indexe der defekten WorkerNodes aus workerNodesToTest
		ArrayList<DefaultMutableTreeNode> workerNodesToTest_treeNode = new ArrayList<DefaultMutableTreeNode>(); //dazugehörige TreeNode des WorkerNodes
		long timeout = 5000; //5sec
		
		//hole Liste mit Sites
		Enumeration<ISite> allSites = _workerNodeTreeRoot.children();
		
		
		//iterieren ueber alle Sites
		//TODO __Marco - Durchlauf der Sites parallelisieren
		while (allSites.hasMoreElements())
		{
			//hole Site-Objekt
			siteNode = (DefaultMutableTreeNode) allSites.nextElement();
			site = (ISite) siteNode.getUserObject();
			
			//weise Site einen neuen MasterWorkerNode zu falls der gesetzte fehlerhaft ist
			if (site.hasMasterWorkerNode()==false)
			{
				setNewMasterNodeForSite(site);
				errorFound = true;
			}
			
			//teste alle angehangenen WorkerNodes
			Enumeration<IWorkerNode> allWorkerNodesOfASite = siteNode.children(); //get all WorkerNodes of current Site
			while (allWorkerNodesOfASite.hasMoreElements())
			{
				DefaultMutableTreeNode workerNodeNode = (DefaultMutableTreeNode) allWorkerNodesOfASite.nextElement();
				
				currentWorkerNode = (IWorkerNode) workerNodeNode.getUserObject();
			
				//merke WorkerNodes und seine zugehörige TreeNode
				workerNodesToTest.add(currentWorkerNode);
				workerNodesToTest_treeNode.add(workerNodeNode);
			}
			
			
			//bestimme defekte WorkerNodes
			curruptWorkerNodesIndexes = getCurruptWorkerNodes(workerNodesToTest, timeout);
			
			
			//lösche defekte WorkerNode-Nodes
			for (Integer index : curruptWorkerNodesIndexes)
			{
				_log.debug("Defekten WorkerNode (wnhash=" + getWorkerNodeRemoteObjectHash(workerNodesToTest.get(index)) + ") gefunden");
				removeWorkerNodeFromSite(siteNode, workerNodesToTest_treeNode.get(index), -1);
			}
			
		}
		
		return !errorFound;
	}
	
	
	/**
	 * Liste aller Sites des WNM
	 */
	public ArrayList<ISite> getAllSites()
	{
		Enumeration<ISite> allSites = _workerNodeTreeRoot.children();
		ISite site;
		DefaultMutableTreeNode siteNode;
		ArrayList<ISite> listOfSites = new ArrayList<ISite>(0);
		
		
		while (allSites.hasMoreElements())
		{
			siteNode = (DefaultMutableTreeNode) allSites.nextElement();
			Object siteObject = siteNode.getUserObject();
			
			if (ISite.class.isInstance(siteObject)==true)
			{
				site = (ISite) siteObject;
				listOfSites.add(site);
			}
			else
			{
				_log.error("Fehlerhafte Typen (ISite) in Baumstruktur");
			}
			
		}

		return listOfSites;
	}
	

	

	public ArrayList<IWorkerNode> getAllWorkerNodesBySite(ISite site)
	{
		DefaultMutableTreeNode currentSiteNode;
		ArrayList<IWorkerNode> workerNodes = new ArrayList<IWorkerNode>();
		ArrayList<ISite> listOfSites = new ArrayList<ISite>();
		Enumeration<ISite> allSiteNodes = _workerNodeTreeRoot.children();
		ISite currentSite;
		Object currentSiteObj;
		
		DefaultMutableTreeNode siteNode;
		//iteriere über alle Sites
		while (allSiteNodes.hasMoreElements())
		{
			siteNode = (DefaultMutableTreeNode) allSiteNodes.nextElement();
			currentSite = (ISite) siteNode.getUserObject();
			
			//aktuelle Site ist die gesuchte Site?
			if (currentSite.equals(site)==true)
			{
				//get all WorkerNodes of current Site
				Enumeration<IWorkerNode> allWorkerNodesOfASite = siteNode.children();
				
				//WorkerNodes der return-Liste hinzufügen
				while (allWorkerNodesOfASite.hasMoreElements())
				{
					DefaultMutableTreeNode workerNodeNode = (DefaultMutableTreeNode) allWorkerNodesOfASite.nextElement();
					
					// TODO: WNs autom. entfernen berücksichtigen
					workerNodes.add((IWorkerNode) workerNodeNode.getUserObject());
				}
				
				//vorzeitiges Beenden
				return workerNodes;
			}
			
		}

		return workerNodes;
	}
	
	
	
	/**
	 * Liefert alle WorkerNodes einer bestimmten Site zurück
	 * @param siteNode
	 * @return
	 */
	private ArrayList<IWorkerNode> getWorkerNodesBySiteFromTree(DefaultMutableTreeNode siteNode)
	{
		ArrayList<IWorkerNode> workerNodes = new ArrayList<IWorkerNode>();
		Enumeration<IWorkerNode> allWorkerNodesOfASite = siteNode.children();
		IWorkerNode currentWorkerNode;
		
		while (allWorkerNodesOfASite.hasMoreElements())
		{
			currentWorkerNode = allWorkerNodesOfASite.nextElement();
			workerNodes.add(currentWorkerNode);
		}
		
		return workerNodes;
	}

	
	/**
	 * Erkennt, ob ein WorkerNode ein MasterWorkerNode ist 
	 * @param wnHash
	 * @return
	 */
	private boolean isMasterWorkerNode(String wnHash)
	{
		boolean result = false;
		DefaultMutableTreeNode siteNode;
		ISite site;
		IWorkerNode workerNode;
		String siteMasterWorkerNodeHash;
		
		//hole Liste mit Sites
		Enumeration<ISite> allSites = _workerNodeTreeRoot.children();
		
		
		//iterieren ueber alle Sites
		while (allSites.hasMoreElements())
		{
			//hole Site-Objekt
			siteNode = (DefaultMutableTreeNode) allSites.nextElement();
			site = (ISite) siteNode.getUserObject();
			
			//hole zugewiesenen MasterWorkerNode aus Site
			workerNode = site.getMasterWorkerNode();
			
			//bestimme hash vom WorkerNode
			siteMasterWorkerNodeHash = getWorkerNodeRemoteObjectHash(workerNode);
			
			//ueberprüfe ob die Hashes übereinstimmen
			if (wnHash.equals(siteMasterWorkerNodeHash))
			{
				result = true;
				return result;	//beende vorzeitig die Schleife
			}
		}
		
		return result;
	}
	
	
	/**
	 * Ermittelt eine Site anhand ihreres zugehoerigen MasterWorkerNode-Hashes
	 * @param wnHash
	 * @return
	 */
	private ISite getSiteOfMasterWorkerNodeHash(String wnHash)
	{
		ISite site = null;
		ISite currentSite = null;
		
		DefaultMutableTreeNode siteNode;
		IWorkerNode workerNode;
		String siteMasterWorkerNodeHash;
		
		//hole Liste mit Sites
		Enumeration<ISite> allSites = _workerNodeTreeRoot.children();
		

		//iterieren ueber alle Sites
		while (allSites.hasMoreElements())
		{
			//hole Site-Objekt
			siteNode = (DefaultMutableTreeNode) allSites.nextElement();
			currentSite = (ISite) siteNode.getUserObject();
			
			//hole zugewiesenen MasterWorkerNode aus Site
			workerNode = currentSite.getMasterWorkerNode();
			
			//bestimme hash vom WorkerNode
			siteMasterWorkerNodeHash = getWorkerNodeRemoteObjectHash(workerNode);
			
			//ueberprüfe ob die Hashes übereinstimmen
			if (wnHash.equals(siteMasterWorkerNodeHash))
			{
				site = currentSite;	//merke Treffer
				return site;		//verlasse vorzeitig die Methode
			}
		}
		
		
		return site;
	}
	
	
	/**
	 * WorkerNode aus HashMap entfernen
	 * @param id
	 */
	public void removeWorkerNodeFromHashMap(int id)
	{
		if(_workerNodeHashMap.containsKey(id))
		{
			_workerNodeHashMap.remove(id);
			_log.info("WorkerNode (id="+id+") aus HashMap entfernt.");
		}
		else
		{
			_log.error("Entfernen des Worker-Nodes (id=" + id + ") fehlgeschlagen, da er nicht mehr in der HashMap vorhanden ist");
		}
		
		repairWorkerNodeHashMap();
	}
	
	
	/**
	 * Entfernt alle ungültigen WorkerNodes aus der HashMap
	 */
	public void repairWorkerNodeHashMap()
	{
		//alle WorkerNode-Ids dieser Liste werden entfernt
//		ArrayList<Integer> curruptWorkerNodeList = new ArrayList<Integer>();
		ArrayList<IWorkerNode> workerNodes = new ArrayList<IWorkerNode>();
		ArrayList<Integer> workerNodeIDs = new ArrayList<Integer>(); //enthält die IDs der WorkerNodes in gleicher Reihenfolge wie "workerNodes"
		ArrayList<Integer> curruptWorkerNodesIndexes = new ArrayList<Integer>(); //enthält indexe aus workerNodes der defekten WorkerNodes
		long timeout = 5000; //5sec
		
		try
		{
			Set<Entry<Integer, IWorkerNode>> allElements = _workerNodeHashMap.entrySet();
			
			//gehe alle Wertepaare durch und erstelle eine WorkerNodeList
			for (Entry<Integer, IWorkerNode> entry : allElements)
			{
				
				workerNodes.add(entry.getValue());	//speichere WorkerNode
				workerNodeIDs.add(entry.getKey());	//dazugehörigen Key in gleicher Reihenfolge wie WorkerNode ablegen
			}
			
			//bestimme defekte WorkerNodes
			curruptWorkerNodesIndexes = getCurruptWorkerNodes(workerNodes, timeout);
			
			
			//abbrechen, falls es keine defekten WorkerNodes gibt
			if (curruptWorkerNodesIndexes.size()<=0)
			{
				return;
			}
			
			
			//lösche Wertepaar
			int currentWorkerNodeId = -1;
			for (Integer index : curruptWorkerNodesIndexes)
			{
				currentWorkerNodeId = workerNodeIDs.get(index);
				if(_workerNodeHashMap.remove(currentWorkerNodeId)==null)
				{
					_log.info("Defekter WorkerNode (wnHash=" + getWorkerNodeRemoteObjectHash(workerNodes.get(index)) + ", wnId=" + currentWorkerNodeId + ") kann nicht gelöscht werden, da er nicht mehr in _workerNodeHashMap exitsiert.");
				}
				else
				{
					_log.info("Defekten WorkerNode (wnHash=" + getWorkerNodeRemoteObjectHash(workerNodes.get(index)) + ", wnId=" + currentWorkerNodeId + ") gelöscht.");
				}
			}
			
		}
		catch (Exception e)
		{
			_log.error("Reparieren der WorkerNode-HashMap fehlgeschlagen");
		}
	}
	

	
	
	
	/**
	 * Testes eine angegebene List von WorkerNodes, ob diese in Orndung sind.
	 * 
	 * @param workerNodesToTest - Teste dieser WorkerNodes
	 * @return - Liste mit Indexen der defekten WorkerNodes
	 */
	public ArrayList<Integer> getCurruptWorkerNodes(ArrayList<IWorkerNode> workerNodesToTest, long timeout)
	{
		ArrayList<Integer> currputWorkerNodes = new ArrayList<Integer>();
		ThreadPoolExecutor pool = _threadPool;
		boolean isWorkerNodeCurrupt = false;
		boolean taskHasBeenCannceled = false;
		
		//Arbeitsaufträge erstellen
		LinkedList<Callable<Boolean>> serviceList = new LinkedList<Callable<Boolean>>();
		for (IWorkerNode workerNode : workerNodesToTest)
		{
			serviceList.add(new HealtCheck(workerNode));
		}

		//vorzeitiges Beenden, falls keine Elemente in der ServiceList enthalten sind
		if (serviceList.size()<=0) return currputWorkerNodes;
		
		//Arbeitsaufträge vorbereiten --> jeder WorkerNode wird in einem extra Thread getestet
//		pool = new ThreadPoolExecutor(serviceList.size(),
//									  serviceList.size(),
//									  0L,
//									  TimeUnit.SECONDS,
//									  new LinkedBlockingQueue<Runnable>());

		
		try
		{
			//Arbeitsaufträge starten und auf Ergebnis warten
			List<Future<Boolean>> futureList;
			long start = System.currentTimeMillis();
			_log.debug("Zeitmessung: getCurruptWorkerNodes() starte invokeAll()...");
			futureList = pool.invokeAll(serviceList, timeout, TimeUnit.MILLISECONDS);
			_log.debug("Zeitmessung: getCurruptWorkerNodes() ...invokeAll() beendet. took " + String.valueOf((System.currentTimeMillis()-start)) + "ms");
			//WorkerNodes speichern, die defekt sind
			for (int i = 0; i < futureList.size(); i++)
			{
				Future<Boolean> currentWorkerNodeTestResult = futureList.get(i);
				
				//wenn der WN defekt ist oder nicht zuende gerechnet werden konnte--> merken
				isWorkerNodeCurrupt = currentWorkerNodeTestResult.get();
				taskHasBeenCannceled = currentWorkerNodeTestResult.isCancelled();
				if (isWorkerNodeCurrupt==true)
				{
					currputWorkerNodes.add(i);
					_log.debug("getCurruptWorkerNodes(): defekten WorkerNode gefunden");
				}
				if (taskHasBeenCannceled==true)
				{
					currputWorkerNodes.add(i);
					_log.debug("getCurruptWorkerNodes(): defekten WorkerNode gefunden (durch TIMEOUT)");
				}
			}
		}
		catch (InterruptedException e)
		{
			_log.error("Prüfen der Gesundheit von WorkerNodes fehlgeschlagen (InterruptedException)");
		}
		catch (ExecutionException e)
		{
			_log.error("Prüfen der Gesundheit von WorkerNodes fehlgeschlagen (ExecutionException)");
		}
		
		
		
		return currputWorkerNodes;
	}
	
	
	public HashMap<Integer, IWorkerNode> getWorkerNodeHashMap()
	{
		return this._workerNodeHashMap;
	}
	
	public void setWorkerNodeHashMap(HashMap<Integer, IWorkerNode> workerNodeHashMap)
	{
		this._workerNodeHashMap = workerNodeHashMap;
	}

	
	
	public void saveTreeRepresentationAsDotFile(FileWriter outputFile)
	{
		try
		{
			//speichere ein Datei
			outputFile.write(_graphVizRepresentationOfTree.getDotSource());
			outputFile.close();
		}
		catch (IOException e)
		{
			_log.error("Speichern des Graphen in einer Datei fehlgeschlagen");
		}
	}

	
	public void removeAllWorkerNodes()
	{
		_workerNodeHashMap.clear();
		_workerNodeMetaInformation.clear();
		_wnIdToRemoteObjectHashMapping.clear();
		repairWorkerNodeTree();
		

		//Sites bleiben bestehen, jedoch werden alle WorkerNodes gelöscht
		Enumeration<ISite> allSites = _workerNodeTreeRoot.children();
		DefaultMutableTreeNode currentSiteNode;
		ISite currentSite;
		while (allSites.hasMoreElements())
		{
			currentSiteNode = (DefaultMutableTreeNode) allSites.nextElement();
			
			//lösche alle WorkerNodes/Nodes der aktuellen Site
			currentSiteNode.removeAllChildren();
			
			//MasterWorkerNode zurücksetzen
			currentSite = (ISite) currentSiteNode.getUserObject();
			currentSite.setMasterWorkerNode(null);
		}
			
		_log.info("Alle WorkerNodes wurden vom WNM entfernt");
		
		//zur Kontrolle den neuen (leeren) Baum ausgeben	
		parseTree(true);
	}

	public void setGraphOutputFilename(String filename)
	{
		_graphOutputFilename = filename;
		
		//Graphviz aktualisieren
//		updateGraphvizRepresentationOfWorkerNodes();
	}

}
