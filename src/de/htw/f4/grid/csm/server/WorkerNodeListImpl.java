package de.htw.f4.grid.csm.server;

import java.awt.List;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.WorkerNodeImpl;
import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeList;


/**
 * Diese Klasse ist fuer das Schachprogramm und dem WNM
 * bestimmt und enthaelt Listen mit Worker-Nodes.
 * 
 *
 */
public class WorkerNodeListImpl extends WorkerNodeListAbstract implements IWorkerNodeList, Serializable
{
	//In Abfolge der Registrierung bei dem WNM werden die WorkerNodes der Reihe
	//nach in workerNodeHashMap abgelegt.
	private HashMap<Integer, IWorkerNode> _workerNodeHashMap;
	
	
	private final static Logger _log = Logger.getLogger(WorkerNodeListImpl.class);
//	private HashMap<Integer, IWorkerNode> workerNodeHashMap;
	
	
	public WorkerNodeListImpl()
	{
		super();
		_workerNodeHashMap = new HashMap<Integer, IWorkerNode>();
	}
	
	public WorkerNodeListImpl(HashMap<Integer, IWorkerNode> workerNodeHashMap)
	{
		setWorkerNodeHashMap(workerNodeHashMap);
	}
	

	public IWorkerNode get(int workerNodeId) 
	{
		IWorkerNode newWorkerNode = new WorkerNodeImpl();
		HashMap<Integer, IWorkerNode> workerNodeHashMap = getWorkerNodeHashMap();
	
		if(workerNodeHashMap.containsKey(workerNodeId))
		{
			// TODO: WNs autom. entfernen berücksichtigen
			newWorkerNode = workerNodeHashMap.get(workerNodeId);
		}
		return newWorkerNode;
	}


	/**
	 * Gibt alle WorkerNodes als ArrayListe zurueck
	 */
	public ArrayList<IWorkerNode> getAllWorkerNodes()
	{
		HashMap<Integer, IWorkerNode> workerNodeHashMap = getWorkerNodeHashMap();
		ArrayList<IWorkerNode> workerNodes = new ArrayList<IWorkerNode>();
		
		//hole alle WorkerNodes aus der HashMap raus
		Collection<IWorkerNode> workerNodeCollection = workerNodeHashMap.values();
		
		//hole Iterator aus Collection 
		Iterator<IWorkerNode> itr = workerNodeCollection.iterator();
		
		//fuege jeden WorkerNode der ArrayListe hinzu
		while (itr.hasNext())
		{
			// TODO: WNs autom. entfernen berücksichtigen
			IWorkerNode einWorkerNode = itr.next();
			workerNodes.add(einWorkerNode);
		}
		
//		ListTree<IWorkerNode> test = new ArrayListTree<IWorkerNode>();
//		MutableListTreeCursor<IWorkerNode> cursor = test.getCursor();
//		
//		Comparator<IWorkerNode> wnc = null;
//		Collections.sort(workerNodes, wnc);
		
		
		
		return workerNodes;
	}
	
	
	
	public int size()
	{
		HashMap<Integer, IWorkerNode> workerNodeHashMap = getWorkerNodeHashMap();
		return workerNodeHashMap.size();
	}


	//implementiert in der AbstraktenKlasse
//	public void removeWorkerNode(int id) 
//	{
//		// TODO: WNs autom. entfernen berücksichtigen
//		if(this.workerNodeHashMap.containsKey(id))
//		{
//			
//			this.workerNodeHashMap.remove(id);
//			this._log.info("WorkerNode ("+id+") deleted.");
//		}
//		
//	}

	

	public void removeWorkerNode(IWorkerNode workerNode) 
	{
		HashMap<Integer, IWorkerNode> workerNodeHashMap = getWorkerNodeHashMap();
		// TODO: WNs autom. entfernen berücksichtigen
		if(workerNodeHashMap.containsValue(workerNode))
		{
			for(int i = 0; i < workerNodeHashMap.size(); i++)
			{
				if(workerNodeHashMap.get(i) == workerNode)
				{
					removeWorkerNodeFromHashMap(i);
					break;
				}
			}
		}
		
	}
	

	public HashMap<Integer, IWorkerNode> getWorkerNodeHashMap()
	{
		return this._workerNodeHashMap;
	}
	
	public void setWorkerNodeHashMap(HashMap<Integer, IWorkerNode> workerNodeHashMap)
	{
		this._workerNodeHashMap = workerNodeHashMap;
	}

	public void removeWorkerNodeFromHashMap(int id) 
	{
		if(_workerNodeHashMap.containsKey(id))
		{
			this._workerNodeHashMap.remove(id);
			this._log.info("WorkerNode ("+id+") deleted.");
		}
		else
		{
			_log.error("Entfernen des Worker-Nodes id=" + id + " fehlgeschlagen, da er bereits entfernt wurde");
			
			//TODO id aus alles HashMaps's löschen
		}
	}

}
