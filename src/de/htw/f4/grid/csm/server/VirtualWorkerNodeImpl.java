package de.htw.f4.grid.csm.server;

import java.io.Serializable;

import de.htw.f4.grid.csm.server.interfaces.IVirtualWorkerNode;


public class VirtualWorkerNodeImpl implements IVirtualWorkerNode, Serializable
{
	
	private int listId;
	private int workerNodeId;
	
	public VirtualWorkerNodeImpl(int listId, int workerNodeId)
	{
		this.listId = listId;
		this.workerNodeId = workerNodeId;
	}

	public int getListId()
	{
		return this.listId;
	}

	public int getWorkerNodeId()
	{
		return this.workerNodeId;
	}


}
