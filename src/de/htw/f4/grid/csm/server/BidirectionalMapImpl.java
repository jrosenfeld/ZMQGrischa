package de.htw.f4.grid.csm.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IBidirectionalMap;

//http://forums.sun.com/thread.jspa?threadID=578312
public class BidirectionalMapImpl<VirtualWorkerNode, WorkerNode> implements IBidirectionalMap<VirtualWorkerNode, WorkerNode>
{
	private Map<VirtualWorkerNode, WorkerNode> _virtualWorkerNode2workerNode;
    private Map<WorkerNode, VirtualWorkerNode> _workerNode2virtualWorkerNode;

    
    
    public BidirectionalMapImpl()
    {
    	_virtualWorkerNode2workerNode = new HashMap<VirtualWorkerNode, WorkerNode>();
    	_workerNode2virtualWorkerNode = new HashMap<WorkerNode, VirtualWorkerNode>();
    }
    
    
	public WorkerNode getWorkerNode(VirtualWorkerNode key)
	{
		return _virtualWorkerNode2workerNode.get(key);
	}

	
	public VirtualWorkerNode getVirtualWorkerNode(WorkerNode value)
	{
		return (VirtualWorkerNode) _workerNode2virtualWorkerNode.get(value);
	}

	
	public void put(VirtualWorkerNode key, WorkerNode value)
	{
		_virtualWorkerNode2workerNode.put(key, value);
		_workerNode2virtualWorkerNode.put(value, key);
	}


	public ArrayList<WorkerNode> getAllWorkerNodes()
	{
		//hole alle WorkerNodes aus der HashMap raus
		
		ArrayList<WorkerNode> resultList = new ArrayList<WorkerNode>();
		
		for(WorkerNode currentWorkerNode : _virtualWorkerNode2workerNode.values())
		{
			resultList.add(currentWorkerNode);
		}
		
		return resultList;
	}


	public ArrayList<VirtualWorkerNode> getAllVirtualWorkerNodes()
	{
		//hole alle WorkerNodes aus der HashMap raus
//		Collection<VirtualWorkerNode> keyCollection = _workerNode2virtualWorkerNode.values();
		
		ArrayList<VirtualWorkerNode> resultList = new ArrayList<VirtualWorkerNode>();
		
		for(VirtualWorkerNode currentWorkerNode : _workerNode2virtualWorkerNode.values())
		{
			resultList.add(currentWorkerNode);
		}

		return resultList;
	}

}
