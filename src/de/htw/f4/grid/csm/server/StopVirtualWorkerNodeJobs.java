package de.htw.f4.grid.csm.server;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.server.interfaces.IVirtualWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IWnm;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 * Stoppt die Jobs von virtuellen WorkerNodes
 * @author rybec
 *
 */
public class StopVirtualWorkerNodeJobs implements Runnable, Callable<Boolean>
{
	private final static Logger _log = Logger.getLogger(StopVirtualWorkerNodeJobs.class);
	private ArrayList<IVirtualWorkerNode> _virtualWorkerNodes;
	private IWnm _wnm;

	public StopVirtualWorkerNodeJobs(ArrayList<IVirtualWorkerNode> virtualWorkerNodes, IWnm wnm)
	{
		_virtualWorkerNodes = virtualWorkerNodes;
		_wnm = wnm;
	}


	public void run()
	{
		try
		{
			if (_virtualWorkerNodes==null)
			{
				_log.error("run(): _virtualWorkerNodes==null!"); 
			}
			else
			{
				_wnm.stopVirtualWorkerNodeJobs(_virtualWorkerNodes);
			}
		}
		catch (SimonRemoteException e)
		{
			_log.error("Beenden der Jobs virtueller WorkerNodes fehlgeschlagen");
		}
	}


	public Boolean call() throws Exception
	{
		Boolean result = true;
		run();
		return result;
	}

}
