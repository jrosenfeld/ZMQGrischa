package de.htw.f4.grid.csm.server;

import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.jobs.JobResult;
import de.htw.f4.grid.csm.jobs.interfaces.IJob;
import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;
import de.htw.f4.grid.csm.server.interfaces.IBidirectionalMap;
import de.htw.f4.grid.csm.server.interfaces.ISite;
import de.htw.f4.grid.csm.server.interfaces.IVirtualWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IWnm;
import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeList;
import de.htw.f4.grid.csm.test.sonstiges.TestPingmessung;
import de.htw.f4.grid.csm.util.execptions.NotEnoughWorkerNodes;
import de.htw.f4.grid.csm.util.workernode.HealtCheck;
import de.htw.f4.grid.csm.util.workernode.StopJob;
import de.root1.simon.SimonRemoteInstance;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 * 
 * worker node manager (WNM)
 *  
 * Der WNM stellt Listen mit beliebiger Anzahl von WorkerNodes bereit.
 * Wird eine groeßere Anzahl an WorkerNodes benoetigt als bisher an dem WNM
 * angemeldet, so wird im Hintergrund versucht neue WorkerNodes
 * zu erzeugen.
 * 
 */
public class WnmImpl implements IWnm, Serializable
{
	// constants
	private static final long serialVersionUID = 1L;
	private final static Logger log = Logger.getLogger(WnmImpl.class);
	
	private final String stubName = "Wnm";
	private WorkerNodeListWithManagementImpl _workerNodeList; // Liste aller registrierten WorkerNodes
	
	private String _graphOutputFilename;
	
	//Liste aller zugewiesenen/vergebener WorkerNodes
	private HashMap<Integer,ArrayList<ArrayList<IWorkerNode>>> _assignedWorkerNodeMap; 
	private ArrayList<Integer> _assignedWorkerNodeList; //Liste mit IDs aller WorkerNodes, die vergeben sind
	
	private HashMap<String ,IJobResult> resultList; // Liste<WorkerNodeId, JobResult> aller Ergebnisse 
	
	
//	private ThreadPoolExecutor _threadPool; //Threads für Operationen an den WorkerNodes 
	
	 
	public WnmImpl() throws SimonRemoteException
	{	
		this._workerNodeList = new WorkerNodeListWithManagementImpl();
		this.log.info("WnmImpl started");
		
		_assignedWorkerNodeMap = new HashMap<Integer, ArrayList<ArrayList<IWorkerNode>>>();
		_assignedWorkerNodeList = new ArrayList<Integer>(); 
		
		this.resultList = new HashMap<String, IJobResult>();
		
		_graphOutputFilename = "graph.dot";
		
		//http://forums.sun.com/thread.jspa?threadID=645335
//		_threadPool =  new ThreadPoolExecutor(512,
//										 	  2048,
//											  0L,
//											  TimeUnit.SECONDS,
//											  new LinkedBlockingQueue<Runnable>());
		
		startWorkerNodeLifeCheck();
	}

	
	
	private void startWorkerNodeLifeCheck()
	{
		// FIXME not implemented yet
	}

	
	
	/**
	 * Ein WorkerNode meldet sich hiermit bei dem WNM an.
	 */
	synchronized public void registerWorkerNode(IWorkerNode object)
			throws SimonRemoteException
	{
		this._workerNodeList.add(object);
//		SimonRemoteStatistics statistics = Simon.getStatistics(object);
//		this.log.info("WorkerNode (remoteObjectHash=" + _workerNodeList.getWorkerNodeRemoteObjectHash(object) + ") add successfully to WorkerNodeList");
	}

	

	
	/**
	 * Alle angemeldeten WorkerNodes werden aus dem WNM entfernt.
	 */
	synchronized public void releaseAllWorkerNodes(boolean shutDown)
	{
		this.log.info("Alle angemeldeten WorkerNodes sollen abgemeldet werden...");
		
		
		//Anzahl der WorkerNodes bestimmen
		int countOfWorkerNodes = getNumberOfRegisteredWorkerNodes();
		IWorkerNodeList workerNodeList;
		ArrayList<IWorkerNode> allWorkerNodes;

		
		//Liste aller WorkerNodes holen
		workerNodeList = getWorkerNodeList(countOfWorkerNodes);
		allWorkerNodes = workerNodeList.getAllWorkerNodes();
		
		
		//alle WorkerNodes vom WNM entfernen
//		releaseWorkerNode(currentWorkerNode);
		removeAllWorkerNodes();

		
		//WorkerNode über Abmelden informieren
		for (IWorkerNode currentWorkerNode : allWorkerNodes)
		{
			try
			{
				currentWorkerNode.release(shutDown);
			}
			catch (SimonRemoteException e)
			{
				//WorkerNode beendet wie gewünscht, jedoch kommt immer
				//eine Exception. Dies ist aber nicht weiter schlimm.
				
//				this.log.error("release() auf WorkerNode fehlgeschlagen");
			}
		}
	}

	
	/**
	 * Alle registrieren WorkerNodes aus allen Listen entfernen. 
	 */
	private void removeAllWorkerNodes()
	{
		_workerNodeList.removeAllWorkerNodes();
	}
	
	
	/**
	 * Ein WorkerNodes meldet sich hiermit von dem WNM ab und beendet sich danach automatisch.
	 */
	synchronized public void releaseWorkerNode(IWorkerNode object)
			throws SimonRemoteException
	{
		//FIXME __marco - macht synchronized Sinn?
		//                blockiert zu lange. Evt. Löschaufträge in (Thread) Warteschlange sammlen und auf einmal ausführen,
		//                so dass der Aufruf von  releaseWorkerNode() nicht blockiert
		//WorkerNode aus allen Listen entfernen
		this._workerNodeList.removeWorkerNode(object);		
	}

	
	/**
	 * Startet den WNM. Damit beginnt auch die Aquerierung von WorkerNodes.
	 */
	public void startService()
	{
		// FIXME not implemented yet
		
	}

	
	/**
	 * Stoppt den WNM. Alle bisher angemeldeten WorkerNodes werden beendet und abgmeldet.
	 */
	public void stopService()
	{
		// FIXME not implemented yet
		
	}
	

	public IWorkerNodeList getWorkerNodeList(int numberOfWorkerNodes)
	{
		// TODO: erstelle auf Grundlage von  '_workerNodeList' eine neue Instanz des Interfaces
		//		IWorkerNodeList und gebe dies zurueck.

		IWorkerNodeList listOfWorkerNodes = new WorkerNodeListImpl(_workerNodeList.getWorkerNodeHashMap());
		
		return listOfWorkerNodes;
	}

	
	
	public int getNumberOfRegisteredWorkerNodes()
	{
		//FIXME __Marco - liefern beide Größen die gleichen Ergebnisse?
		int size1 = _workerNodeList.getWorkerNodeHashMap().size();
//		int size2 = _workerNodeList.getAllWorkerNodes().size();
//		
//		if (size1!=size2)
//		{
//			this.log.error("Fehler: getNumberOfRegisteredWorkerNodes(): size1=" + size1 + "  aber size2=" + size2);
//		}
		
		return size1;
	}

	
	
	/**
	 * DEBUG ONLY!!!
	 * print out workerNode-tree
	 */
	public StringBuffer parseWorkerNodeTree(boolean printOut)
	{
		StringBuffer result = new StringBuffer();
		result = _workerNodeList.parseTree(printOut);
		return result;
	}


	private ArrayList<ISite> getListOfAllSites()
	{
		ArrayList<ISite> list;
		list = _workerNodeList.getAllSites();
		
		return list;
	}


	/**
	 * Startet den Test zur Pingmessung zwischen MasterWorkerNodes
	 */
	private void testPingmessung()
	{
		TestPingmessung test = new TestPingmessung(_workerNodeList);
		test.start();
	}
	
	
	
	public void startTestPingmessung()
	{
		testPingmessung();
	}


	
	synchronized public void removeWorkerNode(IWorkerNode workerNode)
	{
		_workerNodeList.removeWorkerNode(workerNode);
		repairAssignedWorkerNodeList();
	}


	/**
	 * Gibt eine Liste mit n Workernodes zurueck
	 * 
	 * @param count
	 * @param clientId - eindeutige ID des Clients, der WorkerNodes beim Server verwenden möchte 
	 * @return
	 * @throws NotEnoughWorkerNodes 
	 */
	public ArrayList<IWorkerNode> getFreeWorkerNodeList(int count, int clientId) throws NotEnoughWorkerNodes
	{
		ArrayList<IWorkerNode> freeWorkerNodeList = new ArrayList<IWorkerNode>(); //noch nicht zugewiesene aber freie WorkerNodes
		
		int countOfAssignedWorkerNodes = _assignedWorkerNodeList.size();
		int countOfAllWorkerNodes = getNumberOfRegisteredWorkerNodes();
		int freeWorkerNodes = 0;
		
		
		//sicherheitshalber count checken und abbrechen wenn count ungültig
		if (count < 0)
		{
			return freeWorkerNodeList;
		}		
		

//		long start2 = System.currentTimeMillis();
		freeWorkerNodes = getFreeWorkerNodeCount(); //1ms bei 8 Nodes (HTW<->Marco)
//		log.debug("Zeitmessung: getFreeWorkerNodeList()5: getFreeWorkerNodeCount() took " + String.valueOf(System.currentTimeMillis()-start2) +  " ms)");
		
		
		//leere Liste zurück geben falls nicht genügend WorkerNodes zur Verfügung stehen
		if(freeWorkerNodes < count)
		{
			//nein --> gebe leere ListeZurück
			
			//TODO: Exception werfen: NICHT GENUG KAPAZITÄTEN
			return freeWorkerNodeList;	
		}	  
		
		
		//virtuelle Liste erstellen
//		virtualWorkerNodeList = generateVirtualWorkerNodeList(count, clientId);
		
		//hole x WorkerNodes
		long start = System.currentTimeMillis();
		freeWorkerNodeList = getFreeWorkerNodes(count); //93ms bei 8 Nodes (HTW<->Marco)
		this.log.debug("Zeitmessung: getFreeWorkerNodeList()6.getFreeWorkerNodes(" + count + ") took  " + String.valueOf(System.currentTimeMillis()-start) + "ms");
		
		//entferne defekte WorkerNodes
//		start = System.currentTimeMillis();

		
		//defekte WN's aus Liste entfernen
		//  __Marco - aufrufen, nachdem alle JobResults vorliegen, um keine Rechenzeit zu blokieren.
		//                Im schlimmsten Fall können dadurch keine Jobs an defekte WorkerNodes geschickt werden,
		//                was aber nicht weiter schlimm ist, denn die meisten WorkerNodes sollten korrekt sein.
//		deleteCorruptWorkerNodes(freeWorkerNodeList);	//98ms bei 8 Nodes (HTW<->Marco)

		
		
		//		this.log.debug("Zeitmessung: getFreeWorkerNodeList()7.deleteCorruptWorkerNodes() took  " + String.valueOf(System.currentTimeMillis()-start) + "ms");
		
		//füge erzeugte Liste zum entsprechenden Client dazu
//		start = System.currentTimeMillis();				// 0ms bei 8 Nodes (HTW<->Marco)
		addFreeWorkerNodeListToClient(freeWorkerNodeList, clientId);
//		this.log.debug("Zeitmessung: getFreeWorkerNodeList()8.addFreeWorkerNodeListToClient(freeWorkerNodeList, clientId) took  " + String.valueOf(System.currentTimeMillis()-start) + "ms");
		
		return freeWorkerNodeList;
	}
	
	
	/**
	 * Entfernt aus einer Liste defekte WorkerNodes
	 * @param resultList
	 * @return
	 */
	private void deleteCorruptWorkerNodes(ArrayList<IWorkerNode> resultList)
	{
		//FIXME __Marco : Parallelisieren
		long timeout = 5000; //5sec
		ArrayList<Integer> deleteIndexes = new ArrayList<Integer>();
		
		//bestimme alle defekten WorkerNodes
		deleteIndexes = _workerNodeList.getCurruptWorkerNodes(resultList, timeout);
		
		for (Integer index : deleteIndexes)
		{
			resultList.remove(index);
		}
		
		
//		ArrayList<IWorkerNode> delete = new ArrayList<IWorkerNode>();
//		
//		//merke alle defekten WorkerNodes
//		for (IWorkerNode workerNode : resultList)
//		{
//			if (isCurrupt(workerNode)==true)
//			{
//				delete.add(workerNode);
//			}
//		}
//		
//		//entferne alle defekten WorkerNodes
//		for (IWorkerNode workerNode : delete)
//		{
//			resultList.remove(workerNode);
//		}
		
	}

	
	/**
	 * Gibt an wieviele WNs gerade zur freien Verfuegung stehen 
	 * 
	 * @return
	 */
	public int getFreeWorkerNodeCount()
	{
		//ungültige WorkerNodeZuweisungen entfernen
//		long start = System.currentTimeMillis();
		
		
		//defekte WN's aus Liste entfernen
		//  __Marco - aufrufen, nachdem alle JobResults vorliegen, um keine Rechenzeit zu blokieren.
		//                Im schlimmsten Fall können dadurch keine Jobs an defekte WorkerNodes geschickt werden,
		//                was aber nicht weiter schlimm ist, denn die meisten WorkerNodes sollten korrekt sein.
//		repairAssignedWorkerNodeList();
		
//		this.log.debug("Zeitmessung: getFreeWorkerNodeCount(): " + String.valueOf(System.currentTimeMillis()-start) + "ms für repairAssignedWorkerNodeList()");
		
		
		int countOfAssignedWorkerNodes = _assignedWorkerNodeList.size();
		
		long start = System.currentTimeMillis();
		int countOfAllWorkerNodes = getNumberOfRegisteredWorkerNodes();
		this.log.debug("Zeitmessung: getFreeWorkerNodeCount(): " + String.valueOf(System.currentTimeMillis()-start) + "ms für getNumberOfRegisteredWorkerNodes()");
		
		int freeWorkerNodes = 0;
		
		freeWorkerNodes = countOfAllWorkerNodes-countOfAssignedWorkerNodes;
		
		return freeWorkerNodes;
	}
	

	
	/**
	 * Verknüpfe freieWorkerNodeList mit ClientID
	 * 
	 * @param freeWorkerNodeList
	 * @param clientId
	 */
	private void addFreeWorkerNodeListToClient(ArrayList<IWorkerNode> freeWorkerNodeList, int clientId)
	{
		//hole liste vom Client
		ArrayList<ArrayList<IWorkerNode>> clientWorkerNodeLists = new ArrayList<ArrayList<IWorkerNode>>();
		
		
		//hole Liste(n) für client, falls diese bereits angelegt sind
		if (_assignedWorkerNodeMap.containsKey(clientId)==true)
		{
			clientWorkerNodeLists = _assignedWorkerNodeMap.get(clientId);
			
		}
		
		
		//füge aktuelle List der clientId hinzu
		clientWorkerNodeLists.add(freeWorkerNodeList);

		
		_assignedWorkerNodeMap.put(clientId, clientWorkerNodeLists);
	}



	/**
	 * 
	 * @param count
	 * @param clientId - eindeutige ID des Clients, der WorkerNodes beim Server verwenden möchte 
	 * @return
	 */
//	private IBidirectionalMap<IWorkerNode,IWorkerNode> generateVirtualWorkerNodeList(int count, int clientId)
//	{
////		ArrayList<IWorkerNode> resultList = new ArrayList<IWorkerNode>();
//
//		ArrayList<IWorkerNode> freeWorkerNodeList = new ArrayList<IWorkerNode>(); //noch nicht zugewiesene aber freie WorkerNodes
//		
//		IBidirectionalMap<IWorkerNode,IWorkerNode> resultList = new BidirectionalMapImpl<IWorkerNode, IWorkerNode>();
//		
//		int countOfAssignedWorkerNodes = _assignedWorkerNodeList.size();
//		int countOfAllWorkerNodes = getNumberOfRegisteredWorkerNodes();
//		int freeWorkerNodes = 0;
//		
//		
//		freeWorkerNodes = countOfAllWorkerNodes-countOfAssignedWorkerNodes;
//
//		//count enthält gültigen Wert?
//		
//		//genug freie WorkerNodes vorhanden?
//		if(freeWorkerNodes < count)
//		{
//			//nein --> gebe leere ListeZurück
//			
//			//TODO: Exception werfen: NICHT GENUG KAPAZITÄTEN
//			return resultList;	
//		}
//		
//		
//		//hole x WorkerNodes
//		freeWorkerNodeList = getFreeWorkerNodes(count);
//		
//		
//		//verknüpfe WorkerNodes<->virtuelleWorkerNodes
//		resultList = mapVirtualWorkerNodesWithWorkerNodes(freeWorkerNodeList);
//		
//		
//		//füge erzeugte Liste zum entsprechenden Client dazu
//		addBidirectionalWorkerNodeListToClient(resultList, clientId);
//		
//		
//		//extrahiere virtuelleWorkerNodes aus MappingList
////		resultList = getVirtualWorkerNodesFromBidirectionalList(resultList);
//		
//		
//		return resultList;
//	}

	
	
	
	private ArrayList<IWorkerNode> getVirtualWorkerNodesFromBidirectionalList(
			IBidirectionalMap<IWorkerNode, IWorkerNode> bidirectionalWorkerNodeList)
	{
		ArrayList<IWorkerNode> virtualWorkerNodeList = new ArrayList<IWorkerNode>();
	
		
		virtualWorkerNodeList = bidirectionalWorkerNodeList.getAllVirtualWorkerNodes();
		 
		return virtualWorkerNodeList;
	}



//	private void addBidirectionalWorkerNodeListToClient(
//			IBidirectionalMap<IWorkerNode, IWorkerNode> bidirectionalWorkerNodeList,
//			int clientId)
//	{
//		
//		//hole liste vom Client
//		ArrayList<IBidirectionalMap<IWorkerNode,IWorkerNode>> clientBiMap = new ArrayList<IBidirectionalMap<IWorkerNode,IWorkerNode>>();
//		
//		
//		//client hat bereits Liste(n)?
//		if (_assignedWorkerNodeMap.containsKey(clientId)==true)
//		{
//			clientBiMap = _assignedWorkerNodeMap.get(clientId);
//			
//		}
//		
//		clientBiMap.add(bidirectionalWorkerNodeList);
//		
//		_assignedWorkerNodeMap.put(clientId, clientBiMap);
//		
//	}



//	/**
//	 * Legt autom. virtuelleWorkerNodes an und verknüpft diese mit den übergebenen WorkerNodes
//	 * @param freeWorkerNodeList
//	 * @return
//	 */
//	private IBidirectionalMap<IWorkerNode,IWorkerNode> mapVirtualWorkerNodesWithWorkerNodes(
//			ArrayList<IWorkerNode> freeWorkerNodeList)
//	{
//		IWorkerNode newVirtualWorkerNode;
//		IBidirectionalMap<IWorkerNode,IWorkerNode> resultMap = new BidirectionalMapImpl<IWorkerNode, IWorkerNode>();
//		
//		for (IWorkerNode currentWorkerNode : freeWorkerNodeList)
//		{
//			//TODO: impl von VirtualWorkerNode komplett?
//			newVirtualWorkerNode = new VirtualWorkerNodeImpl();
//			
//			//map-element erzeugen
//			resultMap.put(newVirtualWorkerNode, currentWorkerNode);
//		}
//		
//		return resultMap;
//		
//	}



	/**
	 * Gibt eine Liste zurück, die n freie WorkerNodes enthält
	 * @param count
	 * @return
	 * @throws NotEnoughWorkerNodes 
	 */
	public ArrayList<IWorkerNode> getFreeWorkerNodes(int count) throws NotEnoughWorkerNodes
	{
		ArrayList<IWorkerNode> resultList = new ArrayList<IWorkerNode>();
		ArrayList<IWorkerNode> allWorkerNodes = new ArrayList<IWorkerNode>();
		IWorkerNode currentWorkerNode;
		int currentWorkerNodeId = -1;
		
		
		//hole ALLE workerNodes (freie und vergebene)
		allWorkerNodes = _workerNodeList.getAllWorkerNodes();

		
		
		HashMap<Integer, IWorkerNode> allWorkerNodesHashMap = _workerNodeList.getWorkerNodeHashMap();
		Set<Entry<Integer, IWorkerNode>> enrySet = allWorkerNodesHashMap.entrySet();
		
		for (Entry<Integer, IWorkerNode> entry : enrySet)
		{
			currentWorkerNodeId = entry.getKey();
			currentWorkerNode = entry.getValue();
		
			//workerNode unvergeben?
			if (_assignedWorkerNodeList.contains(currentWorkerNodeId)==false);
			{
				resultList.add(currentWorkerNode);
			}
			
			//breche Schleife ab, sobald die Anzahl der gewünschten WorkerNodes erreicht ist
			if (resultList.size() >= count)
			{
				break; //vorzeitiges beenden
			}
		}
		
		
		//count erreicht?
		if (resultList.size() < count)
		{
//			resultList = new ArrayList<IWorkerNode>();
			throw new NotEnoughWorkerNodes("Nicht genügend WorkerNodes vorhanden");
		}
		

		return resultList;
	}


	//DEBUG ONLY !!!
	public void saveTreeRepresentationAsDotFile(FileWriter outputFile)
	{
		_workerNodeList.saveTreeRepresentationAsDotFile(outputFile);
	}



	public IWorkerNode getWorkerNode(int id)
	{
		return _workerNodeList.get(id);
	}

	
	
	
	
	/**
	 * Durchsucht eine Liste mit WorkerNodes und identifiziert
	 * einen WorkerNode anhand der übergebenen WorkerNodeId
	 *  
	 * @param workerNodeId
	 * @param listToSearchIn
	 * @return
	 */
	private IWorkerNode getWorkerNodeById(int workerNodeId, ArrayList<IWorkerNode> listToSearchIn)
	{
		IWorkerNode workerNode = null;
		HashMap<Integer, IWorkerNode> allWorkerNodes;
		Set<Entry<Integer, IWorkerNode>> entrySet;
		int currentWorkerNodeId = -1;
		IWorkerNode currentWorkerNode;
		
		//hole ALLE workerNodes
		allWorkerNodes = _workerNodeList.getWorkerNodeHashMap();
		
		//erzeuge EntrySet
		entrySet = allWorkerNodes.entrySet();
	
		
		//identifiziere einen WorkerNode anhand seine ID
		for (Entry<Integer, IWorkerNode> entry : entrySet)
		{
			currentWorkerNodeId = entry.getKey();
			currentWorkerNode = entry.getValue();
		
			if(currentWorkerNodeId == workerNodeId)
			{
				return currentWorkerNode;
			}
		}
		
		return workerNode;
	}
	
	
	public void startJob(IJob job, int workerNodeId, int clientId)
	{
//		IBidirectionalMap<IVirtualWorkerNode, IWorkerNode> biWorkerNodeMap = new BidirectionalMapImpl<IVirtualWorkerNode, IWorkerNode>();

		ArrayList<ArrayList<IWorkerNode>> clientWorkerNodeLists = new ArrayList<ArrayList<IWorkerNode>>();
		IWorkerNode workerNode = null;
		long start = System.currentTimeMillis();
		this.log.debug("Zeitmessung: startJob().getClientWorkerNodeLists() start (wnId=" + workerNodeId + ")...");
		clientWorkerNodeLists = getClientWorkerNodeLists(clientId);
//		this.log.debug("Zeitmessung: startJob().getClientWorkerNodeLists() ...ende (wnId=" + workerNodeId + ").");
//		this.log.debug("Zeitmessung: startJob().getClientWorkerNodeLists() took " + String.valueOf(System.currentTimeMillis()-start) + "ms");
		
		//identifiziere einen WorkerNode anhand seine ID
		start = System.currentTimeMillis();
		for(ArrayList<IWorkerNode> currentWorkerNodeList : clientWorkerNodeLists)
		{
			workerNode = getWorkerNodeById(workerNodeId, currentWorkerNodeList);
			
			if(workerNode != null)
			{
				break;
			}
		}
//		this.log.debug("Zeitmessung: startJob().identifiziere einen WorkerNode anhand seine ID took   " + String.valueOf(System.currentTimeMillis()-start) + "ms");
		
		//führe am gefundenn WorkerNode den Job aus
		if(workerNode != null)
		{
			try
			{
//				start = System.currentTimeMillis();
				workerNode.startJob(job);
//				this.log.debug("Zeitmessung: startJob().workerNode.startJob(job) took  " + String.valueOf(System.currentTimeMillis()-start) + "ms");
			}
			catch (SimonRemoteException e)
			{
				String message = "Starten eines Jobs beim WorkerNode fehlgeschlagen";
				this.log.error(message);
				this.log.debug(message, e);
			}
			
		}
	}


	public IJobResult getJobResult(String moveHash) throws SimonRemoteException 
	{
		if(this.resultList.containsKey(moveHash) == false)
		{
			this.log.info("There's no result on workerNode:" + moveHash);
			return null;			
		}
		
		return this.resultList.get(moveHash);
	}
	
	
//	
//	public void stopJob(int workerNodeId, int clientId) throws SimonRemoteException 
//	{
//		ArrayList<ArrayList<IWorkerNode>> clientWorkerNodeLists = new ArrayList<ArrayList<IWorkerNode>>();
//		IWorkerNode workerNode = null;
//		clientWorkerNodeLists = getClientWorkerNodeLists(clientId);
//		
//		//identifiziere einen WorkerNode anhand seine ID 
//		for(ArrayList<IWorkerNode> currentWorkerNodeList : clientWorkerNodeLists)
//		{
//			for(IWorkerNode currentWorkerNode : currentWorkerNodeList)
//			{
//				try
//				{
//					if(currentWorkerNode.getId() == workerNodeId)
//					{
//						workerNode = currentWorkerNode;
//					}			
//					
//				}
//				catch (SimonRemoteException e)
//				{
//					this.log.error("Fehler");
//					this.log.debug("Fehler", e);
//				}
//			}	
//			if(workerNode != null)
//			{
//				break;
//			}
//		}
//		
//		
//		// Ergebnis aus Ergebnisliste löschen
//		this.resultList.remove(workerNodeId);
//		this.log.info("JobErgebnis aus Liste entfernt.");
//	}
	
	
	
	private ArrayList<ArrayList<IWorkerNode>> getClientWorkerNodeLists(int clientId)
	{
		ArrayList<ArrayList<IWorkerNode>> clientWorkerNodeLists = new ArrayList<ArrayList<IWorkerNode>>();
		ArrayList<Integer> defectWorkerNodesIndexes = new ArrayList<Integer>(); //liste mit allen defekten WorkerNodes-Indexen
		ArrayList<IWorkerNode> workerNodesOfAllLists = new ArrayList<IWorkerNode>();
		
		
		//client-Liste holen
		clientWorkerNodeLists = _assignedWorkerNodeMap.get(clientId);

		
		
		//defekte WNs löschen kostet zu viel Zeit, deswegen vorerst wieder auskommentiert
		
//		//Liste mit allen WorkerNodes erstellen
//		for (ArrayList<IWorkerNode> workerNodeList : clientWorkerNodeLists)
//		{
//			for (IWorkerNode currentWorkerNode : workerNodeList)
//			{
//				workerNodesOfAllLists.add(currentWorkerNode);
//			}
//		}
//		
//
//		
//		//indexe aller defekte WorkerNodes ermitteln
//		this.log.debug("Zeitmessung: getClientWorkerNodeLists() start...");
//		Long start = new Long(System.currentTimeMillis());
//		defectWorkerNodesIndexes = _workerNodeList.getCurruptWorkerNodes(workerNodesOfAllLists);
//		this.log.debug("Zeitmessung: getClientWorkerNodeLists() ...ende (took " + String.valueOf((System.currentTimeMillis()-start)) + "ms)");
//		
//		
//		//defekte WorkerNodes entfernen
//		for(Integer index : defectWorkerNodesIndexes)
//		{
//			workerNodesOfAllLists.remove(index);
//		}
		
		
		return clientWorkerNodeLists;
		
//		//FIXME __Marco: enthält noch Fehler ???
//		//ungültige WorkerNodes entfernen
//		for (ArrayList<IWorkerNode> workerNodeList : clientWorkerNodeLists)
//		{
//			for (IWorkerNode currentWorkerNode : workerNodeList)
//			{
//				//defekte WorkerNodes merken
//				//FIXME __Marco - Parallelisieren
//				if (isCurrupt(currentWorkerNode)==true)
//				{
//					defectWorkerNodes.add(currentWorkerNode);
//				}
//			}
//		}
//		
////		
////		//alle defekte WorkerNodes aus liste entfernen
////		for(IWorkerNode defectWorkerNode : defectWorkerNodes)
////		{
////			clientWorkerNodeLists.remove(defectWorkerNode);
////		}
////		
////		//lokale Map aktualisieren, falls defekte WorkerNodes entfernt wurden
////		if  (defectWorkerNodes.size()>0)
////		{
////			_assignedWorkerNodeMap.put(clientId, clientWorkerNodeLists);
////		}
//		

		
	}
	
	
	/**
	 * Prueft ein WorkerNode auf Korrektheit.
	 * @param workerNodeToTest
	 * @return true wenn der WorkerNode defekt ist
	 */
	private boolean isCurrupt(IWorkerNode workerNodeToTest)
	{
		int workerNodeId = -1;
		
		if (workerNodeToTest==null) return true;
		
		try
		{
			workerNodeId = workerNodeToTest.getId();
		}
		catch (SimonRemoteException e)
		{
			this.log.error("WorkerNode (wnhash=" + _workerNodeList.getWorkerNodeRemoteObjectHash(workerNodeToTest) + ") defekt");
			
			return true;
		}
		
		return false;
	}
	
	
	public void sendMessageToWnm(String message)
	{
		this.log.debug("incomming remote message: " + message);
	}
	
	
	
	/**
	 * Löschte alle IDs zu denen keine WorkerNode mehr existieren
	 */
	private void repairAssignedWorkerNodeList()
	{
		HashMap<Integer, IWorkerNode> workerNodeHashMap;
		ArrayList<Integer> defectWorkerNodeIDs = new ArrayList<Integer>();	//Liste mit allen ungültigen WorkerNodeID's
		
		
		//hole Map aller momentan registrierten workerNodes
		workerNodeHashMap = _workerNodeList.getWorkerNodeHashMap();
		
		
		//Durchlaufe alle ID's
		for (Integer wnId : _assignedWorkerNodeList)
		{
			//merke defekte WorkerNodeID's
			if (workerNodeHashMap.get(wnId)==null)
			{
				defectWorkerNodeIDs.add(wnId);
			}
		}
		
		
		//entferne alle gefundenen defekten WorkerNodeIDs
		for(Integer defectWorkerNodeId : defectWorkerNodeIDs)
		{
			_assignedWorkerNodeList.remove(defectWorkerNodeId);
			this.log.debug("Defekten WorkerNode (id=" + defectWorkerNodeId + ") aus assignedWorkerNodeList entfernt");
		}
		
	}



	/**
	 * Sorgt dafuer, dass alle WNs ihren momentanen Job (hart) beenden und wieder zur freien
	 * Verfuegung stehen (vom WnmConnector initiiert)
	 *   
	 * @throws SimonRemoteException
	 */
	public void releaseWorkerNodeList(int listId, int clientId)	throws SimonRemoteException
	{
		long timeout = 1000;
		
		//alle WorkerNodeListen eines Clients
		ArrayList<ArrayList<IWorkerNode>> workerNodeLists = _assignedWorkerNodeMap.get(clientId);
		ArrayList<IWorkerNode> stopWorkerNodeJobs = new ArrayList<IWorkerNode>(); //alle Workernodes, deren Jobs gestoppt werden 
		
		//WorkerNodes der Client-Liste leeren, indem ein leere Liste gesetzt wird
		if (workerNodeLists!=null)
		{
			ArrayList<IWorkerNode> clientList = workerNodeLists.get(listId);
			if (clientList!=null)
			{
				
				//alle WorkerNodes sammeln
				this.log.debug("Zeitmessung: Alle WorkerNodes der ClientListe (" + listId +") sammeln...");
				for (Iterator iterator = clientList.iterator(); iterator.hasNext();)
				{
					IWorkerNode workerNode = (IWorkerNode) iterator.next();
					stopWorkerNodeJobs.add(workerNode);
				}
				this.log.debug("Zeitmessung: ... Einsammeln alle WorkerNodes der ClientListe (" + listId + ") beendet");
				
				//Jobs beenden
				this.log.debug("Zeitmessung: alte WorkerNode-Jobs werden beendet...");
				stopWorkerNodeJobs(stopWorkerNodeJobs, timeout);
				this.log.debug("Zeitmessung: ... erfolgreiche Beendingung alter WorkerNode-Jobs");
				
				//WorkerNodes aus Liste entfernen
				clientList.clear();
			}
			
			this.log.debug("Client (id=" + clientId + ") hat WorkerNodes freigegeben (listId=" + listId + ")");
		}
		
	}



	/**
	 * Löscht alle WorkerNode-Listen vom angegebenen Client.
	 * Wird i.d.R. beim ersten Programmstart vnon GridGameManager aufgerufen.
	 */
	public void releaseWorkerNodeList(int clientId) throws SimonRemoteException
	{
		//alle WorkerNodeListen eines Clients
		ArrayList<ArrayList<IWorkerNode>> workerNodeLists = _assignedWorkerNodeMap.get(clientId);
		
		//WorkerNodes der Client-Liste leeren, indem ein leere Liste gesetzt wird
		if (workerNodeLists!=null)
		{
			for (Iterator iterator = workerNodeLists.iterator(); iterator.hasNext();)
			{
				ArrayList<IWorkerNode> arrayList = (ArrayList<IWorkerNode>) iterator.next();
				arrayList.clear();
			}
			
			this.log.debug("Alle WorkerNodes vom Client (id=" + clientId + ") freigegeben");
		}
		
	}
	
	
	
	
	/**
	 * Stoppt alle Jobs aus einer Liste von WorkerNodes
	 * 
	 * @param workerNodesToTest
	 * @param timeout - Zeit in ms, die maximal auf das Beenden eines Jobs gewartet wird
	 */
	private void stopWorkerNodeJobs(ArrayList<IWorkerNode> workerNodesToTest, long timeout)
	{
		ArrayList<Integer> currputWorkerNodes = new ArrayList<Integer>();
		ThreadPoolExecutor pool = new ThreadPoolExecutor(512,
													 	  1500,
														  0L,
														  TimeUnit.SECONDS,
														  new LinkedBlockingQueue<Runnable>());
		
		//Arbeitsaufträge erstellen
		LinkedList<Callable<Boolean>> serviceList = new LinkedList<Callable<Boolean>>();
		for (IWorkerNode workerNode : workerNodesToTest)
		{
			serviceList.add(new StopJob(workerNode));
		}

		//vorzeitiges Beenden, falls keine Elemente in der ServiceList enthalten sind
		if (serviceList.size()<=0) return;
		
		try
		{
			//Arbeitsaufträge starten
			List<Future<Boolean>> futureList;
			
			//FIXME __Marco: hier klappt was noch nicht richtig. Anscheinend funktioniert
			//             Thread.stop() auf den WorkerNodes nicht
			futureList = pool.invokeAll(serviceList, timeout, TimeUnit.MILLISECONDS);
			
			pool.shutdownNow();
		}
		catch (InterruptedException e)
		{
			this.log.error("Beenden der Jobs von WorkerNodes fehlgeschlagen (InterruptedException)");
		}
	}



	public void stopVirtualWorkerNodeJobs(ArrayList<IVirtualWorkerNode> virtualWorkerNodes)	throws SimonRemoteException
	{
		long timeout = 5000;
		
		//hier kommen alle WorkerNodes rein, deren Jobs gestoppt werden sollen
		ArrayList<IWorkerNode> workerNodeList = new ArrayList<IWorkerNode>();
		
		//liste mit workerNodes ID's die gelöschen werden soll
		ArrayList<Integer> workerNodeIdList = new ArrayList<Integer>();

	
		
		//sammle IDs der WorkerNodes
		for(IVirtualWorkerNode virtualWorkerNode : virtualWorkerNodes)
		{
			if (virtualWorkerNode!=null)
			{
				workerNodeIdList.add(virtualWorkerNode.getWorkerNodeId());
			}
			else
			{
				this.log.error("stopVirtualWorkerNodeJobs(): VirtuellerWorkerNode wird ignoriert (NULL-Referenz)");
			}
		}
		
		
		//passenden WorkerNodes zu IDs holen
		for(Integer workerNodeId : workerNodeIdList)
		{
			workerNodeList.add(_workerNodeList.get(workerNodeId));
		}
		
		
		//jobs abschießen
		stopWorkerNodeJobs(workerNodeList, timeout);
		
		
		
		//defekte WN's aus Liste entfernen
		//  __Marco - aufrufen, nachdem alle JobResults vorliegen, um keine Rechenzeit zu blokieren.
		//                Im schlimmsten Fall können dadurch keine Jobs an defekte WorkerNodes geschickt werden,
		//                was aber nicht weiter schlimm ist, denn die meisten WorkerNodes sollten korrekt sein.
		/** Reparieren der WorkerNode-Listen anstoßen **/
		_workerNodeList.repairWorkerNodeHashMap();
		repairAssignedWorkerNodeList();
	}
	

	
	
	public void saveJobResult(IJobResult jobResult, String moveHash) throws SimonRemoteException
	{
		this.resultList.put(moveHash, jobResult);		
	}



	public void setGraphOutputFilename(String filename)
	{
		_graphOutputFilename = filename;
		if (_workerNodeList!=null)
		{
			_workerNodeList.setGraphOutputFilename(_graphOutputFilename);
		}
	}



	public void repairWorkerNodes()
	{
		//FIXME __marco - blokiert das hier aus? (ungetestet)
		repairAssignedWorkerNodeList();
	}

}
