package de.htw.f4.grid.csm.server;

import java.io.Serializable;
import java.net.InetAddress;

import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeMetaInformation;


public class WorkerNodeMetaInformationImpl implements
		IWorkerNodeMetaInformation, Serializable
{
	private int _rttToMasterWorkerNode; //Zeit in ms zum MasterWorkerNode
	private InetAddress[] _localIpAdresses;

	
	public WorkerNodeMetaInformationImpl()
	{
		_rttToMasterWorkerNode = -1;
	}


	public int getAvgRttToMasterWorkerNode()
	{
		return _rttToMasterWorkerNode;
	}


	public void setAvgRttToMasterWorkerNode(int rtt)
	{
		_rttToMasterWorkerNode = rtt;
	}


	public InetAddress[] getIpAdresses()
	{
		return _localIpAdresses;
	}


	public void setIpAdresses(InetAddress[] ipAdresses)
	{
		_localIpAdresses = ipAdresses;
	}

	
}
