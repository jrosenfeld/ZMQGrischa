package de.htw.f4.grid.csm.server;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IWorkerNodeList;
import de.htw.f4.grid.csm.util.LoggingMessages;

public abstract class WorkerNodeListAbstract implements IWorkerNodeList, Serializable
{
	
	
	private final static Logger _log = Logger.getLogger(WorkerNodeListAbstract.class);
	
	
	
	
	/**
	 * Liefert die eindeutige ID (in Form des 'RemoteObjectHash') eines Worker-Nodes zurueck 
	 * @param workerNode
	 * @return
	 */
	public String getWorkerNodeRemoteObjectHash(IWorkerNode workerNode)
	{
		String result = "-1";
		
		try
		{
			//vorzeitiges Abbrechen falls WorkerNode NULL ist
			if (workerNode==null) return result;
			
			InvocationHandler invocationHandler = java.lang.reflect.Proxy.getInvocationHandler(workerNode);
			String remoteObjectStr = invocationHandler.toString();
			int hashBeginIndex = remoteObjectStr.indexOf("remoteObjectHash=");
			int hashEndIndex = remoteObjectStr.indexOf("]", hashBeginIndex);
			
			result = remoteObjectStr.substring(hashBeginIndex, hashEndIndex);
			result = result.replace("remoteObjectHash=", "");
			
			
			//zusätzlich SimonProxy anhängen, damit der Wert eindeutig wird.
			//remoteObjectHash scheint nämlich nur für einen Computer eindeutig zu sein, wenn jedoch
			//auf einem Computer zwei WorkerNodes laufen so ist der hash für beide Nodes
			//identisch --> das kann Probleme verursachen !
			
			//FIXME __Marco: Funktioniert nicht, sobald die Verbindung zum WorkerNode abgebrochen ist
			//simonProxy = Error occured while invoking [de.htw.f4.grid.csm.client.WorkerNodeImpl|ip=pluto14/141.45.154.149:13735;sessionID=2;remoteObjectHash=4313181]#toString(). Error was: Cannot handle method call "toString()" on already closed session.
			//--> proxyBeginIndex == -1
			/**
			String simonProxy = invocationHandler.toString();
			_log.debug("+++++++---->  simonProxy = " + simonProxy);
			
			String result2 = "";
			int proxyBeginIndex = simonProxy.indexOf("SimonProxy@");
			_log.debug("+++++++---->  proxyBeginIndex = " + proxyBeginIndex);
			
			int proxyEndIndex = simonProxy.indexOf("|", proxyBeginIndex+1);
			_log.debug("+++++++---->  proxyEndIndex = " + proxyEndIndex);
			
			result2 = simonProxy.substring(proxyBeginIndex, proxyEndIndex);
			_log.debug("+++++++---->  result2 = " + result2);
			
			result2 = result2.replace("SimonProxy@", "");
			
			result = result.concat(result2);
			**/
		}
		catch (IllegalArgumentException e)
		{
			String message = "Hashberechnung eines WorkerNodes fehlgeschlagen" +
			                 " (" + e.getMessage() + ")";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
		}

		return result;
	}
	
	

}
