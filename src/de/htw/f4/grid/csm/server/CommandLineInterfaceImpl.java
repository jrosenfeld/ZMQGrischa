package de.htw.f4.grid.csm.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import org.apache.log4j.Logger;


public class CommandLineInterfaceImpl implements ICommandLineInterface
{
	private final static Logger _log = Logger.getLogger(CommandLineInterfaceImpl.class);
	
	private ServerSocket _myserver;
	private Socket _theclient;
	private int _listeningPort;
	private PrintWriter _out;
	private BufferedReader _in;

	public CommandLineInterfaceImpl()
	{
		_myserver = null;
		_theclient = null;
		_listeningPort = 4444;
	}


	public void sendToOutputStream(String input)
	{
		_log.info("cli (out) = \n" + input); //send to stdout
		
		String sendStr;
		
		sendStr = input.replace("\n", "%%");
		_out.println(sendStr);		//send to stream
		
//		System.out.println(input);  
	}


	public void start(int listeningPort)
	{
		_listeningPort = listeningPort;
		
		createSocket();
	}

	
	private void createSocket()
	{
		//Server starten
		try
		{
			_myserver = new ServerSocket(_listeningPort);
		}
		catch (IOException e)
		{
			System.out.println("Could not listen on port: " + _listeningPort + ".");
			System.exit(1);
		}
		
		
		//einegehende Verbindungen akzeptieren
		try
		{
			_theclient = _myserver.accept();
		} catch (IOException e)
		{
			System.err.println("“Accept failed.");
			System.exit(1);
		}		

		chatWithClient();
	}
	
	
	
	private void chatWithClient()
	{
		createStreams();
		
//		String inputLine;
//		String outputLine;
//		
//		
//		try
//		{
//			while((inputLine=_in.readLine()) != null)
//			{
//				outputLine = inputLine + " :-)";
//				_out.println(outputLine);
//				System.out.println("Anfrage vom Client: " + inputLine);
//				if (inputLine.equals("Bye."))
//				{
//					break;
//				}
//			}
//		}
//		catch (IOException e)
//		{
//			_log.error("Fehler im CLI-Dienst aufgetreten");
//		}
//		
//		//alle Verbindungen Schließen
//		closeAllConnections();
	}
	
	
	

	private void closeAllConnections()
	{
		try
		{
			_out.close();
			_in.close();
			_theclient.close();
			_myserver.close();
		}
		catch (IOException e)
		{
			_log.error("Beenden des CLI-Server fehlgeschlagen. Streams konnten nicht vollständig beendet werden");
		}
	}


	/**
	 *  in- und output-streams erzeugen
	 */
	private void createStreams()
	{
		try
		{
			_out = new PrintWriter(_theclient.getOutputStream(),true);
		}
		catch (IOException e)
		{
			_log.error("Erzeugung des OutputStreams fehlgeschlagen");
			return ;
		}
		
		
		try
		{
			_in = new BufferedReader(new InputStreamReader(_theclient.getInputStream()));
		}
		catch (IOException e)
		{
			_log.error("Erzeugung des InputStreamReaders fehlgeschlagen");
			return ;
		}
	}

	
	/**
	 * Beende den CLI-Dienst
	 */
	public void stop()
	{
		closeAllConnections();
	}


	
	public BufferedReader getBufferedReader()
	{
		return _in;
	}
}
