package de.htw.f4.grid.csm.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;



/**
 *
 * Die commandline kann über einen Socket von einem anderen Programm bedient werden.  
 *
 */
public interface ICommandLineInterface
{

	/**
	 * startet den Server
	 * @param listeningPort - Port auf dem auf einegenden Verbindungen gelauscht wird
	 */
	public void start(int listeningPort);
	
	
	/**
	 * Dient beenden
	 */
	public void stop();
	
	
	/**
	 * Ein String kann an die Kommandozeile geschickt werden
	 * @param input - Eingabe für die Kommandozeile
//	 * @return - Ausgabe der Kommandozeile
	 */
	public void sendToOutputStream(String input);
	
	
	public BufferedReader getBufferedReader();
	
	
}
