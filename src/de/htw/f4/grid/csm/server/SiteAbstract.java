package de.htw.f4.grid.csm.server;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.ISite;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 * 
 * Implementiert einige Methoden des Interfaces ISite
 *
 */
public abstract class SiteAbstract implements ISite
{
	private String _name;
	private IWorkerNode _masterWorkerNode;
	
	public SiteAbstract(String name)
	{
		_name = name;
	}
	

	public IWorkerNode getMasterWorkerNode()
	{
		return _masterWorkerNode;
	}


	public String getName()
	{
		return _name;
	}


	public void setMasterWorkerNode(IWorkerNode einWorkerNode)
	{
		_masterWorkerNode = einWorkerNode;
	}
	
	
	public boolean hasMasterWorkerNode()
	{
		if (_masterWorkerNode==null)
		{
			return false;
		}
		else
		{
			if (isCurrupt(_masterWorkerNode)==true)
			{
				return false;
			}
		}
			
		return true;
		
	}
	
	
	/**
	 * Prueft ein WorkerNode auf Korrektheit.
	 * @param workerNodeToTest
	 * @return true wenn der WorkerNode defekt ist
	 */
	private boolean isCurrupt(IWorkerNode workerNodeToTest)
	{
		int workerNodeId;
		
		try
		{
			workerNodeId = workerNodeToTest.getId();
		}
		catch (SimonRemoteException e)
		{
			return true;
		}
		
		return false;
	}

}
