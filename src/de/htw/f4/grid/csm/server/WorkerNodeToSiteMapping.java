package de.htw.f4.grid.csm.server;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.ISite;


/**
 * Verknpüft WorkerNode mit der dazugehörigen Site
 */
public class WorkerNodeToSiteMapping
{
	private IWorkerNode _workerNode;
	private ISite _site;
	
	
	
	public WorkerNodeToSiteMapping(IWorkerNode workerNode, ISite site)
	{
		_workerNode = workerNode;
		_site = site;
	}
	

	public IWorkerNode getWorkerNode()
	{
		return _workerNode;
	}



	public ISite getSite()
	{
		return _site;
	}
	
}
