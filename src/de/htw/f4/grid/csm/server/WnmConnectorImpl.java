package de.htw.f4.grid.csm.server;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.WorkerNodeImpl;
import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.jobs.JobResult;
import de.htw.f4.grid.csm.jobs.interfaces.IJob;
import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;
import de.htw.f4.grid.csm.server.interfaces.IBidirectionalMap;
import de.htw.f4.grid.csm.server.interfaces.ISite;
import de.htw.f4.grid.csm.server.interfaces.IVirtualWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IWnm;
import de.htw.f4.grid.csm.server.interfaces.IWnmConnector;
import de.htw.f4.grid.csm.util.DebugHelper;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.htw.f4.grid.csm.util.execptions.NotEnoughWorkerNodes;
import de.htw.f4.grid.csm.util.workernode.CreateVirtualWorkerNode;
import de.htw.f4.grid.csm.util.workernode.WorkerNodeTreeRepresentation;
import de.htw.f4.grid.schachlogik.DistributedSearch;
import de.root1.simon.SimonRemoteInstance;
import de.root1.simon.exceptions.SimonRemoteException;

public class WnmConnectorImpl implements IWnmConnector, Serializable
{
	private int _connectorId;
	private IWnm _workerNodeManager;
	private final static Logger _log = Logger.getLogger(WnmConnectorImpl.class);
	
	
//	private IBidirectionalMap<IWorkerNode,IWorkerNode> _bidirectionalWorkerNodeList;
	
	//Liste aller zugewiesenen/vergebener WorkerNodes
	private HashMap<Integer,ArrayList<IVirtualWorkerNode>> _workerNodeLists; 
	
	private int lastListId;
	//<virtuellerWorkerNode,workerNode>
	
	private ThreadPoolExecutor _threadPool; //Threads u.a. zum Erzeugen von virtuellen WorkerNodes
	private ThreadPoolExecutor _threadPoolForBuildingVirtualWorkerNodes;
	private ThreadPoolExecutor _pool2;
	
	private int _gameCount; //Anzahl der durchgeführten Züge
	
	public WnmConnectorImpl(IWnm wnm) throws SimonRemoteException
	{
		_pool2 =  new ThreadPoolExecutor(128,
				  768,
				  0L,
			  	  TimeUnit.SECONDS,
			  	  new LinkedBlockingQueue<Runnable>());
		_workerNodeManager = wnm;
		_connectorId = 0; //TODO schöner wenn die ID vom Server generiert wird
		
		_workerNodeLists = new HashMap<Integer,ArrayList<IVirtualWorkerNode>>();
		
		this.lastListId = 0;
		
		
//		this._workerNodeList = new WorkerNodeListWithManagementImpl();
//		this.log.info("WnmImpl started");
//		
//		startWorkerNodeLifeCheck();
	}
	

	private void setGameCount(int gameCount)
	{
		_gameCount = gameCount;
		DebugHelper.CURRENT_LIST_ID = _gameCount;
	}
	
	public ArrayList<IVirtualWorkerNode> getWorkerNodeList(int count)
	{
		ArrayList<IVirtualWorkerNode> resultList = new ArrayList<IVirtualWorkerNode>();
		ArrayList<IWorkerNode> freeWorkerNodeList = new ArrayList<IWorkerNode>();		
		int newMapId = -1;

		
		try
		{
			
			
			
			_log.debug("Zeitmessung: getWorkerNodeList(): Hole freie WorkerNodes...");
			long start = System.currentTimeMillis();
			try
			{
				freeWorkerNodeList = _workerNodeManager.getFreeWorkerNodeList(count, _connectorId); //190ms bei 8 Nodes (HTW<->Marco)
			}
			catch (NotEnoughWorkerNodes e)
			{
				_log.error("Nicht genügend WorkerNodes vorhanden. Anfrage nach " + count + " WorkerNodes gescheitert");
				freeWorkerNodeList = new ArrayList<IWorkerNode>(0);
			} 
			
			_log.debug("Zeitmessung:# getWorkerNodeList()3: ...freie WorkerNodes stehen zur Verfügung (\n#T000#" + DebugHelper.CURRENT_LIST_ID + "#T004#" + String.valueOf(System.currentTimeMillis()-start) +  "#ms)");
			
			newMapId = this.lastListId++;
			
			_log.debug("Zeitmessung:\n#T000#" + DebugHelper.CURRENT_LIST_ID);
			
			//verknüpfe WorkerNodes<->virtuelleWorkerNodes
			_log.debug("Zeitmessung: getWorkerNodeList(): verknüpfe WorkerNodes<->virtuelleWorkerNodes...");
			long start2 = System.currentTimeMillis();
			resultList = buildVirtualWorkerNodes(freeWorkerNodeList, newMapId);					//94ms bei 8 WNs (HTW<->Marco)
			_log.debug("Zeitmessung: getWorkerNodeList()4: ...verknüpfe WorkerNodes<->virtuelleWorkerNodes beendet. (" + String.valueOf(System.currentTimeMillis()-start2) +  " ms)");
			
			_workerNodeLists.put(newMapId,resultList);
		}
		catch (SimonRemoteException e)
		{
			String message = "Aufruf von getFreeWorkerNodeList() fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
		}

		return resultList;
	}
	
	
	
	/**
	 * Erzeugt eine Liste von VirtuellenWorkerNodes anhand von "realen" WorkerNodes
	 * @param workerNodes
	 * @return
	 */
	private ArrayList<IVirtualWorkerNode> buildVirtualWorkerNodes(ArrayList<IWorkerNode> workerNodes, int listId)
	{
		ArrayList<IVirtualWorkerNode> result = new ArrayList<IVirtualWorkerNode>();
		long timeout = 5000; //in ms
		ThreadPoolExecutor pool = getThreadPoolForBuildingVirtualWorkerNodes();
		
		//Arbeitsaufträge erstellen
		LinkedList<Callable<IVirtualWorkerNode>> serviceList = new LinkedList<Callable<IVirtualWorkerNode>>();
		for (IWorkerNode workerNode : workerNodes)
		{
			serviceList.add(new CreateVirtualWorkerNode(workerNode, listId));
		}

		//vorzeitiges Beenden, falls keine Elemente in der ServiceList enthalten sind
		if (serviceList.size()<=0) return result;
		
		try
		{
			//Arbeitsaufträge starten und auf Ergebnis warten
			List<Future<IVirtualWorkerNode>> futureList;
			futureList = pool.invokeAll(serviceList,timeout, TimeUnit.MILLISECONDS); //warte maximal 5sek auf das Ergebnis
	
			//Ergebnisse einsammeln
			for (int i = 0; i < futureList.size(); i++)
			{
				Future<IVirtualWorkerNode> currentVirtualWorkerNode = futureList.get(i);
				IVirtualWorkerNode virtualWorkerNode;
				if (currentVirtualWorkerNode==null)
				{
					_log.error("buildVirtualWorkerNodes(): currentVirtualWorkerNode==null");
					/*
					  
					  java.util.concurrent.CancellationException
					  "Exception indicating that the result of a value-producing task, such as a FutureTask, cannot be retrieved because the task was cancelled."
					  
					  
					  de.htw.f4.grid.jschess.WinboardCommunication 2010-06-05 01:58:55,111 -- ERROR -- Fehler beim Abruf von opponentTurn(e1d2)
de.htw.f4.grid.jschess.WinboardCommunication 2010-06-05 01:58:55,112 -- DEBUG -- Fehler beim Abruf von opponentTurn(e1d2)
java.util.concurrent.CancellationException
        at java.util.concurrent.FutureTask$Sync.innerGet(FutureTask.java:220)
        at java.util.concurrent.FutureTask.get(FutureTask.java:83)
        at de.htw.f4.grid.csm.server.WnmConnectorImpl.buildVirtualWorkerNodes(WnmConnectorImpl.java:160)
        at de.htw.f4.grid.csm.server.WnmConnectorImpl.getWorkerNodeList(WnmConnectorImpl.java:111)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:39)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:25)
        at java.lang.reflect.Method.invoke(Method.java:597)
        at de.root1.simon.ProcessMessageRunnable.processInvoke(ProcessMessageRunnable.java:371)
        at de.root1.simon.ProcessMessageRunnable.run(ProcessMessageRunnable.java:91)
        at java.util.concurrent.ThreadPoolExecutor$Worker.runTask(ThreadPoolExecutor.java:886)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:908)
        at java.lang.Thread.run(Thread.java:619)
					 */
				}
				virtualWorkerNode = currentVirtualWorkerNode.get();
				if (virtualWorkerNode==null)
				{
					_log.error("buildVirtualWorkerNodes(): VirtualWorkerNode konnte nicht erzeugt werden (Null-Referenz)");
				}
				else
				{
					result.add(virtualWorkerNode);
				}
			}
			_log.debug("buildVirtualWorkerNodes(): serviceList.size=" + serviceList.size() +" <--> result.size=" + result.size());
			
			
			//pool leeren
			pool.shutdownNow(); //TODO __Marco - pool wirklich immer stoppen oder einfach so lassen?
			// Anmerkung: Der Theadpool wird immer neu erzeugt. Das ist auch gut so!
		}
		catch (InterruptedException e)
		{
			_log.error("Erstellung eines VirtuellenWorkerNodes fehlgeschlagen (InterruptedException)");
		}
		catch (CancellationException e)
		{
			_log.error("buildVirtualWorkerNodes(): 'java.util.concurrent.CancellationException'");
		}
		catch (ExecutionException e)
		{
			_log.error("Erstellung eines VirtuellenWorkerNodes fehlgeschlagen (ExecutionException)");
		}
		
		
		return result;
	}
	
	
	
	public void startJob(IJob job, IVirtualWorkerNode virtualWorkerNode)
	{
		_log.debug("startJob() aufgerufen");
		int workerNodeId = -1;
		
		workerNodeId = virtualWorkerNode.getWorkerNodeId();
		try
		{
			_workerNodeManager.startJob(job, workerNodeId, this._connectorId);
		}
		catch (SimonRemoteException e)
		{
			String message = "Job kann nicht gestartet werden.";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
		}
	}
	
	public IJobResult getResult(String moveHash)
	{
		int workerNodeId = -1;
		IJobResult result = null;
		
		
		
		try
		{
			result = this._workerNodeManager.getJobResult(moveHash);
		}
		catch (SimonRemoteException e)
		{
			String message = "Abrufen des JobErgebnisses fehlgeschlagen";
			this._log.error(message);
			return null;
		}
		
		return result;
	}
	
	
	
	public void stopJobs(ArrayList<IVirtualWorkerNode> virtualWorkerNodes) throws SimonRemoteException
	{
//		ThreadPoolExecutor pool = _pool2;
////		pool = new ThreadPoolExecutor(128,
////				  768,
////				  0L,
////			  	  TimeUnit.SECONDS,
////			  	  new LinkedBlockingQueue<Runnable>());
//		
//		long timeout = 5000; //5sek
//
//		//Arbeitsaufträge erstellen
//		LinkedList<Callable<Boolean>> serviceList = new LinkedList<Callable<Boolean>>();
//		for (IVirtualWorkerNode workerNode : virtualWorkerNodes)
//		{
//			serviceList.add(new StopVirtualWorkerNodeJobs(virtualWorkerNodes, _workerNodeManager));
//		}
//
//		//vorzeitiges Beenden, falls keine Elemente in der ServiceList enthalten sind
//		if (serviceList.size()<=0) return;
//		
//		//Arbeitsaufträge starten und auf Ergebnis warten
//		List<Future<Boolean>> futureList;
//		try
//		{
//			futureList = pool.invokeAll(serviceList,timeout, TimeUnit.MILLISECONDS); //warte maximal 5sek auf das Ergebnis
//		}
//		catch (InterruptedException e)
//		{
//			_log.error("stopJobs(): Job konnte nicht rechtzeitig beendet werden");
//		} 
//
//		
////		try
////		{
////			pool.shutdownNow();
////		}
////		catch (Exception e)
////		{
////			_log.error("stopJobs(): 'pool.shutdownNow()' fehlgeschlagen");
////		}
//		
//		_log.info("stopJobs(): Auftrag 'Jobs von virtuellenWorkerNodes sollen hart beendet werden' abgeschlossen");
////		//Ergebnisse einsammeln
////		for (int i = 0; i < futureList.size(); i++)
////		{
////			Future<IVirtualWorkerNode> currentVirtualWorkerNode = futureList.get(i);
////			IVirtualWorkerNode virtualWorkerNode;
////			if (currentVirtualWorkerNode==null)
////			{
////				_log.error("buildVirtualWorkerNodes(): currentVirtualWorkerNode==null");
////			}
////			virtualWorkerNode = currentVirtualWorkerNode.get();
////			if (virtualWorkerNode==null)
////			{
////				_log.error("buildVirtualWorkerNodes(): VirtualWorkerNode konnte nicht erzeugt werden (Null-Referenz)");
////			}
////			else
////			{
////				result.add(virtualWorkerNode);
////			}
////		}
		
		
		
		StopVirtualWorkerNodeJobs stopVirtualWorkerNodeJobs = new StopVirtualWorkerNodeJobs(virtualWorkerNodes, _workerNodeManager);

		//stoppen threaden
		Thread stopJobsThread = new Thread(stopVirtualWorkerNodeJobs);
		stopJobsThread.start();
		
		_log.info("Auftrag erteilt: Jobs von virtuellenWorkerNodes sollen hart beendet werden");
	}

	
	
	private ArrayList<IWorkerNode> getWorkerNodeListFromBidirectionalMap(
			IBidirectionalMap<IVirtualWorkerNode, IWorkerNode> biWorkerNodeMap)
	{
		ArrayList<IWorkerNode> resultList = new ArrayList<IWorkerNode>();
		
		resultList = biWorkerNodeMap.getAllWorkerNodes();
		
		return resultList;
	}

	
	public int getFreeWorkerNodeCount()
	{
		int freeCount = -1;
		try
		{
			freeCount = _workerNodeManager.getFreeWorkerNodeCount();
		}
		catch (SimonRemoteException e)
		{
			String message = "Aufruf von getFreeWorkerNodeCount() fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
		}
		
		return freeCount;
	}


	
	
	/**
	 * Sorgt dafuer, dass alle WNs ihren momentanen Job (hart) beenden und wieder zur freien
	 * Verfuegung stehen
	 *   
	 * @param workerNodeList - WorkerNodes die freigegeben werden duerfen
	 * @throws SimonRemoteException
	 */
	public void releaseWorkerNodeList(ArrayList<IVirtualWorkerNode> workerNodeList, int gameCount)	throws SimonRemoteException
	{
		setGameCount(gameCount);
		
		int listId = -1;
		IVirtualWorkerNode firstClientVirtualWorkerNode;
		
		//vorzeitiges Abbrechen, fall noch gar keine VirtualWorkerNodes für den Client existieren
		if (workerNodeList==null)
		{
			return;
		}
		
		//vorzeitiges Abbrechen, fall noch gar keine VirtualWorkerNodes für den Client existieren
		if (workerNodeList.size()<=0)
		{
			return;
		}
		
		
		
		//Bestimmung der ListId, indem diese vom Ersten VirtualWorkerNode abgefragt wird
		firstClientVirtualWorkerNode = workerNodeList.get(0);
		
		//vorzeitiges Abbrechen, fall noch gar keine VirtualWorkerNodes für den Client existieren
		if (firstClientVirtualWorkerNode==null)
		{
			_log.info("ClientId=" + _connectorId + " verfügt über keine WorkerNodes, deshalb können auch keine freigegeben werden");
			return;
		}
		
		
		listId = firstClientVirtualWorkerNode.getListId();
		
		//unbenutze WorkerNode freigeben
		this._workerNodeManager.releaseWorkerNodeList(listId, _connectorId);
	}

	
	
	public void releaseAllWorkerNodesFromList() throws SimonRemoteException
	{
		_workerNodeManager.releaseWorkerNodeList(_connectorId);		
	}

	
	private ThreadPoolExecutor getThreadPoolForBuildingVirtualWorkerNodes()
	{
//		if (_threadPoolForBuildingVirtualWorkerNodes==null)
//		{
			_threadPoolForBuildingVirtualWorkerNodes = new ThreadPoolExecutor(128,
																			  1024,
																			  0L,
																		  	  TimeUnit.SECONDS,
																		  	  new LinkedBlockingQueue<Runnable>());
//		}
		
		return _threadPoolForBuildingVirtualWorkerNodes;
	}
}
