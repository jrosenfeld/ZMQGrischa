package de.htw.f4.grid.csm.server.interfaces;

public interface IVirtualWorkerNode
{
	public int getWorkerNodeId();
	
	public int getListId();
}
