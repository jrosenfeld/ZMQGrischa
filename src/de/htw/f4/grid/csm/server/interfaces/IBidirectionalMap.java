package de.htw.f4.grid.csm.server.interfaces;

import java.util.ArrayList;

public interface IBidirectionalMap<T1,T2>
{
	public void put(T1 key, T2 value) ;

    public T2 getWorkerNode(T1 key) ;
    
    public T1 getVirtualWorkerNode(T2 value) ;
    
    public ArrayList<T2> getAllWorkerNodes();
    
    public ArrayList<T1> getAllVirtualWorkerNodes();
    
}
