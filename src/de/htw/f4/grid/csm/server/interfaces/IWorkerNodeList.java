package de.htw.f4.grid.csm.server.interfaces;

import java.util.ArrayList;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;

/**
 * Interface, welches dem Schach-Prgramm angeboten wird.
 * 
 * Enthaelt mehrere Listen von Worker-Nodes mit unterschiedlichen Anforderungen.
 * Das Schach-Programm stellt eine Anfrage von z.B. 10 WorkerNodes und erhaelt
 * als Antworte eine Instanz von diesem Interface.
 * Diese Interface hat Kenntnis ueber die Gruppierung der Worker-Nodes.
 * Ueber methoden koennen z.B. alle Worker-Nodes als ArrayListe zurueckgegeben werden,
 * oder aber nur alle Worker-Nodes einer bestimmten Site (==Rechenzentrum) 
 *  
 */
public interface IWorkerNodeList
{
	/**
	 * Liefert eine Liste mit allen enthaltenen Worker-Nodes
	 */
	public ArrayList<IWorkerNode> getAllWorkerNodes();
	

	//TODO: public ArrayList<IWorkerNode> getXWorkerNodes(); Rueckgabe von x Workernodes
	
	/**
	 * Liefert einen einzelnen Worker-Node zurueckt. Bestimmt wird
	 * dieser ueber seine eindeutige ID.
	 */
	public IWorkerNode get(int workerNodeId);
	
	
	public int size();


	public void removeWorkerNodeFromHashMap(int id);
	
	public void removeWorkerNode(IWorkerNode workerNode);
	

}
