package de.htw.f4.grid.csm.server.interfaces;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;

public interface IVirtualWorkerNodeMapping
{
	public void setMapping(IWorkerNode virtualWorkerNode, IWorkerNode workerNode);
	
	public IWorkerNode getVirtualWorkerNode();
	public IWorkerNode getWorkerNode();
}
