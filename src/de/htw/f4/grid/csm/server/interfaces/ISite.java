package de.htw.f4.grid.csm.server.interfaces;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;


/**
 * Beschreibt eine Site.
 * Eine Site kann z.B. sein: Hamburg oder Zeuthen 
 *
 */
public interface ISite
{
	public String getName();
	
	
	public String getIpAdressess();
	
	
	/**
	 * Liefert den Master-WorkerNode der Site.
	 * @return
	 */
	public IWorkerNode getMasterWorkerNode();
	
	
	/**
	 * Legt den Master-WorkerNode der Site fest.
	 * @param einWorkerNode
	 */
	public void setMasterWorkerNode(IWorkerNode einWorkerNode);
	
	
	/**
	 * Prüft, ob ein MasterWorkerNode zugewiesen ist
	 * @return true, wenn MasterWorkerNode vorhanden, false wenn nicht
	 */
	public boolean hasMasterWorkerNode();
}
