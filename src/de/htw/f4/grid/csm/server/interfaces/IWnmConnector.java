package de.htw.f4.grid.csm.server.interfaces;


import java.util.ArrayList;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.jobs.interfaces.IJob;
import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;
import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;

/*
 * Soll dazu dienen eine Verbindung zum Server herszustellen und WorkerNodes zu bekommen 
 * und freizugeben. 
 */
public interface IWnmConnector extends SimonRemote
{
	/*
	 * Gibt eine Liste mit n Workernodes zurueck
	 */
	public ArrayList<IVirtualWorkerNode> getWorkerNodeList(int count) throws SimonRemoteException;
	
	
	/*
	 * Gibt an wieviele WNs gerade zur freien Verfuegung stehen 
	 */
	public int getFreeWorkerNodeCount() throws SimonRemoteException;
	

	/**
	 * Sorgt dafuer, dass alle WNs ihren momentanen Job beenden und wieder zur freien
	 * Verfuegung stehen
	 *   
	 * @param workerNodeList - WorkerNodes die freigegeben werden duerfen
	 * @param gameCount - DEBUG-value, Anzahl der insgesamt durchgeführten Spiele. Wird für DEBUG-Zwecke benötigt
	 * @throws SimonRemoteException
	 */
	public void releaseWorkerNodeList(ArrayList<IVirtualWorkerNode> workerNodeList, int gameCount) throws SimonRemoteException;
	
	
	/**
	 * Löscht alle WorkerNode-Listen vom aktuellen Client.
	 * Wird i.d.R. beim ersten Programmstart vnon GridGameManager aufgerufen.
	 * 
	 * @throws SimonRemoteException
	 */
	public void releaseAllWorkerNodesFromList() throws SimonRemoteException;
	
	public void startJob(IJob job, IVirtualWorkerNode virtualWorkerNode) throws SimonRemoteException;
	
	public IJobResult getResult(String moveHash) throws SimonRemoteException;
	
	
	/**
	 * Beendet die laufenden Jobs aller übergebenen WorkerNodes
	 * @param virtualWorkerNodeList
	 * @throws SimonRemoteException
	 */
	public void stopJobs(ArrayList<IVirtualWorkerNode> virtualWorkerNodeList) throws SimonRemoteException;
}
