package de.htw.f4.grid.csm.server.interfaces;

import java.io.FileWriter;
import java.util.ArrayList;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.jobs.interfaces.IJob;
import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;
import de.htw.f4.grid.csm.util.execptions.NotEnoughWorkerNodes;
import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;


/**
 * worker node manager (WNM)
 *  
 * Der WNM stellt Listen mit beliebiger Anzahl von WorkerNodes bereit.
 * Wird eine groeßere Anzahl an WorkerNodes benoetigt als bisher an dem WNM
 * angemeldet, so wird im Hintergrund versucht neue WorkerNodes
 * zu erzeugen.
 *
 */
public interface IWnm extends SimonRemote
{

	/**
	 * Startet den WNM. Damit beginnt auch die Aquerierung von WorkerNodes.
	 */
	public void startService();
	
	
	/**
	 * Stoppt den WNM. Alle bisher angemeldeten WorkerNodes werden beendet und abgmeldet.
	 */
	public void stopService();
	
	
	/**
	 * Ein WorkerNode meldet sich hiermit bei dem WNM an.
	 * 
	 * @param object
	 * @throws SimonRemoteException
	 */
	public void registerWorkerNode(IWorkerNode object) throws SimonRemoteException;
	
	
	/**
	 * Ein WorkerNodes wird hiermit von dem WNM entfernt.
	 */
	public void releaseWorkerNode(IWorkerNode object) throws SimonRemoteException;
	

	/**
	 * Alle angemeldeten WorkerNodes werden aus dem WNM entfernt.
	 * @param shutDown - true, dann beendet sich autom. der WorkerNode-Prozess
	 * @throws SimonRemoteException
	 */
	public void releaseAllWorkerNodes(boolean shutDown) throws SimonRemoteException;
	
	/**
	 * Erzeugt eine Liste mit Worker-Nodes.
	 * @param numberOfWorkerNodes Anzahl der Worker-Nodes, die in der Liste enthalten sein sollen.
	 * @return
	 */
	public IWorkerNodeList getWorkerNodeList(int numberOfWorkerNodes) throws SimonRemoteException;
	
	public int getNumberOfRegisteredWorkerNodes() throws SimonRemoteException;
	
	
	/**
 	 * DEBUG ONLY!!!
	 * print out workerNode-tree
	 * @return - Ausgabe der TreeMap als String
	 * @throws SimonRemoteException
	 */
	public StringBuffer parseWorkerNodeTree(boolean printOut) throws SimonRemoteException;
	
	
	/**
	 * DEBUG ONLY !!!
	 */
	public void startTestPingmessung() throws SimonRemoteException;
	
	
	/**
	 * DEBUG ONLY !!!
	 */
	public void saveTreeRepresentationAsDotFile(FileWriter outputFile) throws SimonRemoteException;
	
	
	/**
	 * Anfrage an den WNM, um den angegeben WorkerNode entfernen zu lassen  
	 * @param workerNode
	 */
	public void removeWorkerNode(IWorkerNode workerNode) throws SimonRemoteException;
	
	
	
	/**
	 * Gibt eine Liste mit n Workernodes zurueck
	 * 
	 * @param count
	 * @param clientId - eindeutige ID des Clients, der WorkerNodes beim Server verwenden möchte 
	 * @return
	 */
	public ArrayList<IWorkerNode> getFreeWorkerNodeList(int count, int clientId) throws SimonRemoteException, NotEnoughWorkerNodes;
	
	public IWorkerNode getWorkerNode(int id) throws SimonRemoteException;
	
	
	/**
	 * Gibt eine Liste zurück, die n freie WorkerNodes enthält. Sind weniger WorkerNodes vorhanden als benötigt,
	 * so wird die Exeption "NotEnoughWorkerNodes" geworfen.
	 * @param count
	 * @return
	 */
	public ArrayList<IWorkerNode> getFreeWorkerNodes(int count) throws SimonRemoteException, NotEnoughWorkerNodes;
	
	
	/**
	 * Gibt an wieviele WNs gerade zur freien Verfuegung stehen 
	 * 
	 * @return
	 */
	public int getFreeWorkerNodeCount() throws SimonRemoteException;
	
	public void startJob(IJob job, int workerNodeId, int clientId) throws SimonRemoteException;
	
	public IJobResult getJobResult(String moveHash) throws SimonRemoteException;
	
	
//	public void stopJob(int workerNodeId, int clientId) throws SimonRemoteException;
	
	
	/***
	 * output a given message directly on the Wnm stdout/logs
	 */
	public void sendMessageToWnm(String message) throws SimonRemoteException;
	
	
	
	/**
	 * Sorgt dafuer, dass alle WNs ihren momentanen Job beenden und wieder zur freien
	 * Verfuegung stehen (von WnmConnector initiiert)
	 *   
	 * @param clientId
	 * @throws SimonRemoteException
	 */
	public void releaseWorkerNodeList(int listId, int clientId)	throws SimonRemoteException;
	

	
	/**
	 * Löscht alle WorkerNode-Listen vom angegebenen Client.
	 * Wird i.d.R. beim ersten Programmstart vnon GridGameManager aufgerufen.
	 * 
	 * @param clientId
	 * @throws SimonRemoteException
	 */
	public void releaseWorkerNodeList(int clientId)	throws SimonRemoteException;
	
	/**
	 * Speichert ein Jobergebnis in der WMN
	 * @param jobResult
	 * @param workerNodeId
	 * @throws SimonRemoteException
	 */
	public void saveJobResult(IJobResult jobResult, String moveHash) throws SimonRemoteException;
	
	

	/**
	 * Beendet alle laufenden Jobs der angegebenen virtuellen WorkerNodes
	 * @param stopWorkerNodeJobs
	 * @throws SimonRemoteException
	 */
	public void stopVirtualWorkerNodeJobs(ArrayList<IVirtualWorkerNode> virtualWorkerNodes) throws SimonRemoteException;
	
	
	/**
	 * Gibt den Dateinamen an, unter dem die Representation der WorkerNodes
	 * abgespeichert wird.
	 * @param filename
	 */
	public void setGraphOutputFilename(String filename);
	
	
	
	/**
	 * Reparieren der WN-Listen anstoßen
	 */
	public void repairWorkerNodes();
	
}
