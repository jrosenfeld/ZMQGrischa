package de.htw.f4.grid.csm.server.interfaces;

import java.io.FileWriter;
import java.util.ArrayList;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;
import de.htw.f4.grid.csm.util.execptions.NotEnoughWorkerNodes;
import de.root1.simon.exceptions.SimonRemoteException;

public interface IServer
{

	/**
	 * start server
	 */
	public void start();
	
	
	/**
	 * stop server
	 */
	public void stop();
	
	/**
	 * return the server status
	 */
	public void getStatus();
	
	public int getWorkerNodeCount();
	
	
	/**
	 * Liefert eine beliebige Anzahl von Worker-Nodes.
	 * Werden mehr Worker-Nodes benoetigt als eigentlich zur Verfuegung stehen
	 * wird eine entsprechende Exception geworfen.
	 * 
	 * return WorkerNodeList
	 */
	public IWorkerNodeList getListOfWorkerNodes(int number);
	
	
	/**
	 * DEBUG ONLY!!!
	 * print out workerNode-tree
	 */
	public StringBuffer parseWorkerNodeTree(boolean printOut);
	
	
	/**
	 * Port auf dem SIMON lautscht.
	 */
	public void setListeningPort(int port);
	
	
	/**
	 * Gibt den Dateinamen an, unter dem die Representation der WorkerNodes
	 * abgespeichert wird.
	 * @param filename
	 */
	public void setGraphOutputFilename(String filename);
	
	
	/**
	 * DEBUG ONLY!!!
	 */
	public void startTestPingmessung();
	
	
	/**
	 * DEBUG ONLY !!!
	 */
	public void saveTreeRepresentationAsDotFile(FileWriter outputFile);
	
	
	/**
	 * Anfrage an den WNM, um den angegeben WorkerNode entfernen zu lassen  
	 * @param workerNode
	 */
	public void removeWorkerNode(IWorkerNode workerNode);
	
	

	/**
	 * Alle am Server angemeldeten WorkerNodes abmelden
	 * @param shutDown - true, dann beendet sich autom. der WorkerNode-Prozess
	 */
	public void releaseAllWorkerNodes(boolean shutDown);
	
	
	
	/**
	 * Anzahl der WNs anzeigen ohne Abfragen an den Nodes auszuführen (Infos werden lokal erzeugt)
	 * @return
	 */
	public int getCountWorkerNodes();
	
	
	/**
	 * Reparieren der WorkerNode-Listen anstoßen
	 */
	public void repairWorkerNodes();
	
	/****************************************************************************/
	/*********************INTERFACES FÜR CHESS-GAME *****************************/
	/****************************************************************************/
	
	/*
	 * Gibt eine Liste mit n Workernodes zurueck
	 */
	public ArrayList<IWorkerNode> getWorkerNodeList(int count) throws SimonRemoteException, NotEnoughWorkerNodes;
	
	
	/*
	 * Gibt an wieviele WNs gerade zur freien Verfuegung stehen 
	 */
	public int getFreeWorkerNodeCount() throws SimonRemoteException;
	

	/**
	 * Sorgt dafuer, dass alle WNs ihren momentanen Job beenden und wieder zur freien
	 * Verfuegung stehen
	 *   
	 * @param workerNodeList - WorkerNodes die freigegeben werden duerfen
	 * @throws SimonRemoteException
	 */
	public void releaseWorkerNodeList(ArrayList<IWorkerNode> workerNodeList) throws SimonRemoteException;
}
