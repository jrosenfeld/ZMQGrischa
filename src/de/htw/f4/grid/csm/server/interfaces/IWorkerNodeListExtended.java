package de.htw.f4.grid.csm.server.interfaces;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;

/**
 * Ein Interface speziell fuer die WNM.
 * 
 * Soll eine Ergaenzung zum IWorkerNodeList-Interface bieten.
 * Es werden Methoden zum Management der Worker-Nodes angeboten,
 * die nur dem WNM zugaenglich sein duerfen.
 *  
 *
 */

public interface IWorkerNodeListExtended
{
	
	/**
	 * Fuegt einen Worker-Node hinzu.
	 * 
	 * @param workerNode
	 */
	public void add(IWorkerNode workerNode);
	
	
	/**
	 * Loescht einen Worker-Node.
	 * @param workerNodeId
	 */
	public void remove(int workerNodeId);

	
	/**
	 * Liefert die Liste von Worker-Nodes als HashMap
	 * @return
	 */
	public HashMap<Integer, IWorkerNode> getWorkerNodeHashMap();
	
	
	
	/**
	 * Liefert eine Liste allen Worker-Nodes einer vorgegebenen Site
	 * @param site
	 * @return
	 */
	public ArrayList<IWorkerNode> getAllWorkerNodesBySite(ISite site);
	

	/**
	 * Durchläuft den WorkerNode-Baum und gibt alle Elemente bei Bedarf aus
	 * 
	 * @param printOut - bei "true" wird die Map zusätzlich ausgegeben
	 * @return
	 */
	public StringBuffer parseTree(boolean printOut);
	
	
	
	/**
	 * Liste aller Sites des WNM
	 * @return
	 */
	public ArrayList<ISite> getAllSites();
	
	
	public void removeWorkerNode(IWorkerNode workerNode);
	
	
	/**
	 * Erstellt ein .dot-File (graphviz). Daraus kann ein Graph generiert werden
	 * @param outputFile
	 */
	public void saveTreeRepresentationAsDotFile(FileWriter outputFile);
	
	
	/**
	 * Gibt den Dateinamen an, unter dem die Representation der WorkerNodes
	 * abgespeichert wird.
	 * @param filename
	 */
	public void setGraphOutputFilename(String filename);
	
	
	/**
	 * Alle WorkerNodes entfernen. Die WorkerNodes wurden bereits über das
	 * Entfernen informiert, deswegen brauch keine extra Fehlerbehandlung
	 * durchgeführt werden. 
	 */
	public void removeAllWorkerNodes();
	
	

	/**
	 * Aktualisiert das Erzeugungsskript für Graphviz, in welchem alle momentan
	 * angemeldeten Workernodes enthalten sind.  
	 */
	public void updateGraphvizRepresentationOfWorkerNodes();
	
	
	/**
	 * Testes eine angegebene List von WorkerNodes, ob diese in Orndung sind.
	 * 
	 * @param workerNodesToTest - Teste dieser WorkerNodes
	 * @param timeout - Zeit (in ms) die maximal gewartet wird bis Ergebnisse vorliegen 
	 * @return - Liste mit Indexen der defekten WorkerNodes
	 */
	public ArrayList<Integer> getCurruptWorkerNodes(ArrayList<IWorkerNode> workerNodesToTest, long timeout);
	
	
	/**
	 * Entfernt alle ungültigen WorkerNodes aus der HashMap
	 */
	public void repairWorkerNodeHashMap();
}
