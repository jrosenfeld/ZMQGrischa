package de.htw.f4.grid.csm.server.interfaces;

import java.net.InetAddress;

/**
 * Meta-Information zu den Worker-Nodes werden hier
 * gesammelt, damit keine unnötige Verbindung zu diesen
 * aufgebaut werden muessen.
 * Dies vermeidet unnoetigen Kommunukationsaufwand.
 * Die Implementierung sollte auf dem WNM hinterlegt sind
 * und auch von ihm verwaltet werden. 
 *
 */
public interface IWorkerNodeMetaInformation
{

	/**
	 * Liefert die Latenz, die zwischem diesen WorkerNode
	 * und dem MasterWorkerNode besteht
	 * @return rtt (in ms) zum MasterWorkerNode
	 */
	public int getAvgRttToMasterWorkerNode();
	public void setAvgRttToMasterWorkerNode(int rtt);
	
	
	/**
 	 * Liefert die IPAdressen aller verfügbaren lokalen NetzwerkInterfaces
	 * des WorkerNodes
	 * @return
	 * @throws SimonRemoteException
	 */
	public InetAddress[] getIpAdresses();
	public void setIpAdresses(InetAddress[] ipAdresses);
}
