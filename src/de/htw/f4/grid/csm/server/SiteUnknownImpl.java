package de.htw.f4.grid.csm.server;

import de.htw.f4.grid.csm.client.interfaces.IWorkerNode;


/**
 * 
 * Unknown Site.
 *
 */
public class SiteUnknownImpl extends SiteAbstract
{
	private final static String SITENAME="unbekannt";
	
	public SiteUnknownImpl()
	{
		super(SITENAME);
	}

	
	public String getIpAdressess()
	{
		// TODO IP-Adresse dynamisch ermitteln
		return "10.0.0.1";
	}


	
}
