package de.htw.f4.grid.schachlogik;

import org.apache.log4j.Logger;


import de.htw.f4.grid.schachInterfaces.IChessGame;



public class IterativeAlphaBetaSearch implements Runnable
{
	private final static Logger _log = Logger.getLogger(IterativeAlphaBetaSearch.class);
	private IChessGame game;
	private Player maximizingPlayer;
	public IChessGame bestTurn;
	
	private int value;
	
	public int getValue()
	{
		return value;
	}

	private int depth;


	public IterativeAlphaBetaSearch(IChessGame game)
	{
		this.game=game;
		this.maximizingPlayer=game.getPlayerToMakeTurn();
		depth=1;
	}
	
	public IterativeAlphaBetaSearch(IChessGame game, Player maximizingPlayer)
	{
		this.game=game;
		this.maximizingPlayer=maximizingPlayer;
		depth=1;
	}
	
	
	public void run() 
	{
		AlphaBetaSearchFixedDepth abp;
		//*** beachten wer maximiert immer so tief suchen das maximierender spieler in
		//*** einem Blatt gerade dran ist
		if(this.maximizingPlayer==game.getPlayerToMakeTurn()) depth=0;
		else depth=1;
		
		//*** thread schleife **************************************************
		while(true) 
		{
			abp=new AlphaBetaSearchFixedDepth();
			value=abp.getAlphaBetaTurn(depth, game);
			//*** Besten Zug merken ********************************************
			bestTurn=abp.nextGame;
			//*** zwei tiefer suchen -> gleicher Spieler am Zug **************************
			depth=depth+2;
			this._log.info("Tiefensuche ist bei: " + depth + "Value: " + value);
			if(depth > 40)
			{
				this._log.info("Tiefensuche wird weich beendet");
				break;
			}
			
		}	
	}
}
	
	