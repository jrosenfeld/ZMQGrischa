package de.htw.f4.grid.schachlogik;

public enum GameState 
{
	LEGAL, ILLEGAL, MATT, DRAW
}
