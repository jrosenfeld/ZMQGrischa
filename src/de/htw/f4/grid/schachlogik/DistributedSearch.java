package de.htw.f4.grid.schachlogik;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;
import de.htw.f4.grid.csm.server.interfaces.IVirtualWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IWnmConnector;
import de.htw.f4.grid.csm.util.DebugHelper;
import de.htw.f4.grid.csm.util.workernode.StartJob;
import de.htw.f4.grid.jschess.GameList;
import de.htw.f4.grid.schachInterfaces.IChessGame;
import de.root1.simon.exceptions.SimonRemoteException;



public class DistributedSearch
{
	private final static Logger _log = Logger.getLogger(DistributedSearch.class);
	private final static long BUFFER_TIME=3000;
	public IChessGame nextGame;
	private TreeMap<String, Integer> resultSet;
	private TreeSet<String> sended;
	private Player maximizingPlayer;
	private ThreadPoolExecutor _threadPool; //Threads zum Abschicken von Jobs 
	private IWnmConnector _wnmConnector;
	private IChessGame _game;

	
	
	public DistributedSearch(GameList gameList){

	}
	
	/**
	 * 
	 * @param wnmConnector
	 * @param nodes
	 * @param game
	 * @param wait
	 * @return
	 * @throws Exception
	 */
	public int getAlphaBetaTurn(IWnmConnector wnmConnector, 
				                ArrayList<IVirtualWorkerNode> nodes, 
								IChessGame game, 
								long wait,
								ThreadPoolExecutor threadPool) throws Exception
	{
		_wnmConnector = wnmConnector;
		_game = game;
		_threadPool = threadPool;
		long start = System.currentTimeMillis();
		_log.debug("Zeitmessung: getAlphaBetaTurn(): initialisieren");
		
		//*** Listen initialisieren ******************************************************
		ArrayList<IChessGame> gamesToCompute=new ArrayList<IChessGame>();
		ArrayList<String> computedGamesAsStrings=new ArrayList<String>();
		resultSet = new TreeMap<String, Integer>();
		sended=new TreeSet<String>();
		ArrayList<StartJob> startJobList = new ArrayList<StartJob>(); //enthält Arbeisaufträge, welche WorkerNodes Jobs zuordnen
		
		//*** maximierenden Spieler merken ***********************************************
		this.maximizingPlayer=_game.getPlayerToMakeTurn();
		
		//*** Zu berechnende Stellungen holen ********************************************
		gamesToCompute=this.splitWork(_game, nodes.size());
		
		

		for(int i=0; i<gamesToCompute.size(); i++) {
			if (i >= nodes.size()) {
				this.computeLocaly(gamesToCompute.get(i));
				continue;
			}
			
			StartJob newJob = new StartJob(wnmConnector, nodes.get(i), gamesToCompute.get(i), wait, BUFFER_TIME, maximizingPlayer, resultSet, sended);
			startJobList.add(newJob);
		}

		
		//Arbeit abschicken und auf Ergebnis warten
		_log.info("getAlphaBetaTurn(): Starten aller Jobs wird beauftragt...");
		startJobAsThread(startJobList, wait/2); //warte max. die hälfte der normalen Wartezeit auf das Zustellen der Jobs
		
		_log.debug("Zeitmessung:# getAlphaBetaTurn(): Jobs verschicken beendet, took  \n#T000#" + DebugHelper.CURRENT_LIST_ID + "#T003#" + String.valueOf(System.currentTimeMillis()-start) + "#ms");
		
		
		//nachdem alle Jobs abgeschickt wurden: Strings aus den berechneten spielen machen
		for(int i=0; i<gamesToCompute.size(); i++)
		{
			computedGamesAsStrings.add(gamesToCompute.get(i).getStringRepresentation());
		}
		
		int abgeschickteJobs = startJobList.size();
		_log.info(sended.size()+" / "+nodes.size()+"nodes rechnen");
		_log.debug("Zeitmessung#: getAlphaBetaTurn(): Es wurden " + abgeschickteJobs + 
				   " Jobs verschickt\n#T000#" + DebugHelper.CURRENT_LIST_ID + "#T001#"+sended.size());
		_log.debug("Zeitmessung: Warte ab jetzt "+ wait +
		"ms auf die Ergebnisse der Berechnungen");	
		
		//*** Warten *********************************************************************
		Thread.sleep(wait);
		
		_log.debug("Zeitmessung: Wartezeit abgelaufen. Beginne mit dem Einsammeln der Ergebnisse.");
		
		//Ergebnisse einsammeln
		this.collectJobResults(gamesToCompute);
		_log.debug(this.resultSet.size() + " Jobergebnisse wurden eingesammelt");
		
		
		//*** Ergebnisse auswerten *******************************************************
		//computedGamesAsStrings --> [txldklstbbbbbbbbxxsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxBBBBBBBBTSLDKLSTS, txldklstbbbbbbbbsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxBBBBBBBBTSLDKLSTS, ...
		//resultSet              --> {tsldklstbbbbbbbbxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxBBBBBBBBTSLDKLSTw=10, tsldklstbbbbbbbxxxxxxxxbxxxxxxxxxxxxxxxxxxxxxxxxBBBBBBBBTSLDKLSTS=-8, ...
		AlphaBetaSearchGridResults abs=new AlphaBetaSearchGridResults(computedGamesAsStrings, resultSet);
		int value=abs.getAlphaBetaTurn(0, _game);
		this.nextGame=abs.nextGame;
		
//		results.containsKey(game.getStringRepresentation())
		
		_log.debug("Zeitmessung: Das Endergebnis steht fest.");
		
		return value;
	}

	

	
	private void collectJobResults(ArrayList<IChessGame> gamesToCompute) {
		IJobResult<Integer> result = null;
		String currentGameStringRepresentation;
		IVirtualWorkerNode node;

		for (IChessGame game : gamesToCompute) {

			try {
				result = _wnmConnector.getResult(game.getHash());
			} catch (SimonRemoteException e) {
				_log.error(
						"collectJobResults(): Ausführung von 'this.wnmConnector.getResult(this.node)'  fehlgeschlagen",
						e);
				return;
			}

			// TODO: stopJob am Ende von allen Ergebnissen gesammelt aufrufen
			// this.wnmConnector.stopJob(this.node);

			// kein Ergebnis vorhanden?
			if (result == null) {
				_log.error("Der Job eines WorkerNodes konnte kein Ergebnis lieferen.");
				return;
			}

			if (result.getResult() == null) {
				_log.error("result.getResult()==null");
				return;
			}

			// hole passendes Spiel zum WorkerNodes
			currentGameStringRepresentation = game.getStringRepresentation();

			if ((currentGameStringRepresentation.getBytes())[64] == _game
					.getStringRepresentation().getBytes()[64]) {
				resultSet.put(currentGameStringRepresentation,
						result.getResult());
			} else {
				resultSet.put(currentGameStringRepresentation,
						result.getResult() * (-1));
			}
		}
	}




	/**
	 * Verschickt einen Job. Dieser Vorgang wird als Thread ausgeführt.
	 * 
	 * @param wnmConnector
	 * @param node
	 * @param game
	 * @param wait
	 * @param threadGroupForJobs
	 * @param BUFFER_TIME
	 * @param maximizingPlayer
	 * @param resultSet
	 * @param sended
	 */
	private void startJobAsThread(ArrayList<StartJob> jobSubmitTasks, long timeout)
	{
		ThreadPoolExecutor pool = getThreadPool();
		
		//Arbeitsaufträge erstellen
		LinkedList<Callable<Boolean>> serviceList = new LinkedList<Callable<Boolean>>();
		for (StartJob startJob : jobSubmitTasks)
		{
			serviceList.add(startJob);
		}

		//vorzeitiges Beenden, falls keine Elemente in der ServiceList enthalten sind
		if (serviceList.size()<=0) return;
		
		//Arbeitsaufträge vorbereiten --> jeder WorkerNode wird in einem extra Thread getestet
//		pool = new ThreadPoolExecutor(serviceList.size(),
//									  serviceList.size(),
//									  0L,
//									  TimeUnit.SECONDS,
//									  new LinkedBlockingQueue<Runnable>());

		try
		{
			//Arbeitsaufträge starten und auf Ergebnis warten
			List<Future<Boolean>> futureList;
			futureList = pool.invokeAll(serviceList, timeout, TimeUnit.MILLISECONDS);
			
			
			//Einsammeln der Ergebnisse gar nicht nötig, da der Job die Ergebnisse in eine Variable
			//schreibt, die beim Konstrukur angegeben wurde, also jederzeit bekannt ist.
			
//			//sammle Ergebnisse ein
//			for (int i = 0; i < futureList.size(); i++)
//			{
//				Future<Boolean> result = futureList.get(i);
//				
//				//Fehler ausgeben, falls der Job nicht erfolgreich übermittelt wurde
//				try
//				{
//					if (result.get()==false)
//					{
//						_log.error("Zustellung eins Jobs fehlgeschlagen (result.get()==false)");
//					}
//				}
//				catch (ExecutionException e)
//				{
//					_log.error("Zustellung eins Jobs fehlgeschlagen (ExecutionException)");
//				}
//			}
		}
		catch (InterruptedException e)
		{
			_log.error("Arbeitsauftrag \"Verschicke Job zum WorkerNode\" fehlgeschlagen (InterruptedException)");
		}
		
//		StartJob newStartJob;
//		newStartJob = new StartJob(wnmConnector, node, game, wait, threadGroupForJobs, BUFFER_TIME, maximizingPlayer, resultSet, sended);
//		Thread newJobSubmitThread = new Thread(threadGroupForJobs, newStartJob);
//		newJobSubmitThread.start();
	}
	
	
	
	/* Liefert eine Liste mit spielen zurück die unteruscht werden müssen */
	private ArrayList<IChessGame> splitWork(IChessGame game, int nodeCount)throws Exception
	{
		ArrayList<IChessGame> nextGames=game.getNextTurns();

		if (nextGames.size() >= nodeCount) {
			return nextGames;
		}
		
		while (nextGames.size() < nodeCount) {
			for (int i=0; i<nextGames.size(); i++) {
				IChessGame tmpGame = nextGames.get(i);
				ArrayList<IChessGame> tmpGames = tmpGame.getNextTurns();
				
				if ((nextGames.size() + tmpGames.size() - 1) >= nodeCount) {
					return nextGames;
					
				} 
				
				nextGames.remove(i);
				nextGames.addAll(tmpGames);
			}	 
		}
		
		return nextGames;
	}
	
//	
//	private void sendJob(IWnmConnector wnmConnector, 
//						 IVirtualWorkerNode node, 
//						 IChessGame game, 
//						 long wait, 
//						 ThreadGroup threadGroupForJobs)
//	{
//		//*** Wartezeit anpassen *********************************************************
//		long modifiedWaitTime = wait-BUFFER_TIME;						
//
//		//*** Suche anlegen **************************************************************
//		GridSearch currentGridSearch;							
//		currentGridSearch = new GridSearch(wnmConnector, node, game, resultSet,
//										   maximizingPlayer, modifiedWaitTime);
//		
//		
//		//*** Suche starten, Thread dabei in Gruppen paken *******************************
//		Thread t=new Thread(threadGroupForJobs, currentGridSearch);	
//		t.start();	
//		
//		
//		//*** Das gesendete Brett merken *************************************************
//		sended.add(game.getStringRepresentation());							
//	}
	
	
	private void computeLocaly(IChessGame game)
	{
		AlphaBetaSearchFixedDepth abs=new AlphaBetaSearchFixedDepth();
		int value;
		
		//*** Wenn spieler für den gerechnet wird dran ist *******************************
		if(game.getPlayerToMakeTurn()==maximizingPlayer)
		{
			value=abs.getAlphaBetaTurn(0, game);
			this.resultSet.put(game.getStringRepresentation(), value);
		}
		//*** Wenn Gegner dran ist *******************************************************
		else
		{
			value=abs.getAlphaBetaTurn(1, game);
			//*** Bewertung -> Vorzeichen vertauschen ************************************
			value=value*(-1);
			this.resultSet.put(game.getStringRepresentation(), value);
		}
	}
	
	
	private ArrayList<IChessGame> advancedSplit(ArrayList<IChessGame> games, int nodeCount)
	{
		ArrayList<IChessGame> nextGames=new ArrayList<IChessGame>();
		ArrayList<IChessGame> tempGames=new ArrayList<IChessGame>();
		int freeNodes=nodeCount;
		int counter=0;
		
		//*** Zunächst alle Spiele übernehmen ********************************************
		nextGames.addAll(games);
		
		tempGames=games.get(counter).getNextTurns();
		//*** Solange genug Nodes da sind um ein Spiel durch seine nachfolger zu erstzen
		while(tempGames.size()<freeNodes)
		{
			if (counter < games.size()) //FIXME __marco - wieso?
			//*** Diese Knoten auswerten *************************************************
			nextGames.addAll(tempGames);
			
			//*** Anzahl freier Knoten aktualisieren *************************************
			freeNodes=freeNodes-tempGames.size();
			
			//*** Aber nicht den Vater ***************************************************
			nextGames.remove(games.get(counter));
			
			//*** Anzahl freier Knoten aktualisieren *************************************
			freeNodes=freeNodes+1;

			counter++;
			if (counter < games.size()) //FIXME __marco - wieso?
			{
				tempGames=games.get(counter).getNextTurns();
			}
		}
		
		return nextGames;
	}
	
	
	private ThreadPoolExecutor getThreadPool()
	{
//		if (_threadPool==null)
//		{
//			_threadPool =  new ThreadPoolExecutor(128,
//				 	  							  768,
//				 	  							  0L,
//				 	  							  TimeUnit.SECONDS,
//				 	  							  new LinkedBlockingQueue<Runnable>());
//		}
		
		return _threadPool;
	}
	
}
