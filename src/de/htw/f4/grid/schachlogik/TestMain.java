package de.htw.f4.grid.schachlogik;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.ChessClientImpl;
import de.htw.f4.grid.csm.client.interfaces.IClient;
import de.htw.f4.grid.csm.server.interfaces.IWnmConnector;
import de.htw.f4.grid.csm.util.DebugHelper;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.htw.f4.grid.schachInterfaces.IChessGameManager;




public class TestMain
{
	private final static Logger _log = Logger.getLogger(TestMain.class);
	/**
	 * @param args
	 */

	private static String testString="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxTw";
	public static void main(String[] args) 
	{
//		LocalGameConsole();
		GridGame(args);
	}
	
	private static void GridGame(String[] args)
	{

			String turn;
		
			
			// Verbindung aufbauen
			IClient<IWnmConnector> client = new ChessClientImpl();
			client.setParameter(args, client);
			client.start();
			System.out.println("test");
			IWnmConnector wnmConnector = client.getRemoteObject();
			
			
			IChessGameManager game = null;
			try
			{
				game = new GridGameManager(wnmConnector);
				game.init();
			
				BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

				while(true)
				{
					System.out.println(game.getCurrentGame().getReadableString());
					System.out.println("Computer zieht:");
					long start = System.currentTimeMillis();
					_log.debug("Zeitmessung: ComputerZug start");
					System.out.println(game.getTurn(15000));
					System.out.println(game.getCurrentGame().getReadableString());
					_log.debug("Zeitmessung:# ComputerZug stop (took \n#T000#" + DebugHelper.CURRENT_LIST_ID + "#T005#" + String.valueOf(System.currentTimeMillis()-start)  + "#ms)");
					do
					{
						System.out.println("Zug eingeben: ");
						
						turn=console.readLine();
					}
					while(!game.opponentTurn(turn));
				}
			}
			catch (Exception e) 
			{
				_log.error("Fehler im GridGame");
				if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug("Fehler im GridGame", e);
			}	
	}
	
	
	private static void LocalGameConsole()
	{
		IChessGameManager game=new LocalGameManager();
		boolean legalMove;
		game.init();
		game.init(testString, true, true, true, true);
		String input;
		BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
		try
		{
			while(true)
			{
				System.out.println(game.getCurrentGame().getReadableString());
				System.out.println(game.getCurrentGame().getQuality(Player.WHITE));
				legalMove=false;
				while(!legalMove)
				{
					System.out.println("Zug?: ");
					input=reader.readLine();
					legalMove=game.opponentTurn(input);
				}
				System.out.println(game.getCurrentGame().getReadableString());
				System.out.println(game.getTurn(10000));
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	
}
