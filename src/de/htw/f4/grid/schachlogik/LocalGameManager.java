package de.htw.f4.grid.schachlogik;

import org.apache.log4j.Logger;

import de.htw.f4.grid.jschess.WinboardCommunication;
import de.htw.f4.grid.jschess.WinboardTools;
import de.htw.f4.grid.schachInterfaces.IChessGame;
import de.htw.f4.grid.schachInterfaces.IChessGameManager;

public class LocalGameManager implements IChessGameManager
{
	private IChessGame board;
	private WinboardTools wbt = new WinboardTools();
	private boolean show_think = false;
	
	@SuppressWarnings("deprecation")
	public String getTurn(long time) throws Exception
	{
		IterativeAlphaBetaSearch iter=new IterativeAlphaBetaSearch(board);
		Thread thread=new Thread(iter);
		thread.start();
		Thread.sleep(time);
		IChessGame newBoard=iter.bestTurn;
	
		//TODO sch�ner machen als hart abbrechen
		thread.stop();
		/*AlphaBetaSearchFixedDepth abp=new AlphaBetaSearchFixedDepth();
		abp.getAlphaBetaTurn(7, board);
		IChessGame newBoard=abp.nextGame;*/
		
		board=newBoard;

		// Ausgabe des Engine Output bei WinBoard
		wbt.printThinking(show_think, 0, getPosScore(),(int) time, 0, newBoard.getTurnNotation());

		return newBoard.getTurnNotation();
	}

	public void init() 
	{
		board=ChessBoard.getStandardChessBoard();
		// Ausgabe des Engine Output bei WinBoard
	}
	
	public void init(String board, boolean k_Castling, boolean q_Castling, boolean K_Castling, boolean Q_Castling) 
	{
//		this.board=ChessBoard.getStandardChessBoard();		//FIXME: <<warum standardbrett?
		this.board=new ChessBoard();
		this.board.loadFromString(board);
		
		// Rochade noch moeglich oder nicht
		this.board.setRochade(k_Castling, q_Castling, K_Castling, Q_Castling);
	}

	public boolean opponentTurn(String turn) 
	{
		try
		{
			board=board.makeTurn(turn);
			// Ausgabe des Engine Output bei WinBoard
			wbt.printThinking(show_think, 0, getPosScore(), 0, 0, turn);
		}
		catch(Exception x)
		{
			//System.out.println(x.toString());
			return false;
		}
		return true;
	}

	public IChessGame getCurrentGame() 
	{
		return board;
	}
	
	private double getPosScore()
	{
		Quality q = new Quality((ChessBoard)board);
		return q.getPositionQuality(Player.WHITE, board.getTurnsMade());
	}

	public void setThinking(boolean show_think) {
		this.show_think = show_think;
	}

	public int getPlysMade() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setPlysMade(int count) {
		// TODO Auto-generated method stub
		
	}
	
//	private int getNodeCount()
//	{
//		
//	}

}
