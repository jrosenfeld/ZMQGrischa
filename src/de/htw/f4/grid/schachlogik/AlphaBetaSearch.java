package de.htw.f4.grid.schachlogik;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import de.htw.f4.grid.jschess.GameList;
import de.htw.f4.grid.schachInterfaces.IChessGame;

public abstract class AlphaBetaSearch 
{
	
	private final static Logger log = Logger.getLogger(AlphaBetaSearch.class);
	private final static int MIN_INT = -10000000;
	private final static int MAX_INT = + 10000000;
	private GameList clientGameList;
	int count =0;
	
	public IChessGame nextGame;
	protected Player maximizingPlayer;
	protected int maxSearchDepth;
	
	
	public int getAlphaBetaTurn(int maxSearchDepth, IChessGame game) 
	{
		clientGameList = new GameList();
		this.maximizingPlayer=game.getPlayerToMakeTurn();
		this.maxSearchDepth=maxSearchDepth;
		int q= maxValue(game, 0,MIN_INT, MAX_INT);
		return q;
	}

	/* TODO: 	Spielliste kann erstellt werden mit Typ: Spiel, Suchtiefe, Qualität
	 * 			Zuerst muss geprüft werden ob zu berechnendes Spiel darin enthalten ist, falls ja darf die Qualität abgefragt werden und die Berechnung kann erspart werden.
	 * 
	 * 			Es wird eine Spielliste auf den Client gefüllt, die zum Server geschickt wird sobald diese eine gewissen größe erreicht hat.
	 */
	
	private void saveGameToList(IChessGame game, int depth, double quality){
		// Nur speichern wenn Tiefe größer als 5 ist
		if (depth>5)
			{
				clientGameList.setGame(game, depth, quality);
				System.out.println("Tiefe "+depth +" " +game.getStringRepresentation() + " count="+count++);
			}
	}
	
	private int maxValue(IChessGame game, int depth, int alpha, int beta) 
	{    
		//*** Variablen Definition *********************************************
		ArrayList<IChessGame> successorList;
		int minimumValueOfSuccessor;
		IChessGame successor;
		int v=MIN_INT;
		
		if(game.hasBlackLost())
		{
			
			if(maximizingPlayer==Player.BLACK)
			{
				saveGameToList(game, depth, MIN_INT+depth);
				return MIN_INT+depth;
			}
			else 
			{
				saveGameToList(game, depth, MAX_INT-depth);
				return MAX_INT-depth;
			}
		}
		if(game.hasWhiteLost())
		{
			if(maximizingPlayer==Player.BLACK)
			{
				saveGameToList(game, depth, MAX_INT-depth);
				return MAX_INT-depth;
			}
			else
			{
				saveGameToList(game, depth, MIN_INT+depth);
				return MIN_INT+depth;
			}
		}
		//*** Testen ob Blatt **************************************************
		if (this.isLeave(game, depth)) 
		{
//			saveGameToList(game, depth, this.getPosQuality(game));
			return (int) this.getPosQuality(game);
		} 
		else 
		{
			//*** Nachfolger Spiele bestimmen **********************************
			successorList = game.getNextTurns();
			//*** Nachfolger abklappern ****************************************
			for (int i=0; i<successorList.size(); i++) 
			{
				successor = successorList.get(i);
				
				//*** Ungültige züge nciht weiter bearbeiten *****************************
				if(depth==0&&!successor.isLegalBoard()) 
				{
					minimumValueOfSuccessor=MIN_INT;
					//this.log.debug("Brett wurde als ungültig erkannt:\n" + successor.getReadableString());
				}
				else
				{
					minimumValueOfSuccessor = minValue(successor,depth+1, alpha, beta);
				}
				
				//*** Wenn besserer Nachfolger gefunden diesen nehmen **********
 				if (minimumValueOfSuccessor > v) 
				{
					v = minimumValueOfSuccessor;
					if(depth==0) this.nextGame=successor;
				}
				if (v >= beta) 
				{
//					saveGameToList(successor, depth, v);
					return v;
				}
				alpha=Math.max(v, alpha);
			}
//			saveGameToList(game, depth, v);
			return v;
		}
	}

	
	private int minValue(IChessGame game, int depth, int alpha, int beta) 
	{
		//*** Variablen Definition *********************************************
		ArrayList<IChessGame> successorList;
		int maximumValueOfSuccessor;
		IChessGame successor;
		int v=MAX_INT;

		if(game.hasBlackLost())
		{
			if(maximizingPlayer==Player.BLACK)
			{
				saveGameToList(game, depth, MIN_INT+depth);
				return MIN_INT+depth;
			}
			else 
			{
				saveGameToList(game, depth, MAX_INT-depth);
				return MAX_INT-depth;
			}
		}
		if(game.hasWhiteLost())
		{
			if(maximizingPlayer==Player.BLACK)
			{
				saveGameToList(game, depth, MAX_INT-depth);
				return MAX_INT-depth;
			}
			else
			{
				saveGameToList(game, depth, MIN_INT+depth);
				return MIN_INT+depth;
			}
		}
		//*** Testen ob Blatt **************************************************
		if (this.isLeave(game, depth)) 
		{
//			saveGameToList(game, depth, this.getPosQuality(game));
			return (int) this.getPosQuality(game);
		} 
		else 
		{
			successorList = game.getNextTurns();
			for (int i=0; i<successorList.size(); i++) 
			{
				successor = successorList.get(i);
				maximumValueOfSuccessor=maxValue(successor,  depth+1, alpha, beta);
				
				if (maximumValueOfSuccessor < v) 
				{
					v = maximumValueOfSuccessor;
				}
				if (v <= alpha) 
				{
//					saveGameToList(successor, depth, v);
					return v;
				}
				beta=Math.min(beta, v);
			}
//			saveGameToList(game, depth, v);
			return v;
		}
	}
	
	
	protected abstract boolean isLeave(IChessGame game, int depth);
	
	protected abstract int getQuality(IChessGame game);
	
	protected abstract double getPosQuality(IChessGame game);
}
