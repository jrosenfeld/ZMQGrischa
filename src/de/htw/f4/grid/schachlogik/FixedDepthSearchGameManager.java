package de.htw.f4.grid.schachlogik;

import de.htw.f4.grid.schachInterfaces.IChessGame;
import de.htw.f4.grid.schachInterfaces.IChessGameManager;

public class FixedDepthSearchGameManager implements IChessGameManager
{
	private IChessGame board;
	
	
	public String getTurn(long time) throws Exception
	{
		AlphaBetaSearchFixedDepth abp=new AlphaBetaSearchFixedDepth();
		abp.getAlphaBetaTurn(6, board);
		IChessGame newBoard=abp.nextGame;
		board=newBoard;
		return newBoard.getTurnNotation();
	}

	public void init() 
	{
		board=ChessBoard.getStandardChessBoard();
	}

	public boolean opponentTurn(String turn) 
	{
		try
		{
			board=board.makeTurn(turn);
		}
		catch(Exception x)
		{
			//System.out.println(x.toString());
			return false;
		}
		return true;
	}

	public IChessGame getCurrentGame() 
	{
		return board;
	}

	public void setThinking(boolean show_think) {
		
	}

	public void init(String board, boolean k_Castling, boolean q_Castling, boolean K_Castling, boolean Q_Castling) 
	{
		this.board=new ChessBoard();
		this.board.loadFromString(board);
		
		// Rochade noch moeglich oder nicht
		this.board.setRochade(k_Castling, q_Castling, K_Castling, Q_Castling);
	}

	public int getPlysMade() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setPlysMade(int count) {
		// TODO Auto-generated method stub
		
	}
	

	
//	private int getNodeCount()
//	{
//		
//	}
}
