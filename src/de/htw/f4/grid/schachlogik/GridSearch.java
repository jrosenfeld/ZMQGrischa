package de.htw.f4.grid.schachlogik;


import java.util.TreeMap;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.jobs.JobChessImpl;
import de.htw.f4.grid.csm.jobs.JobResult;
import de.htw.f4.grid.csm.jobs.interfaces.IJob;
import de.htw.f4.grid.csm.jobs.interfaces.IJobResult;
import de.htw.f4.grid.csm.server.interfaces.IVirtualWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IWnmConnector;
import de.htw.f4.grid.schachInterfaces.IChessGame;
import de.root1.simon.exceptions.SimonRemoteException;


public class GridSearch implements Runnable
{
	private final static Logger _log = Logger.getLogger(GridSearch.class);
	private IChessGame game;
	private IWnmConnector wnmConnector;
	private IVirtualWorkerNode node;
	private long time;
	private TreeMap<String, Integer> results;
	private Player maximizingPlayer;
	
	public GridSearch(IWnmConnector wnmConnector, 
					  IVirtualWorkerNode node, 
					  IChessGame game, 
					  TreeMap<String, Integer> results, 
					  Player maximizingPlayer,
					  long time) 
	{
		this.game=game;
		this.wnmConnector = wnmConnector;
		this.maximizingPlayer=maximizingPlayer;
		this.node=node;
		this.results=results;
		this.time=time;
	} 
	
	
	@SuppressWarnings("unchecked")
	public void run() 
	{
//		try
//		{
			//wenn der WorkerNode defekt ist so übergebe -1 als WorkerNode id
			//dies muss in JobChessImpl entsprechend ausgewertet werden
			int workerNodeId;
			
			//vorzeitiges beenden, falls ein ungültiger WorkerNode angegeben wurde
			if (this.node==null)
			{
				_log.error("Der Job eines WorkerNodes konnte kein Ergebnis lieferen (NULL-Referenz auf WorkerNode).");
				return ;
			}
			
//			_log.info("run(): Hole WorkerNode-Id...");
			workerNodeId = this.node.getWorkerNodeId();
			
//			_log.info("run(): Erstell neuen JobChessImpl");
			IJob job=new JobChessImpl(game, time, workerNodeId, this.maximizingPlayer);
//			IJobResult<Integer> result = new JobResult<Integer>();
			
			
			
			//starte job. beende vorzeitig, falls es Kommunikationsprobleme gab
			try
			{
//				_log.info("run(): Job in Auftrag geben --> this.wnmConnector.startJob(job,this.node);");
				this.wnmConnector.startJob(job,this.node);
			}
			catch (SimonRemoteException e1)
			{
//				_log.error("run(): Ausführung von 'this.wnmConnector.startJob(job,this.node)'  fehlgeschlagen", e1);
				return;
			}
			
//			//warte feste Zeit auf Beendingung des Jobs
//			try
//			{
//				Thread.sleep(time);
//			}
//			catch (InterruptedException e)
//			{
//				_log.error("run(): Thread vorzeitig abgebrochen. Ursprüngliche Wartezeit war " + time + "ms"); 
////				e.printStackTrace();
//			}
			
			
			
			
//			IVirtualWorkerNode node;
//			try
//			{
//				result = this.wnmConnector.getResult(this.node);
//			}
//			catch (SimonRemoteException e)
//			{
//				_log.error("run(): Ausführung von 'this.wnmConnector.getResult(this.node)'  fehlgeschlagen", e);
//				return;
//			}
//			//TODO: stopJob am Ende von allen Ergebnissen gesammelt aufrufen
////			this.wnmConnector.stopJob(this.node);
//				
//			
//			//kein Ergebnis vorhanden?
//			if (result==null)
//			{
//				_log.error("Der Job eines WorkerNodes konnte kein Ergebnis lieferen.");
//				return ;
//			}
			if (game==null)
			{
				_log.error("game==null");
				return;
			}
			
			
			if (game.getStringRepresentation()==null)
			{
				_log.error("game.getStringRepresentation()==null");
				return;
			}
//	
//			if (result.getResult()==null)
//			{
//				_log.error("result.getResult()==null");
//				return;
//			}
//			if(maximizingPlayer==game.getPlayerToMakeTurn())
//			{
//				results.put(game.getStringRepresentation(), result.getResult());
//			}
//			else
//			{
//				results.put(game.getStringRepresentation(), result.getResult()*(-1));
//			}
//			
////		}
////		catch(SimonRemoteException e)
////		{
//////			_log.error("run(): Fehler");
////			_log.debug("run(): Fehler (SimonRemoteException)", e);
////			
////		}
	}
	
}
