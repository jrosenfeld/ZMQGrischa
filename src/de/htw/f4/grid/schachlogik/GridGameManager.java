package de.htw.f4.grid.schachlogik;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.server.interfaces.IVirtualWorkerNode;
import de.htw.f4.grid.csm.server.interfaces.IWnmConnector;
import de.htw.f4.grid.csm.util.DebugHelper;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.htw.f4.grid.jschess.GameList;
import de.htw.f4.grid.jschess.WinboardTools;
import de.htw.f4.grid.schachInterfaces.IChessGame;
import de.htw.f4.grid.schachInterfaces.IChessGameManager;
import de.root1.simon.exceptions.SimonRemoteException;

public class GridGameManager implements IChessGameManager
{
	private final static Logger _log = Logger.getLogger(GridGameManager.class);
	private ArrayList<IVirtualWorkerNode> workerNodeList;
	private IChessGame board;
	private IWnmConnector wnmConnector;
	private WinboardTools wbt = new WinboardTools();
	private boolean show_think = false;
	private ThreadPoolExecutor _threadPoolForSendingJobs; //Threads, die zum Versenden der Jobs zuständig sind
	private DistributedSearch abps;
	private GameList gameList;

	public GridGameManager(IWnmConnector wnmConnector) throws Exception
	{
		//*** Zu Server verbinden **********************************************
		// Verbindung aufbauen
		
//		IClient<IWnmConnector> chessClient = new ChessClientImpl();
//		chessClient.start();
//		IWnmConnector wnmConnector = chessClient.getRemoteObject();
//		IServer server = new ServerImpl();
		
		//port setzen
//		server.setListeningPort(22222);  //TODO: port dynamisch setzen
//		server.start();
		
		this.wnmConnector= wnmConnector;
		abps=new DistributedSearch(gameList);
		
		
//		
//		//*** Worker Nodes holen  **********************************************
//		int nodeCount = 0;
//		
//		
//		nodeCount = wnmConnector.getFreeWorkerNodeCount();
//		workerNodeList = wnmConnector.getWorkerNodeList(nodeCount);
		
		long start = System.currentTimeMillis();
		releaseAllWorkerNodesFromList();
		_log.debug("Zeitmessung: GridGameManager().releaseAllWorkerNodesFromList() took  " + String.valueOf(System.currentTimeMillis()-start) + "ms");
		
		start = System.currentTimeMillis();
		getAllAvailableWorkerNodes();	//581ms bei 8 WNs (HTW<->Marco)
		_log.debug("Zeitmessung: GridGameManager().getAllAvailableWorkerNodes() took  " + String.valueOf(System.currentTimeMillis()-start) + "ms");
		
		
	}
	
	
	/**
	 * Hole maximale Anzahl von WorkerNodes vom Server
	 */
	private void getAllAvailableWorkerNodes()
	{
		//*** Worker Nodes holen  **********************************************
		int nodeCount = 0;
		
		
		//gebe zuvor reservierte WorkerNodes frei
		try
		{
			if (this.workerNodeList!= null)
			{
				_log.info("getAllAvailableWorkerNodes(): Gebe WorkerNodes frei...");
				wnmConnector.releaseWorkerNodeList(this.workerNodeList, board.getTurnsMade());	//100ms bei 8Rechner (htw<->marco)
				_log.info("getAllAvailableWorkerNodes(): ...WorkerNodes freigegeben");
			}
		}
		catch (SimonRemoteException e1)
		{
			String message = "Freigeben der WorkerNodes am Server fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e1.getMessage())==true) _log.debug(message, e1);
		}
		
		
		
		//hole neue WorkerNodes
		try
		{
//			long start2 = System.currentTimeMillis();
			nodeCount = this.wnmConnector.getFreeWorkerNodeCount();				//92ms bei 8 WNs (HTW<->Marco)
//			_log.debug("Zeitmessung: getAllAvailableWorkerNodes1: " + String.valueOf(System.currentTimeMillis()-start2) + "ms für this.wnmConnector.getFreeWorkerNodeCount()");
			
			
//			long start = System.currentTimeMillis();
			this.workerNodeList = wnmConnector.getWorkerNodeList(nodeCount);	//294ms bei 8 WNs (HTW<->Marco)
//			_log.debug("Zeitmessung: getAllAvailableWorkerNodes2: " + String.valueOf(System.currentTimeMillis()-start) + "ms für wnmConnector.getWorkerNodeList(nodeCount)");
		}
		catch (SimonRemoteException e)
		{
			String message = "Abrufe der WorkerNodes vom Server fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) _log.debug(message, e);
		}

	}
	
	
	/**
	 * Löscht alle WorkerNode-Listen vom aktuellen Client.
	 * Wird i.d.R. beim ersten Programmstart vnon GridGameManager aufgerufen.
	 */
	private void releaseAllWorkerNodesFromList()
	{
		try
		{
			wnmConnector.releaseAllWorkerNodesFromList();
		}
		catch (SimonRemoteException e)
		{
			_log.error("Freigeben aller WorkerNodes aus WorkerNodeListe fehlgeschlagen");
		}
	}
	
	
	
	public IChessGame getCurrentGame() 
	{
		return this.board;
	}

	
	
	public String getTurn(long time) throws Exception 
	{
		_threadPoolForSendingJobs =  new ThreadPoolExecutor(128,
			    1024,
			    0L,
			    TimeUnit.SECONDS,
			    new LinkedBlockingQueue<Runnable>());

		DebugHelper.CURRENT_LIST_ID = board.getTurnsMade(); //aktuelle Spielzuganzahl merken, damit später überall für debug-zwecke darauf zurückgegriffen werden kann
		
		
//		DistributedSearch abps=new DistributedSearch();
		
		long start = System.currentTimeMillis();
		getAllAvailableWorkerNodes(); //aktuelle WorkerNodes nachladen
		_log.debug("Zeitmessung:# getTurn().getAllAvailableWorkerNodes() took  \n#T000#" + DebugHelper.CURRENT_LIST_ID + "#T002#" + String.valueOf(System.currentTimeMillis()-start) + "#ms");
		

		
		abps.getAlphaBetaTurn(this.wnmConnector,workerNodeList, board, time, _threadPoolForSendingJobs);
		_log.debug("getTurn(): 'abps.getAlphaBetaTurn' erfolgreich"); 
		
		board=abps.nextGame;
		_log.debug("getTurn(): 'board=abps.nextGame' erfolgreich");
		
		
		// alle noch laufenden Job-Berechnungen hart abschießen (als Thread)
		try
		{
			_log.debug("getTurn(): 'this.wnmConnector.stopJobs(workerNodeList)' gestartet");
			this.wnmConnector.stopJobs(workerNodeList);
			_log.debug("getTurn(): 'this.wnmConnector.stopJobs(workerNodeList)' erfolgreich ausgeführt");
		}
		catch (Exception e)
		{
			_log.error("getTurn(): Fehler aufgetreten in 'this.wnmConnector.stopJobs(workerNodeList)'");
		}
		
		_log.debug("getTurn(): '_threadPoolForSendingJobs.shutdownNow()' gestartet");
		_threadPoolForSendingJobs.shutdownNow();
		_log.debug("getTurn(): '_threadPoolForSendingJobs.shutdownNow()' erfolgreich ausgeführt");
		
//		_log.debug("Zeitmessung: Beginne nicht mehr benötigte WorkerNodes freizugeben...");
//		this.wnmConnector.releaseWorkerNodeList(this.workerNodeList);
//		_log.debug("Zeitmessung: ...nicht mehr benötigte WorkerNodes sind jetzt alle freigegeben.");
		
		
		// Rundenzaehler um 1 erhöhen
		//board.addRound();
		
		
		
		// schieße alle verspäteten JobThreads ab
		// TODO Daniel - Die Jobs rechnen hier nicht weiter. D.h. wenn der Gegner zieht
		//               sind bereits alle Jobs abgebrochen bzw zu Ende. Wenn du möchtest,
		//               dass die Jobs auch noch rechnen während der Gegner zieht, dann müssen
		//               die folgenden Zeilen wieder raus. (Marco)

		
		//pool leeren
//		_threadPoolForSendingJobs.shutdown(); //TODO __Marco - pool wirklich immer stoppen oder einfach so lassen?
		return board.getTurnNotation();
	}

	public void init() 
	{
		this.board=ChessBoard.getStandardChessBoard();
		// Ausgabe des Engine Output bei WinBoard
	}

	public void init(String board, boolean k_Castling, boolean q_Castling, boolean K_Castling, boolean Q_Castling) 
	{
		this.board=new ChessBoard();
		this.board.loadFromString(board);
		
		// Rochade noch moeglich oder nicht
		this.board.setRochade(k_Castling, q_Castling, K_Castling, Q_Castling);
	}
	
	public boolean opponentTurn(String turn) throws Exception 
	{
		try
		{
			board=board.makeTurn(turn);
		}
		catch(Exception x)
		{
			//System.out.println(x.toString());
			return false;
		}
		return true;
	}

	
	public void setThinking(boolean show_think) {
		this.show_think = show_think;
	}
	
}
