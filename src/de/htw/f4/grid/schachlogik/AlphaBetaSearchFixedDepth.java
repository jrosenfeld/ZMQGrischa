package de.htw.f4.grid.schachlogik;

import de.htw.f4.grid.schachInterfaces.IChessGame;

public class AlphaBetaSearchFixedDepth extends AlphaBetaSearch
{
	
	protected int getQuality(IChessGame game) 
	{
		return game.getQuality(maximizingPlayer);
	}

	protected double getPosQuality(IChessGame game) 
	{
		Quality q = new Quality((ChessBoard)game);
		return q.getPositionQuality(maximizingPlayer, game.getTurnsMade());
	}

	protected boolean isLeave(IChessGame game, int depth) 
	{
		if(depth<maxSearchDepth) return false;
		else return true;
	}

}
