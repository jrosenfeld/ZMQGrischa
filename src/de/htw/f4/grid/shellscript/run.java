package de.htw.f4.grid.shellscript;

import java.io.IOException;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.jobs.JobChessImpl;

/**
 * Kann beliebige Shell-Kommandos ausf�hren
 * 
 * @author Marco Strutz - S0516095
 * @see "http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html?page=1"
 * @see http://www.devx.com/tips/Tip/14667
 */
public class run
{
	private final static Logger	_log	= Logger.getLogger(run.class);

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		Executer executer = new Executer();
		
		try
		{
			executer.execute();
		}
		catch (IOException e)
		{
			_log.error("Fehler aufgetreten");
		}
	}

}
