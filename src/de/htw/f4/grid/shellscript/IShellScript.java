package de.htw.f4.grid.shellscript;

import java.util.ArrayList;
import java.util.Map;

import de.root1.simon.exceptions.SimonRemoteException;


public interface IShellScript
{
	
	/**
	 * Liefert das Skript als String
	 * @return
	 */
	public String getScript() throws SimonRemoteException;
	
	
}
