package de.htw.f4.grid.shellscript.scripte;

import de.htw.f4.grid.shellscript.IShellScript;

public class ScriptHelloWorldImpl implements IShellScript
{
	private String _script;

	public ScriptHelloWorldImpl()
	{
	}

	public String getScript()
	{
		create();
		return _script;
	}

	private void create()
	{
		String script;
		String ablageortDesSkriptes = "shellscript";
		String skriptName = "helloWorld.sh";
		
		script = "/bin/sh " + ablageortDesSkriptes + "/" + skriptName;
		
		_script = script;
	}
}
