package de.htw.f4.grid.shellscript;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.util.LoggingMessages;
import de.htw.f4.grid.shellscript.scripte.ScriptHelloWorldImpl;

/**
 * Führt ein beliebiges Script aus
 * 
 */
public class Executer
{
	private final static Logger _log = Logger.getLogger(Executer.class);
	
	public Executer()
	{
		//
	}
	
	public void execute() throws IOException
	{

		//hole HelloWorld Skript
		IShellScript shellScript = (IShellScript) new ScriptHelloWorldImpl();
		String script = shellScript.getScript();
	
		
		// http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html?page=4
		try
		{
			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(script);
			
			InputStream stdin = proc.getInputStream();
			InputStreamReader isr = new InputStreamReader(stdin);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			System.out.println("<OUTPUT>");
			while ((line = br.readLine()) != null)
				System.out.println(line);
			System.out.println("</OUTPUT>");
			int exitVal = proc.waitFor();
			System.out.println("Process exitValue: " + exitVal);
		} 
		catch (Throwable t)
		{
			String message = "Ausführung eins Shell-Skriptes fehlgeschlagen";
			_log.error(message);
			if (LoggingMessages.isUnknownErrorMessage(t.getMessage())==true) _log.debug(message, t);
		}
		
		
//		ProcessBuilder pb = new ProcessBuilder("myshellScript.sh", "myArg1",
//				"myArg2");
//		Map<String, String> env = pb.environment();
//		env.put("VAR1", "myValue");
//		env.remove("OTHERVAR");
//		env.put("VAR2", env.get("VAR1") + "suffix");
//		pb.directory("myDir");
//		Process p = pb.start();


	}

}
