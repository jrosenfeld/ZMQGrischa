package de.htw.f4.grid.grischa;

import de.htw.f4.grid.grischa.exceptions.GriSchaConfigurationException;
import de.htw.f4.grid.grischa.exceptions.GriSchaException;
import de.htw.f4.grid.grischa.exceptions.GriSchaMissingArgumentException;
import de.htw.f4.grid.grischa.exceptions.GriSchaUnknownOption;
import de.htw.f4.grid.grischa.exceptions.GriSchaWrongModeException;
import de.htw.f4.grid.grischa.runningmodes.GriSchaRunningMode;
import de.htw.f4.grid.grischa.runningmodes.GriSchaServerDaemonMode;
import de.htw.f4.grid.grischa.runningmodes.GriSchaServerMode;
import de.htw.f4.grid.grischa.runningmodes.GriSchaWnMode;
import de.htw.f4.grid.grischa.runningmodes.GrischaXboardMode;

/**
 * @author Laurence
 *
 */
public class GriScha {
	public final static String VERSION = "v1.1.3alpha-2";
	private GriSchaConfiguration mConfiguration = null;
	
	/**
	 * GriSchas Server mode
	 */
	public final static String MOD_SERVER = "server";
	
	/**
	 * GriSchas xboard interface mode
	 */
	public final static String MOD_XBOARD = "xboard";
	
	/**
	 * GriSchas worker-node mode
	 */
	public final static String MOD_WN = "wn";

	
	/**
	 * Init GriShca
	 * @param args Command line arguments
	 */
	public GriScha(String[] args) {
		this.mConfiguration = new GriSchaConfiguration();
		
		try {
			this.analyzeArguments(args);
		} catch (GriSchaException e) {
			System.out.println(e.getMessage());
			this.usage();
		}
	}
	
	/**
	 * Analyse the command line arguments
	 * @param args Command line arguments
	 */
	public void analyzeArguments(String[] args) throws GriSchaException {
		if (args.length < 1) {
			throw new GriSchaConfigurationException("you hava to set the " +
					"running mode");
		}
		
		/* 
		 * Check if the first argument is the GriScha mode (Server, Client, 
		 * xboard, Worker-Node) 
		 */
		if (this.checkGriSchaMode(args[0]) == false) {
			throw new GriSchaWrongModeException(args[0]);
		}
		
		this.mConfiguration.setMode(args[0]);
		
		for (int i=1; i<args.length; i++) {
			String argument = args[i];
			if (argument.startsWith("-")) {
				for (int a=1; a<argument.length(); a++) {
					if (argument.charAt(a) == '-') {
						argument = this.getOption(argument);
					}
					
					switch (argument.charAt(a)) {
					case 's':
						if (this.hasParameter(args, i, argument, a) == false) {
							throw new GriSchaMissingArgumentException("Server address");
						}
						this.mConfiguration.setServerAddress(args[i+1]);
						i++; break;
					case 'p':
						if (this.hasParameter(args, i, argument, a) == false) {
							throw new  GriSchaMissingArgumentException("Port");
						}
						this.mConfiguration.setPort(args[i+1]);
						i++; break;
					case 'h':
						this.usage();
						break;
					case 'd':
						this.mConfiguration.isDaemon(true);
						break;
					case 'v':
						System.out.println("Version: " + GriScha.VERSION);
						System.exit(0);
					default:
						throw new GriSchaUnknownOption(argument);
					}
				} 
			} else {
				throw new GriSchaUnknownOption(args[i]);
			}
		}
	}
	
	/**
	 * Checks if a parameter is given to an option
	 * @param args The arguments assigned by the command line 
	 * @param argsIndex The current index of the arguments
	 * @param argument The current argument
	 * @param argumetIndex The index of the current argument character
	 * @return True if parameter is assigned else false
	 */
	public Boolean hasParameter(String[] args,int argsIndex, String argument, 
								int argumetIndex) {

		if (args.length > argsIndex+1 && argument.length() < argumetIndex+2) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Returns the option to an option alias
	 * @param optionAlias Option alias
	 * @return Option
	 * @throws GriSchaUnknownOption If the alias is unknown 
	 */
	public String getOption(String optionAlias) throws GriSchaUnknownOption {
		if (optionAlias.compareTo("--server") == 0) {
			return "-s";
		} 
		
		if (optionAlias.compareTo("--port") == 0) {
			return "-p";
		} 
		
		if (optionAlias.compareTo("--help") == 0) {
			return "-h";
		}
		
		if (optionAlias.compareTo("--daemon") == 0) {
			return "-d";
		}
		
		throw new GriSchaUnknownOption(optionAlias);
	}
	
	/**
	 * Checks if the assigned working mode is valid
	 * @param mode Assigned working mode
	 * @return true if valid else false
	 */
	public Boolean checkGriSchaMode(String mode) {
		return 	(GriScha.MOD_SERVER.compareTo(mode) == 0 ||
				GriScha.MOD_XBOARD.compareTo(mode) == 0 ||
				GriScha.MOD_WN.compareTo(mode) == 0);
	}
	
	/**
	 * Print out usage informations 
	 */
	public void usage() {
		String usage = "\nUsage: grischa mode [options]\n";
		usage += "Modes: \n";
		usage += "  server \t Run GriScha as server\n";
		usage += "  xboard \t Run GriScha as xboard interface\n";
		usage += "  wn     \t Run GriScha as worker-node\n";
		usage += "\nOptions\n";
		usage += "  -s, --server address \t\t Set the server address IP / " +
				                              "DNS-Name\n";
		
		usage += "  -p, --port 1024-49151 \t Set the port\n";
		usage += "  -v \t \t \t Version";
		usage += "  -d, --daemon \t \t \t Run the server as daemon \n";
		
		System.out.println(usage);
	}
	
	/**
	 * Run GriScha
	 */
	public int start() {
		if (this.mConfiguration.getMode() == null) return 1;
		
		GriSchaRunningMode mode = null;
		
		if (this.mConfiguration.getMode().compareTo(GriScha.MOD_SERVER) == 0) {
			mode = new GriSchaServerMode(this.mConfiguration);
		}
		
		if (this.mConfiguration.getMode().compareTo(GriScha.MOD_XBOARD) == 0) {
			mode = new GrischaXboardMode(this.mConfiguration);
		}
		
		if (this.mConfiguration.getMode().compareTo(GriScha.MOD_WN) == 0) {
			mode = new GriSchaWnMode(this.mConfiguration);
		}
		
		if (this.mConfiguration.getMode().compareTo(GriScha.MOD_SERVER) == 0 &&
				this.mConfiguration.isDaemon() == true) {
			mode = new GriSchaServerDaemonMode(this.mConfiguration);
		}
		
		try {
			mode.start();
		} catch (GriSchaConfigurationException e) {
			System.err.println(e.getMessage());
			return 1;
		}
		
		return 0;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		GriScha grischa = new GriScha(args);
		System.exit(grischa.start());
	}
}
