package de.htw.f4.grid.grischa.exceptions;

/**
 * @author laurence
 *
 */
public class GriSchaUnknownOption extends GriSchaException {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public GriSchaUnknownOption() {
		this("");
	}

	/**
	 * Init exception with unknown option
	 * @param option The unknown option
	 */
	public GriSchaUnknownOption(String option) {
		super("Unknown option: " + option);
		// TODO Auto-generated constructor stub
	}

}
