package de.htw.f4.grid.grischa.exceptions;


/**
 * @author Laurence
 *
 */
public class GriSchaConfigurationException extends GriSchaException {
	private static final long serialVersionUID = 1L;

	/**
	 * Init exception without message
	 */
	public GriSchaConfigurationException() {
		this("");
	}

	/**
	 * Init exception with message
	 * @param Discription for configuration exception
	 */
	public GriSchaConfigurationException(String message) {
		super(message);
	}
}
