package de.htw.f4.grid.grischa.exceptions;

/**
 * @author laurence
 *
 */
public class GriSchaException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * Init the exception
	 */
	public GriSchaException() {
		super();
	}

	/**
	 * Init the exception with message
	 * @param message Error Message
	 */
	public GriSchaException(String message) {
		super(message);
	}
}
