package de.htw.f4.grid.grischa.exceptions;

public class GriSchaInvalidPortException extends GriSchaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GriSchaInvalidPortException() {
		// TODO Auto-generated constructor stub
	}

	public GriSchaInvalidPortException(String port) {
		super("Invalid port:" + port);
	}
	
	public GriSchaInvalidPortException(Integer port) {
		this(port.toString());
	}

}
