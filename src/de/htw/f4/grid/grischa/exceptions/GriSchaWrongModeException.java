package de.htw.f4.grid.grischa.exceptions;

/**
 * @author laurence
 *
 */
public class GriSchaWrongModeException extends GriSchaException {
	private static final long serialVersionUID = 1938937511748653329L;

	/**
	 * 
	 */
	public GriSchaWrongModeException() {
		this("");
	}

	/**
	 * Init exception with the wrong mode 
	 * @param wrongMode assigned by command line 
	 */
	public GriSchaWrongModeException(String wrongMode) {
		super(wrongMode + " not a valid mode");
	}
}
