package de.htw.f4.grid.grischa.exceptions;

/**
 * @author laurence
 *
 */
public class GriSchaMissingArgumentException extends GriSchaException {
	private static final long serialVersionUID = 1L;

	/**
	 * Init exception without message
	 */
	public GriSchaMissingArgumentException() {
		this("");
	}

	/**
	 * Init exception with the name of the missing argument
	 * @param argumentName
	 */
	public GriSchaMissingArgumentException(String argumentName) {
		super("You hava to assign the: " + argumentName);
	}
}
