package de.htw.f4.grid.grischa.runningmodes;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;

import org.apache.log4j.Logger;

import de.htw.f4.grid.grischa.GriSchaConfiguration;
import de.htw.f4.grid.grischa.exceptions.GriSchaConfigurationException;

/**
 *
 * @author laurence
 * @version 0.1
 */
public class GriSchaServerDaemonMode extends GriSchaServerMode  
	implements Runnable {
	
	private Logger mLog = null;
	private Thread mDaemon = null;
	
	/**
	 * File name for the pid file
	 */
	public final static String PID_FILE = "grischa.pid";

	
	/**
	 * Init GriScha with configuration
	 * @param config configuration
	 */
	public GriSchaServerDaemonMode(GriSchaConfiguration config) {
		super(config);
		
		this.mLog =  Logger.getLogger(this.getClass());
		
		// Register shutdownhook for receiving kill signal 
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				shutdownhook();
			}
		});
	}
	
	/**
	 * Shutdown server components correctly if kill signal received
	 */
	public void shutdownhook() {
		this.stopServer();
		try {
			this.mDaemon.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Start server as thread
	 * @see de.htw.f4.grid.grischa.runningmodes.GriSchaRunningMode#start()
	 */
	@Override
	public void start() {
		this.mDaemon = new Thread(this);
		this.mDaemon.start();
	}
	
	/**
	 * Writs the pid of the Server into a file
	 */
	public void writePid() {
		File pidFile = new File(GriSchaServerDaemonMode.PID_FILE);
		
		try {
			BufferedWriter bw = new BufferedWriter( new FileWriter(pidFile));                    
			
			bw.write(
				ManagementFactory.getRuntimeMXBean().getName().split("@")[0]
			);
			
			bw.close();
		} catch (IOException e) {
			this.mLog.error(e.getMessage());
		} catch (NumberFormatException e) {
			this.mLog.error(e.getMessage());
		}
	}
	
	/**
	 * Init server in the  thread 
	 */
	public void run()  {
		try {
			this.startServer();
			this.writePid();
		} catch (GriSchaConfigurationException e) {
			this.mLog.error(e.getMessage());
			this.stopServer();
		}
		
		// Keep server thread alive
		while (this.isRunning) { 	
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				this.mLog.error(e.getMessage());
				this.stopServer();
			}
		}
	}
}
