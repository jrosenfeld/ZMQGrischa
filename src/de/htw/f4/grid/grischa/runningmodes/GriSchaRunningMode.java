package de.htw.f4.grid.grischa.runningmodes;

import de.htw.f4.grid.grischa.exceptions.GriSchaConfigurationException;

/**
 * Interface for running modes
 * @author Laurence
 */
public interface GriSchaRunningMode {
	/**
	 * Starts GriScha
	 * @throws GriSchaConfigurationException if something in
	 * the configuration is wrong
	 */
	public void start() throws GriSchaConfigurationException;
	
	/**
	 * Checks if all parameters are set to run the selected mode
	 * @return True if all parameters are set else false
	 */
	public Boolean checkCofiguration();
}
