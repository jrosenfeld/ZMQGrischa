package de.htw.f4.grid.grischa.runningmodes;

import de.htw.f4.grid.grischa.GriSchaConfiguration;
import de.htw.f4.grid.grischa.exceptions.GriSchaConfigurationException;
import de.htw.f4.grid.jschess.WinboardCommunication;

/**
 *
 * @author laurence
 * @version 0.1
 */
public class GrischaXboardMode implements GriSchaRunningMode {
	private GriSchaConfiguration mConf = null;
	
	
	public GrischaXboardMode(GriSchaConfiguration conf) {
		this.mConf = conf;
	}

	/**
	 * @see de.htw.f4.grid.grischa.runningmodes.GriSchaRunningMode#start()
	 */
	public void start() throws GriSchaConfigurationException {
		if (this.checkCofiguration() == false ) {
			throw new GriSchaConfigurationException("You have to set the port" +
					" and server address/ip");
		}
		
		WinboardCommunication xboard = new WinboardCommunication(this.mConf);
		xboard.run();
	}

	public Boolean checkCofiguration() {
		if (this.mConf == null) return false;
		if (this.mConf.getPort() == null) return false;
		if (this.mConf.getServerAddress() == null) return false;
		return true;
	}
}
