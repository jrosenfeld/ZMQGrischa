package de.htw.f4.grid.grischa.runningmodes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import de.htw.f4.grid.csm.server.ServerImpl;
import de.htw.f4.grid.csm.server.interfaces.IServer;
import de.htw.f4.grid.grischa.GriSchaConfiguration;
import de.htw.f4.grid.grischa.exceptions.GriSchaConfigurationException;

/**
 * Run GriScha as Server
 * @author laurence
 * @version 0.1
 */
public class GriSchaServerMode implements GriSchaRunningMode {
	private GriSchaConfiguration mConfiguration =  null;
	private IServer mServer = null;
	protected Boolean isRunning = null;
	
	
	/**
	 * Init with no Configuration
	 */
	public GriSchaServerMode() {
		this(null);
	}
	
	/**
	 * Init server mode with configuration
	 * @param config The configuration
	 */
	public GriSchaServerMode(GriSchaConfiguration config) {
		this.mConfiguration = config;

		this.mServer = new ServerImpl();
		this.isRunning = true;
	}

	/**
	 * @see de.htw.f4.grid.grischa.runningmodes.GriSchaRunningMode#start()
	 */
	public void start() throws GriSchaConfigurationException {		
		// Run the server an the command line
		this.startServer();
		this.startInteractiveCommandLine();
	
		System.exit(0);
	}
	
	/**
	 * Start GriScha server
	 * @throws GriSchaConfigurationException If something is missing in the config
	 */
	public void startServer() throws GriSchaConfigurationException {
		// Check if port is set in configuration
		if (this.checkCofiguration() == false ) {
			throw new GriSchaConfigurationException("No port set");
		}
		
		this.mServer.setListeningPort(this.mConfiguration.getPort());
		this.mServer.start();
	}
	
	/**
	 * Stops GriScha server
	 */
	public void stopServer() {
		this.mServer.stop();
		this.mServer = null;
		this.isRunning = false;
	}
	
	/**
	 * Run command line to control  server
	 */
	public void startInteractiveCommandLine() {
		BufferedReader console = new BufferedReader(
				new InputStreamReader(System.in)
		);
		
		String command = "";

		while (isRunning == true) {
			showServerCommands();
			System.out.println("Kommando an Server: ");

			try {
				command = console.readLine();
			} catch (IOException e) {
				System.err.println("Can't read from command line");
				this.stopServer();
			}

			// server stoppen
			if (command.equals("stop")) {
				System.out.println("Server wird gestoppt...");
				this.stopServer();
			}

			// alle WorkerNodes abmelden/beenden
			if (command.equals("removeAllWorkerNodes")) {
				deregisterAllWorkerNodes();
			}

			// alle WorkerNodes abmelden/beenden
			if (command.equals("stat")) {
				int countWn = this.mServer.getCountWorkerNodes();
				System.out
						.println("WorkerNode-Anzahl (ohne Netzwerk-Abfrage): "
								+ countWn);
			}

			// reparieren anstoßen
			if (command.equals("reapir")) {
				this.mServer.repairWorkerNodes();
			}

			// alle registrierten/validen WorkerNodes ausgeben
			if (command.equals("showRegisteredWorkerNodes")) {
				System.out.println(this.mServer.parseWorkerNodeTree(false));
			}
		}
	}
	
	/**
	 * Alle WorkerNodes abmelden/beenden
	 */
	public void deregisterAllWorkerNodes() {
		boolean shutDownWorkerNodes = true;
		this.mServer.releaseAllWorkerNodes(shutDownWorkerNodes);
	}
	
	/**
	 * Show all commands
	 */
	public void showServerCommands() {
		System.out.println("\n\n--------------------------------------------------------");
		System.out.println("showRegisteredWorkerNodes	- alle registrierten WorkerNodes anzeigen");
		System.out.println("removeAllWorkerNodes		- alle WorkerNodes abmelden");
		System.out.println("stat				- alle registrierten WorkerNodes anzeigen (ohne Netzwerkabfrage)");
		System.out.println("reapir				- Reparieren der WN's anstoßen");
		System.out.println("--------------------------------------------------------\n");
	}

	/**
	 * @see de.htw.f4.grid.grischa.runningmodes.GriSchaRunningMode#
	 */
	public Boolean checkCofiguration() {
		if (this.mConfiguration == null) return false;
		if (this.mConfiguration.getPort() == null) return false;
		
		return true;
	}
}
