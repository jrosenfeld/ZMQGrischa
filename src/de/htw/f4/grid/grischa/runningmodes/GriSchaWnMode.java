package de.htw.f4.grid.grischa.runningmodes;

import org.apache.log4j.Logger;

import de.htw.f4.grid.csm.client.WorkerNodeClientImpl;
import de.htw.f4.grid.csm.client.interfaces.IClient;
import de.htw.f4.grid.csm.server.interfaces.IWnm;
import de.htw.f4.grid.csm.util.LoggingMessages;
import de.htw.f4.grid.grischa.GriSchaConfiguration;
import de.htw.f4.grid.grischa.exceptions.GriSchaConfigurationException;

/**
 * This mode runs a worker-node
 * @author laurence
 * @version 0.1
 */
public class GriSchaWnMode implements GriSchaRunningMode {
	private GriSchaConfiguration mConf;
	private Boolean mIsRunning = null;
	private Logger mLog = null;
	
	
	public GriSchaWnMode(GriSchaConfiguration conf) {
		this.mConf = conf;
		this.mIsRunning = true;
		this.mLog = Logger.getLogger(this.getClass());
	}

	/**
	 * @see de.htw.f4.grid.grischa.runningmodes.GriSchaRunningMode#start()
	 */
	public void start() throws GriSchaConfigurationException {
		if (this.checkCofiguration() == false) {
			throw new GriSchaConfigurationException("You have to set the port" +
					" and server address/ip");
		}
		
		IClient<IWnm> client = new WorkerNodeClientImpl();

		client.setHost(this.mConf.getServerAddress());
		client.setPort(this.mConf.getPort());
		client.start();
		
		while(this.mIsRunning)
		{
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e)
			{
				String message = "Fehler im Thread";
				this.mLog.error(message);
				if (LoggingMessages.isUnknownErrorMessage(e.getMessage())==true) { 
					this.mLog.debug(message, e);
				}
			}
			
			// Stop worker-node if time is expired 
			if (client.getRemainingLifetime() <= 0) {
				this.stop();
				client.stop();
			}				
		}
		
		System.exit(0);
	}
	
	/**
	 * Stop the woker-node mode
	 */
	public void stop() {
		this.mIsRunning = false;
	}

	/**
	 * @see de.htw.f4.grid.grischa.runningmodes.GriSchaRunningMode#checkCofiguration()
	 */
	public Boolean checkCofiguration() {
		if (this.mConf == null) return false;
		if (this.mConf.getPort() == null) return false;
		
		return true;
	}
}
