package de.htw.f4.grid.grischa;

import de.htw.f4.grid.grischa.exceptions.GriSchaInvalidPortException;

/**
 * @author laurence
 *
 */
public class GriSchaConfiguration {
	private String mServerAdress = null;
	private Integer mPort = null;
	private String mMode = null;
	private Boolean mIsDaemon = null;
	
	/**
	 * Init empty configuration 
	 */
	public GriSchaConfiguration() {
		this.mIsDaemon = false;
	}

	/**
	 * Set the server address
	 * @param address IP or DNS-Name of the server
	 */
	public void setServerAddress(String address) {
		this.mServerAdress = address;
	}
	
	/**
	 * Set the default port
	 * @param port The port > 1024
	 * @throws If port is not valid
	 */
	public void setPort(Integer port) throws GriSchaInvalidPortException {
		if (port < 1024) throw new GriSchaInvalidPortException(port);
		this.mPort = port;
	}
	
	/**
	 * @see de.htw.f4.grid.grischa.GriSchaConfiguration#setPort(Integer)
	 */
	public void setPort(String port) throws GriSchaInvalidPortException {
		try {
			this.mPort = Integer.parseInt(port);
		} catch (NumberFormatException e) {
			throw new GriSchaInvalidPortException(port);
		}
	}
	
	/**
	 * Set the mode on which mode GriScha will run
	 * @param mode The running mode {@link GriScha}
	 */
	public void setMode(String mode) {
		this.mMode = mode;
	}
	
	/**
	 * Set the server mode to daemon or no daemon
	 * @param isDaemon True if yes else false
	 */
	public void isDaemon(Boolean isDaemon) {
		this.mIsDaemon = isDaemon;
	}
	
	/**
	 * Get the server address
	 * @return IP or DNS-Name of the server
	 */
	public String getServerAddress() {
		return this.mServerAdress;
	}
	
	/**
	 * Get the default port of the server
	 * @return The port > 1024
	 */
	public Integer getPort() {
		return this.mPort;
	}
	
	/**
	 * Get the mode in which GriScha will run
	 * @return The mode {@link GriScha}
	 */
	public String getMode() {
		return this.mMode;
	}
	
	/**
	 * Run the server as daemon
	 * @return True if yes else false
	 */
	public Boolean isDaemon() {
		return this.mIsDaemon;
	}
}
