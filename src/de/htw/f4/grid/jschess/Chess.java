package de.htw.f4.grid.jschess;

import de.htw.f4.grid.schachlogik.Player;

public class Chess {

	
	private static boolean possible_moves(int figure, int s, int t, int[] board_content)
	{
		int[] q= {1,10,11,9,-1,-10,-11,-9}; //queen moves -used in moves array
		int[][] moves={{0},{0},{21,19,12,8,-21,-19,-12,-8},{11,9,-11,-9},{1,10,-1,-10},q,q}; 	  //in order _,p,n,b,n,q,k
		int[] move = moves[figure];
		int diff = t - s;
		boolean freeway = false;
		boolean can_move = false;
		
		
		if (figure == 2 || figure == 6) 			// Springer, K�nig k�nnen nur einen Schritt
		{	
			for (int i=0; i<move.length ;i++)
			{
				if (diff == move[i])
				{					
					return true;
				}
			}
		}
		else													// Andere Figuren k�nnen mehrere Schritte
		{			
			for (int j=1; j<9; j++)
			{
				for (int i=0; i<move.length ;i++)
				{
					if (diff==move[i]*j)						// Figur darf zum Ziel springen
					{						
						can_move = true;		
					}
				}
			}
			if (can_move)					
			{
				int j = 0, l = 1, k = 0;
				for (int i=0;i<move.length ;i++)				// Richtung ermitteln (int)
				{	
					j=8;
					k=diff;
					while ((k!=0)&&(j!=0))
					{		
						k=k-move[i];						
						j--;						
					}
					if (k==0)
						l=move[i];		
				}
				int space = diff / l - 1;
				int i=0;
				freeway = true;
				while (freeway && i<space)						// Figuren im weg finden
				{
					if (board_content[s+21+(l*(i+1))] == 0)
					{
						freeway = true;
					}
					else
					{
						freeway = false;
					}
					i++;
				}
			}
			return freeway;
			}					
		return false;
	}
	
	/* TODO: 
	 * 	x	Liste mit Figurpositionen abchecken damit keine figuren �bersprungen werden <<anders gel�st!
	*	K�nig nicht ins Matt
	*	K�nig schach Matt dann Meldung	
	*/
	
	public static boolean rochade(int s, int t, int[] board_content, Player player)
	{
		int x = board_content[s+21];		
		if (x>8) { x=x-8;} // Figuren 9 - E auf 1-6 setzen
		// Rochade TODO: merken (in datei schreiben?)
		System.out.println("rochade "+x + " s "+s+" t "+t +" s+24 "+board_content[s+24]+" s+22 "+board_content[s+22]);
		if (x==6 && board_content[t+21]==0 && 
			   ((t-s==2 &&  board_content[s+24]==4 && board_content[s+22]==0) 
			|| 	(t-s==-2 && board_content[s+17]==4 && board_content[s+20]==0 && board_content[s+18]==0)))
		{
			return true;
		}
		return false;
	}
			
	public static boolean valid_move(int s, int t, int[] board_content, Player player)
	{
			// Nur die Figur dessen Spieler gerade dran ist soll bewegt werden d�rfen
		if ((player == Player.WHITE && board_content[s+21]<7) || (player != Player.WHITE && board_content[s+21]>7))
		{
			int x = board_content[s+21];		
			if (x>8) { x=x-8;} 	// Figuren 9 - E auf 1-6 setzen
			if (x>1&&x<7) 			// non-pawn
			{
				// Target free
				if (board_content[t+21] == 0)
				{
					if (possible_moves(x, s, t, board_content))
					{
						return true;
					}
				}
				// Target not free - Schlagen
				else
				{	
					if (board_content[t+21] != 16)
					{
						if (board_content[s+21] < 7)				// Wei� am Zug
						{
							if (board_content[t+21] > 7)			// Schwarz als Ziel
							{
								if (possible_moves(x, s, t, board_content))
								{
									return true;
								}
							}
							else								// gleiche Farbe als Ziel
							{
								return false;
							}
						}
						else									// Schwarz am Zug
						{
							if (board_content[t+21] < 7)			// Wei� als Ziel
							{
								if (possible_moves(x, s, t, board_content))
								{
									return true;
								}
							}
							else								// gleiche Farbe als Ziel
							{
								return false;
							}
						}
					}
				}
			}
			else
				return pawn_move(s, t, board_content, player);
		}
		else
			return false;		
		return false;
	}
	
	public static boolean pawn_move(int s, int t, int[] board_content, Player player)
	{		
			// erst Schlagen pr�fen Wei�
			if (board_content[s+21] < 7 && player == Player.WHITE)				// Wei�er Bauer am Zug
			{
				if (board_content[t+21] > 7 && board_content[t+21] != 16)			// Schwarz als Ziel
				{
					if (t-s==-9 || t-s==-11)				// nur schr�g schlagen
					{
						return true;
					}
					else								// Ziel nicht erreichbar
					{
						return false;
					}
				}
				// if erster zug dann 2 schritte m�glich
				else if (s+21 < 89 && s+21 > 80 && player == Player.WHITE)		// Wei� nur nach unten gehen [+10]
				{
					if (t-s == -10 && board_content[t+21]==0)	// erster zug entfernung 10 oder 20 UND feld frei
					{
						return true;
					}
					else if (t-s == -20 && board_content[t+21]==0 && board_content[t+31]==0)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else if (player == Player.WHITE)						// nicht erster Zug
				{
					if (t-s == -10 && board_content[t+21]==0)	// Ziel ist in richtung unten und frei
					{
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			// erst Schlagen pr�fen Schwarz
			else if (board_content[s+21] > 7 && player != Player.WHITE)			// Schwarzer Bauer am Zug
			{					
				if (board_content[t+21] < 7 && board_content[t+21] != 0)			// Wei� als Ziel
				{
					if (t-s==9 || t-s==11)			// nur schr�g schlagen
					{
						return true;
					}
					else								// Ziel nicht erreichbar
					{
						return false;
					}
				}
				// if erster zug dann 2 schritte m�glich
				else if (s+21 < 39 && s+21 > 30 && player != Player.WHITE)		// Schwarz nur nach oben [-10]
				{
					if (t-s == 10 && board_content[t+21]==0)	// erster zug entfernung 10 oder 20 UND feld frei
					{
						return true;
					}
					else if (t-s == 20 && board_content[t+21]==0 && board_content[t+11]==0)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else if (player != Player.WHITE)						// nicht erster Zug
				{
					if (t-s == 10 && board_content[t+21]==0)	// Ziel ist in richtung unten und frei
					{
						return true;
					}
					else
					{
						return false;
					}
				}		
			}						
			//TODO if ganz hinten dann eintauschen m�glich
		return false;
	}
}
