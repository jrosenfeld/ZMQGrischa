package de.htw.f4.grid.jschess;

import de.htw.f4.grid.schachInterfaces.IChessGame;

public class ComputedGame {

	private IChessGame game;
	private int depth;
	private double quality;
	
	// TODO: Game + Tiefe dürfen nicht zweimal mit unterschiedlicher Qualität enthalten sein
	
	/*
	 * Funktion public ComputedGame(IChessGame game, int depth, Double quality)
	 * Konstruktor für ComputedGame
	 */	
	public ComputedGame(IChessGame game, int depth, Double quality){
		this.game = game;
		this.depth = depth;
		this.quality = quality;
	}
	
	/*
	 * Funktion public boolean equals(IChessGame game, int depth)
	 * Zum überprüfen ob das Spiel mit entsprechender Tiefe bereits enthalten ist, um später die entsprechende Qualität ausgegeb zu können.
	 * @return boolean
	 */	
	public boolean equals(IChessGame game, int depth){
		
		if(this.game.equals(game) && this.depth==depth)
			return true;
		else
			return false;
	}
	
	/*
	 * Funktion public boolean equals(IChessGame game, int depth, Double quality)
	 * Zum überprüfen ob das Spiel mit entsprechender Tiefe und Qualität bereits enthalten ist..
	 * @return boolean
	 */	
	public boolean equals(IChessGame game, int depth, Double quality){
		
		if(this.game.equals(game) && this.depth==depth && this.quality ==quality)
			return true;
		else
			return false;
	}
	
	/*
	 * Funktion public double getQuality()
	 * Gibt die Qualität des gespeicherten Spiels zurück
	 * @return double 
	 */
	public double getQuality() {
		return quality;
	}
}
