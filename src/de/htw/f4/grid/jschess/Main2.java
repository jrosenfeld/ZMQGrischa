package de.htw.f4.grid.jschess;

import de.htw.f4.grid.schachInterfaces.IChessGame;
import de.htw.f4.grid.schachlogik.*;

public class Main2
{
	private static IChessGame board=ChessBoard.getStandardChessBoard();;
	private static WinboardTools wbt = new WinboardTools();
	private static boolean show_think = true;
	private static final long time = 5000;

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{		
		board.loadFromString("txldklst" +
				"bbbbxbbb" +
				"xxsxxxxx" +
				"xxxxxxxx" +
				"xxxDxxxx" +
				"xxxxxxxx" +
				"BBBxBBBB" +
				"TSLxKLSTB");
		IChessGame newboard;
		
		// Startbrett + Bewertung ausgeben
		System.out.println("old="+getScore(board));
		System.out.println("new="+getPosScore(board));
		System.out.println(board.getReadableString());
		
		for (int i=0;i<board.getNextTurns().size();i++)
		{
			// Folgebretter + bewertung ausgeben
			newboard = board.getNextTurns().get(i);
			System.out.println("old="+getScore(newboard));
			System.out.println("new="+getPosScore(newboard));
			System.out.println(newboard.getReadableString());
//			wbt.printThinking(show_think, 0, getPosScore(newboard, board.getPlayerToMakeTurn()),(int) time, 0, "\n"+newboard.getReadableString());
		}
	}
	
	private static int getScore(IChessGame chessGame)
	{
		return chessGame.getQuality(chessGame.getPlayerToMakeTurn());
	}
	
	private static double getPosScore(IChessGame chessGame)
	{
		Quality q = new Quality((ChessBoard)chessGame);
		return q.getPositionQuality(Player.WHITE, chessGame.getTurnsMade());
	}
	
}
