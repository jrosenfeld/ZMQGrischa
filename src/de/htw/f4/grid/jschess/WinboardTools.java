package de.htw.f4.grid.jschess;

import org.apache.log4j.Logger;

import de.htw.f4.grid.schachlogik.LocalGameManager;

public class WinboardTools {

	private final static Logger _log = Logger.getLogger(WinboardTools.class);

	public void printThinking(boolean show_think, int ply, double score, int time, int nodes, String pv)
	{
		/*
		 * The thinking output should be in the following format:

		ply score time nodes pv

		Where:
		ply	Integer giving current search depth.
		score	Integer giving current evaluation in centipawns.
		time	Current search time in centiseconds (ex: 1028 = 10.28 seconds).
		nodes	Nodes searched.
		pv	Freeform text giving current "best" line. You can continue the pv onto another line if you start each continuation line with at least four space characters.
		
		Bsp Fair-Max:
			Tiefe Score Nodes Time Text(bester Zug)
			8	+6.06	520202	0:01.62	d5c7
			7	+6.19	206269	0:00.67	d5c7
			6	+6.06	107637	0:00.35	d5c7
			5	+5.98	15699	0:00.04	d5c7
			4	+6.54	4632	0:00.01	d5c7
			3	+6.69	795		0:00.00	d5c7
			2	+6.69	106		0:00.00	d5c7
			1	+2.03	3		0:00.00	d5c7
			0	+2.12	2		0:00.00	e4e5

		 */
		if (show_think)
			{
				System.out.println(ply + " " + (int)score/1000 + " " + time/10 + " " + nodes + " " + pv);
				_log.info("Boardscore aus Sicht von Weiss="+score/1000);
			}
	}
}
