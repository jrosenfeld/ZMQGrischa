package de.htw.f4.grid.jschess;

import java.util.ArrayList;

import de.htw.f4.grid.schachInterfaces.IChessGame;

/*
 * In der Klasse GameList werden die schon berechneten Bretter mit der jeweils genutzten Tiefe gespeichert und können wieder ausgelesen werden.
 * Es werden die Brettstellungen mit möglicher Rochade und welche Farbe als nächstes dran ist gespeichert.
 * Bei einer positiven Anfrage wird der Wert der Bewertungsfunktion zurückgegeben um so dem Node das erneute auswerten des Suchbaums unter dieser Brettstellung zu ersparen. 
 * @author Thor
 *
 */
public class GameList {

	private ArrayList<ComputedGame> computedGames;
	private int found=0;
	
	
	public GameList(){
		this.computedGames = new ArrayList<ComputedGame>();
	}
	
	public GameList(ArrayList<ComputedGame> gameList){
		this.computedGames = new ArrayList<ComputedGame>();
		this.computedGames.addAll(gameList);
	}
	
	/*
	 * Funktion public boolean isInList(IChessGame game, Integer depth)
	 * Prüft ob das gesuchte Spiel mit entsprechender Tiefe in der Liste ArrayList<ComputedGame> computedGames enthalten ist.
	 * @return boolean
	 */
	public boolean isInList(IChessGame game, int depth) {
		for (int i=0;i<computedGames.size();i++)
		{
			// Wenn das game vorhanden ist und entsprechende depth hat true zurückgeben
			if (computedGames.get(i).equals(game, depth))
			{
				found = i;
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Funktion public double getGameValue(IChessGame game, int depth)
	 * Holt die Qualität des Spiels. Erst verwenden NACH aufruf von isInList!
	 * @return double
	 */
	public double getGameValue(IChessGame game, int depth){
		return computedGames.get(found).getQuality();
	}
	
	/*
	 * Funktion public void setGame(IChessGame game, int depth, double Quality)
	 * Erzeugt ein Objekt ComputedGame und füllt es mit Werten und fügt diese Objekt der Liste computedGames hinzu.
	 * @return void
	 */
	public void setGame(IChessGame game, int depth, double Quality){
		computedGames.add(new ComputedGame(game, depth, Quality));
	}
}
