package de.htw.f4.grid.schachInterfaces;


public interface IChessGameManager
{
	/** 
	 * initialisiert den Manager, eine neue Partie wird gestartet, eine evtl.
	 * exisitierende alte verworfen 
	 */
	public void init();
	
	/** 
	 * initialisiert den Manager, eine neue Partie wird gestartet, eine evtl.
	 * exisitierende alte verworfen, die Ausgangstellung wird durch board beschrieben
	 */
	public void init(String board, boolean k_Castling, boolean q_Castling, boolean K_Castling, boolean Q_Castling);
	
	/**
	 *  Ein gegnerischen Zug in der Notation d2d4 etc. wird ausgef�hrt.
	 * gibt true zur�ck wenn Grischa der Meinung ist der Zug ist legal sonst false 
	 */
	public boolean opponentTurn(String turn) throws Exception;
	
	
	/**
	 * Gibt einen durch Suche gefundenen nachfolge Zug zur aktuellen Stellung
	 * in Form der Notation d2d4 zur�ck
	 */
	public String getTurn(long time) throws Exception;
	
	/*
	 * Gibt das aktuelle Spielbrett zur�ck
	 */
	public IChessGame getCurrentGame();
	
	/**
	 * Gibt an ob eine Ausgabe auf dem Engine Output geschehen soll oder nicht
	 */
	public void setThinking(boolean show_think);

	
}
