package de.htw.f4.grid.schachInterfaces;

public interface IChessResult {
	public int getValue();
	
	public String getGame();
	
	public String getInformation();
}
