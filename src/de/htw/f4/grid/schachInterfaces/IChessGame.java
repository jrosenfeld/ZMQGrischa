package de.htw.f4.grid.schachInterfaces;

import java.util.ArrayList;

import de.htw.f4.grid.schachlogik.GameState;
import de.htw.f4.grid.schachlogik.Player;



public interface IChessGame extends Comparable<IChessGame>
{
	/*
	 * Standard String Repr�sentation:
	 * ein string mit 65 Zeichen, ein Zeichen entspricht einem Feld beginnend beim
	 * Feld A1, A2 ... bis H8.
	 * Wei�e figuren sind Klein-Buchstaben, schwarze Gro�-Buchstaben.
	 * Bauer=b/B, Turm=t/T, Springer=s/S, Laeufer=l/L, Dame=d/D, Koenig=k/K.
	 * Leeresfeld=x; 
	 * der 65 buchstabe gibt an welcher spieler am zug ist S=schwarz und w=wei�; 
	 */
	
	/**
	 * Initialisiert ein Spiel aus einem String der Standard String Repr�sentation.
	 * @param board Der String nachdem das Brett initialisiert wird. 
	 */
	public void loadFromString(String board);
	
	/**
	 * Liefert einen String mit der Standardrepr�sentation des Bretts zur�ck
	 * @return Das Brett in Standard-Repr�santation
	 */
	public String getStringRepresentation();

	/**
	 * Liefert den Spieler der am Zug ist	
	 * @return Die Frabe des Spielers der am Zug ist
	 */
	public Player getPlayerToMakeTurn();
	
	/**
	 * Errechnet m�glich folge Z�ge
	 * @return Eine Liste mit m�glich en folge Z�gen
	 */
	public ArrayList<IChessGame> getNextTurns();
	
	/*
	 * Berechnung der Qualit�t: 
	 */
	
	/**
	 * Errechnet die Qualitaet der Stellung aus Sicht eines Spielers 
	 * @param player der Spieler aus dessen Sicht die Stellung beurteilt wird
	 * @return Die Qualitaet eines Brettes fuer den angegebenen Spieler
	 */
	public int getQuality(Player player);
	
	
//	public BoardRepresentationType getType();
	
	/*
	 * Gibt eine Lesbare 2d darstellung des aktuellen Brettes zur�ck
	 */
	public String getReadableString();
	
	/*
	 * Gibt den Hash-Wert zum Brett zur�ck
	 */
	public String getHash();
	
	/*
	 * Gibt den Sch�tzwert der Stellungsbeurteilung im Vergleich zur vorherigen
	 * Stellung zur�ck positive Werte sehen wei� im VOrteil negative schwarz
	 */
	public int getHeuristicValue();
	
	/*
	 * Gibt einen String in der Form d2d4 zur�ck, der den Zug beschreibt, der
	 * zu dieser Stellung gef�hrt hat
	 */
	public String getTurnNotation();
	
	/*
	 * Gibt ein neues Brett zur�ck auf dem der angegebene Zug gemacht wurde 
	 */
	public IChessGame makeTurn(String turn) throws Exception; 
	
	/*
	 * Gibt an wieviele Züge schon gmeacht wurden
	 */
	public int getTurnsMade();
	
	
	/*
	 * Gibt true zurück, wenn der Spieler am Zug in diesem Zug den König des 
	 * Gegners werfen kann
	 */
	public boolean isLegalBoard();
	
	/*
	 * Gibt den Zustand des Spieles zurück legal=alles ok, illegal -> es stimmt was nciht
	 * z.B. König fehlt oder König wurde grade ins Matt gezogen sowie draw bei keine legalen
	 * Züge mehr möglich und matt
	 */
	public GameState getGameState();
	
	/*
	 * Gibt an welche Rochade noch moeglich ist 
	 */
	public void setRochade(boolean k_Castling, boolean q_Castling, boolean K_Castling, boolean Q_Castling);
	
	public boolean hasWhiteLost();
	public boolean hasBlackLost();

}