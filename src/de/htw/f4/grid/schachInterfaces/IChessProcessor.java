package de.htw.f4.grid.schachInterfaces;


public interface IChessProcessor
{
	public int getAlphaBetaTurnInTime(long time, IChessGame game);
	
	/**
	 * Bestimmt den besten Zug 
	 * Die beste Zufolge ist in game.getNextGame usw. hinterlegt 
	 * @param maxSearchDepth Die Anzahl der Halbz�ge die Vorrausgerechnet wird
	 * @param game Die Ausgangsspiel-Situation
	 * @return Der Erwartungswert der berechneten Zugfolge
	 */
	public int getAlphaBetaTurn(int maxSearchDepth, IChessGame game);
}
