package de.htw.f4.grid.schachInterfaces;

public interface IRemoteChessProcessor {
	public IChessResult getAlphaBetaTurn(String game);
	
}
