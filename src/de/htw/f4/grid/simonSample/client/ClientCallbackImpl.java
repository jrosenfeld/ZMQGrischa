package de.htw.f4.grid.simonSample.client;

import de.htw.f4.grid.simonSample.common.ClientCallbackInterface;
import de.root1.simon.exceptions.SimonRemoteException;

public class ClientCallbackImpl implements ClientCallbackInterface
{

	private static final long serialVersionUID = 1L;


	public synchronized void callback(byte[] byteArray)	throws SimonRemoteException
	{

		System.out.println("ByteArraySize: " + byteArray.length);
		// return byteArray;
	}


	public void sendArray(byte[] byteArray) throws SimonRemoteException
	{
		System.out.println("Client: Server send byteArray of the size " + byteArray.length);
		
	}
	
	

}
