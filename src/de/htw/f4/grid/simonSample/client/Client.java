package de.htw.f4.grid.simonSample.client;

import java.io.IOException;
import java.util.Date;


import de.htw.f4.grid.simonSample.common.IRegistration;
import de.htw.f4.grid.simonSample.common.ServerInterface;
import de.root1.simon.Simon;
import de.root1.simon.exceptions.LookupFailedException;

public class Client {
	public static void main(String[] args) throws IOException,
			LookupFailedException, InterruptedException {

		String ipServer = "rybec2.dyndns.org";
		int portServer = 1099;
		
		// create a callback object
		ClientCallbackImpl clientCallbackImpl = new ClientCallbackImpl();

		int arraySize = 100000;
		if (args.length == 1)
		{
			ipServer = args[0];
		}
		if (args.length == 2)
		{
			ipServer = args[0];
			arraySize = Integer.parseInt(args[1]);
		}

		// 'lookup' the server object
		
		
		// server.login(clientCallbackImpl,arraySize);

		Date begin;
		Date actTime;

		Boolean loop = true;
		// use the serverobject as it would exist on your local machine
		begin = new Date();
		long endTime = (10 * 1000) + begin.getTime();
		ServerInterface server=null;
		IRegistration registration;
		registration = (IRegistration) Simon.lookup(ipServer, portServer, "registration");
		
		registration.anmelden(clientCallbackImpl);
		
		for (int i = 0; i < 10; i++)
		{
			Thread.sleep(1000);
		}
//		server = (ServerInterface) Simon.lookup(ipServer,portServer, "server");
//		server.login(clientCallbackImpl, arraySize);
//		Simon.release(server);
		registration.abmelden(clientCallbackImpl);
		
		// and finally 'release' the serverobject to release to connection to
		// the server
		Simon.release(server);
		Simon.release(registration);
		System.out.println("Client beendet");
		
		Thread.sleep(1000);
		System.exit(0);
	}
}
