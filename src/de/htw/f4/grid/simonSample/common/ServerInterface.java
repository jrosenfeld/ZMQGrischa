package de.htw.f4.grid.simonSample.common;

import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;

public interface ServerInterface extends SimonRemote {

	public void login(ClientCallbackInterface clientCallback, int arraySize) throws SimonRemoteException;
	
	public void ping(byte[] b) throws SimonRemoteException;

	public void setRegistration(IRegistration registration) throws SimonRemoteException;
}
