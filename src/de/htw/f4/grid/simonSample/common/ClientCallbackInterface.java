package de.htw.f4.grid.simonSample.common;

import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;

public interface ClientCallbackInterface extends SimonRemote {

	 public void callback(byte[] byteArray) throws SimonRemoteException;

	 public void sendArray(byte[] byteArray) throws SimonRemoteException;
}
