package de.htw.f4.grid.simonSample.common;

import java.util.Vector;

import de.root1.simon.SimonRemote;
import de.root1.simon.exceptions.SimonRemoteException;

public interface IRegistration extends SimonRemote
{

	/**
	 * Ruft der Client auf, wenn er sich abmeldet.
	 * Intern wird ein Counter f�r jeden Aufruf um eins verringert.
	 */
	public void abmelden(ClientCallbackInterface object) throws SimonRemoteException;
	
	/**
	 * Ruft der Client auf, wenn er sich anmeldet. Intern wird f�r jeden
	 * Aufruf ein Counter hochgez�hlt.
	 */
	public void anmelden(ClientCallbackInterface object) throws SimonRemoteException;

	
	public int getNumberOfRegisteredClients() throws SimonRemoteException;
	
	public Vector<ClientCallbackInterface> getRegisteredClients() throws SimonRemoteException;
}
