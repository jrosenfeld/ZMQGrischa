package de.htw.f4.grid.simonSample.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Vector;

import de.htw.f4.grid.simonSample.common.ClientCallbackInterface;
import de.htw.f4.grid.simonSample.common.IRegistration;
import de.root1.simon.Registry;
import de.root1.simon.Simon;
import de.root1.simon.SimonRegistryStatistics;
import de.root1.simon.SimonRemoteStatistics;
import de.root1.simon.exceptions.NameBindingException;

public class Server {

	public static void main(String[] args) throws UnknownHostException,
			IOException, NameBindingException {

		// create the serverobject
		ServerInterfaceImpl serverImpl = new ServerInterfaceImpl();
		IRegistration registration = new RegistrationImpl();

		serverImpl.setRegistration(registration);
		
		// create the server's registry ...
		
		Registry registry = Simon.createRegistry(1099);

		// ... where we can bind the serverobject to
		registry.bind("server", serverImpl);
		registry.bind("registration", registration);
		
		String host = "";
		
		try {
			host = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		
		System.out.println("Server up and running!");
		System.out.println("Server-IP: " + host);
		System.out.println("-----------------------------");
		
		
		
		// some mechanism to shutdown the server should be placed here
		// this should include the following command:
		// registry.unbind("server");
	}
}
