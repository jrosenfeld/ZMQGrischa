package de.htw.f4.grid.simonSample.server;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.htw.f4.grid.simonSample.common.ClientCallbackInterface;
import de.htw.f4.grid.simonSample.common.IRegistration;
import de.root1.simon.Simon;
import de.root1.simon.exceptions.SimonRemoteException;

public class Benchmark extends Thread
{
	private ClientCallbackInterface _client;
	private int _countOfLoop = 10;
	private int _arraySize;
	private IRegistration _registration;


	public Benchmark(ClientCallbackInterface object, int arraySize,
			IRegistration registration)
	{
		_client = object;
		_arraySize = arraySize;
		_registration = registration;
	}


	public Benchmark(Runnable target)
	{
		super(target);
	}


	public Benchmark(String name)
	{
		super(name);
	}


	public Benchmark(ThreadGroup group, Runnable target)
	{
		super(group, target);
	}


	public Benchmark(ThreadGroup group, String name)
	{
		super(group, name);
	}


	public Benchmark(Runnable target, String name)
	{
		super(target, name);
	}


	public Benchmark(ThreadGroup group, Runnable target, String name)
	{
		super(group, target, name);
	}


	public Benchmark(ThreadGroup group, Runnable target, String name,
			long stackSize)
	{
		super(group, target, name, stackSize);
	}


	public void run()
	{
		
		while (isInterrupted() == false || _countOfLoop<=0)
		{
			Date startTime;
			Date stopTime;
			SimpleDateFormat simpleDate = new SimpleDateFormat("yyyyMMddhhmmss");

			int duration = 0;
			
			byte[] byteArray = new byte[_arraySize];

			startTime = new Date();
			try
			{
				_client.callback(byteArray);
			}
			catch (SimonRemoteException e1)
			{
				// TODO Fehlermeldung ueber log ausgeben!
				e1.printStackTrace();
			}
			stopTime = new Date();
			duration = (int) stopTime.getTime() - (int) startTime.getTime();
			try
			{
				System.out.println("server;" + "serverTime=;"
						+ simpleDate.format(stopTime) + ";" + "workerNodeIp=;"
						+ Simon.getRemoteInetSocketAddress(_client) + ";"
						+ "duration=;" + duration + ";"
						+ "angemeldeteClients=;"
						+ _registration.getNumberOfRegisteredClients() + ";"
						+ "arraySize=;" + _arraySize);
			}
			catch (IllegalArgumentException e1)
			{
				// TODO Fehlermeldung ueber log ausgeben!
				e1.printStackTrace();
			}
			catch (SimonRemoteException e1)
			{
				// TODO Fehlermeldung ueber log ausgeben!
				e1.printStackTrace();
			}
			
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e)
			{
				// TODO Fehlermeldung ueber log ausgeben!
				e.printStackTrace();
			}
				
			_countOfLoop--;
		}
		
		 _client = null;
		 _registration = null;
	}

}
