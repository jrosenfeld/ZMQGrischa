package de.htw.f4.grid.simonSample.server;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.htw.f4.grid.simonSample.common.ClientCallbackInterface;
import de.htw.f4.grid.simonSample.common.IRegistration;
import de.htw.f4.grid.simonSample.common.ServerInterface;
import de.root1.simon.Simon;
import de.root1.simon.exceptions.SimonRemoteException;

public class ServerInterfaceImpl implements ServerInterface {

	private static final long serialVersionUID = 1L;
	private static final int _countOfLoop = 300;
	private IRegistration _registration;

	public synchronized void login(ClientCallbackInterface clientCallback, int arraySize)
			throws SimonRemoteException {

		Date startTime;
		Date stopTime;
		SimpleDateFormat simpleDate = new SimpleDateFormat("yyyyMMddhhmmss");
		
		int duration = 0;

		for (int i = 0; i < _countOfLoop; i++)
		{
			byte[] byteArray = new byte[arraySize];

			startTime = new Date();
			clientCallback.callback(byteArray);
			stopTime = new Date();
			duration = (int) stopTime.getTime() - (int) startTime.getTime();
			System.out.println("server;"+
					           "serverTime=;" + simpleDate.format(stopTime) + ";" +
					           "workerNodeIp=;" + Simon.getRemoteInetSocketAddress(clientCallback)+";"+
					           "duration=;" + duration + ";" +  
					           "angemeldeteClients=;" + _registration.getNumberOfRegisteredClients() + ";" +
					           "arraySize=;" + arraySize);
			try
			{
				Thread.currentThread().sleep(1000);
			}
			catch (InterruptedException e)
			{
				// TODO Fehlermeldung ueber log ausgeben!
				e.printStackTrace();
			}
			
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e)
			{
				// TODO Fehlermeldung ueber log ausgeben!
				e.printStackTrace();
			}
		}
	}

	public synchronized void ping(byte[] b) throws SimonRemoteException {
		
		
	}

	public void setRegistration(IRegistration registration)
			throws SimonRemoteException
	{
		_registration = registration;
	}
	
	

}
