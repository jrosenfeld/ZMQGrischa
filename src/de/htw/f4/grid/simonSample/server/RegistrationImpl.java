package de.htw.f4.grid.simonSample.server;

import java.util.Vector;

import de.htw.f4.grid.simonSample.common.ClientCallbackInterface;
import de.htw.f4.grid.simonSample.common.IRegistration;
import de.root1.simon.exceptions.SimonRemoteException;

public class RegistrationImpl implements IRegistration
{
	private int _anzahlAngemeldeteClient;
	private Vector<ClientCallbackInterface> _listOfClientCallbacks;
	
	
	public RegistrationImpl()
	{
		_anzahlAngemeldeteClient = 0;
		_listOfClientCallbacks = new Vector<ClientCallbackInterface>();
	}


	public synchronized void abmelden(ClientCallbackInterface object) throws SimonRemoteException
	{
		_anzahlAngemeldeteClient--;
		_listOfClientCallbacks.remove(object);
		System.out.println("Server: Client abgemeldet. Verbleibende=" + _anzahlAngemeldeteClient);
	}

	public synchronized void anmelden(ClientCallbackInterface object) throws SimonRemoteException
	{
		_anzahlAngemeldeteClient++;
		_listOfClientCallbacks.add(object);
		System.out.println("Server: Client angemeldet. Aktive Clients=" + _anzahlAngemeldeteClient);
	}


	public synchronized int getNumberOfRegisteredClients() throws SimonRemoteException
	{
		return _anzahlAngemeldeteClient;
	}
	
	
	public synchronized Vector<ClientCallbackInterface> getRegisteredClients() throws SimonRemoteException
	{
		return _listOfClientCallbacks;
	}
}
