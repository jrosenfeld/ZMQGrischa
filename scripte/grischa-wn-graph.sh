#! /bin/sh
###############################################################################
#
# This script generates an output for the Graphviz -
# Graph Visualization Software to
# visualize the worker-nodes connected to the GriScha server.
#
# Usage:    1. Install Graphviz (http://graphviz.org/)
#           2. Set the variable SERVER_PORT to the GriScha server port
#           3. Run the script and pipe its output into the Graphviz command
#              circo.
#           4. e.g ./grischa-wn-graph.sh | circo -Tpng > \
#                   /var/www/grischa/hello.png
#
###############################################################################

# Define and set variables
SERVER_PORT=4711

# Get worker-nodes and sides from netstat
WORKER_NODES=`netstat --numeric-hosts | grep $SERVER_PORT | awk '{ print $5 }'\
              | cut -d : -f4 | xargs resolveip 2> /dev/null \
              | grep -o "\(\w\|-\)*\.[a-zA-Z]*$" | sort`
SIDES=`echo "$WORKER_NODES" | uniq`

# Check if worker-nodes connected. If not exit the script
echo $WORKER_NODES | grep ".help" && echo "No WN" && exit 1;

# Generate output for graphviz
echo 'graph networkmap {
 node [fontname=Verdana,fontsize=60]
 node [style=filled]
 node [fillcolor="#EEEEEE"]
 node [color="#EEEEEE"]
 edge [color="#31CEF0", penwidth=3]';

for SIDE in $SIDES; do
    echo "GriScha [fontsize=80]"
    echo "\"GriScha\" -- \"$SIDE\"";

    WNS=`echo "$WORKER_NODES" | grep $SIDE`
    for WN in $WNS; do
        NODE="$WN_$RANDOM [label=\"\"]";
        echo $NODE;
        echo "\"$SIDE\" -- $NODE";
     done;
done;

echo "}";

exit 0;
